###############################################################
#
# Job options file
#
# MC@NLO ttbar with 1leptons EF(l=e/mu) (based on DS5200)
#
# Responsible person(s)
#   7 April, 2009-xx xxx, 20xx: Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
#==============================================================
#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Main generator : Herwig
include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_14TeV.py" ) 
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 13000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

try:
     StreamEVGEN.RequireAlgs = [ "MultiLeptonFilter" ]
except Exception, e:
     pass


from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo31.209550.ttbar'
# 14.2.25.6
# 5000/10100=0.495
evgenConfig.efficiency = 0.44
#==============================================================
#
# End of job options file
#
###############################################################
