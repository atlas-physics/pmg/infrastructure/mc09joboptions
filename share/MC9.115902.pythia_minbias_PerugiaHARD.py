###############################################################
#
# Job options file for dijet production with pythia
#
# author: C. Gwenlan (June'10)
# Perugia HARD (PYTUNE 321) 
# [P. Skands, Perugia MPI workshop Oct08, T. Sjostrand & P. Skands hep-ph/0408302] 
# reference JO: MC9.105001.pythia_minbias.py
#===============================================================


# min bias sample (ND).
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# use Perugia HARD tune 
include ( "MC09JobOptions/MC9_PythiaPerugiaHARD_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

