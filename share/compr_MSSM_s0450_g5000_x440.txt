#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019049E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190492E+03   # EWSB                
         1     4.62600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05864896E+01   # W+
        25     7.13514513E+01   # h
        35     1.00939484E+03   # H
        36     1.00000000E+03   # A
        37     1.00331088E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.49746886E+02   # ~d_L
   2000001     4.49935482E+02   # ~d_R
   1000002     4.50170738E+02   # ~u_L
   2000002     4.50075783E+02   # ~u_R
   1000003     4.49746886E+02   # ~s_L
   2000003     4.49935482E+02   # ~s_R
   1000004     4.50170738E+02   # ~c_L
   2000004     4.50075783E+02   # ~c_R
   1000005     5.04736208E+03   # ~b_1
   2000005     5.04781001E+03   # ~b_2
   1000006     4.90987887E+03   # ~t_1
   2000006     5.04722396E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998829E+03   # ~e_R
   1000012     5.00002353E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998829E+03   # ~mu_R
   1000014     5.00002353E+03   # ~nu_muL
   1000015     4.99982348E+03   # ~tau_1
   2000015     5.00015359E+03   # ~tau_2
   1000016     5.00002353E+03   # ~nu_tauL
   1000021     5.16013053E+03   # ~g
   1000022     4.39971779E+02   # ~chi_10
   1000023    -1.01781880E+03   # ~chi_20
   1000025     1.01976939E+03   # ~chi_30
   1000035     4.96310079E+03   # ~chi_40
   1000024     1.01627463E+03   # ~chi_1+
   1000037     4.96310062E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.96980511E-01   # N_11
  1  2    -1.33920421E-03   # N_12
  1  3     5.42646706E-02   # N_13
  1  4    -5.55284820E-02   # N_14
  2  1     8.93179987E-04   # N_21
  2  2    -3.79374687E-04   # N_22
  2  3     7.07130658E-01   # N_23
  2  4     7.07082238E-01   # N_24
  3  1     7.76467891E-02   # N_31
  3  2     1.97301955E-02   # N_32
  3  3    -7.04853529E-01   # N_33
  3  4     7.04814300E-01   # N_34
  4  1     1.96525431E-04   # N_41
  4  2    -9.99804372E-01   # N_42
  4  3    -1.42506247E-02   # N_43
  4  4     1.37149218E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01512519E-02   # U_11
  1  2     9.99796943E-01   # U_12
  2  1     9.99796943E-01   # U_21
  2  2     2.01512519E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93935437E-02   # V_11
  1  2     9.99811928E-01   # V_12
  2  1     9.99811928E-01   # V_21
  2  2     1.93935437E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07080947E-01   # cos(theta_t)
  1  2     9.94250306E-01   # sin(theta_t)
  2  1    -9.94250306E-01   # -sin(theta_t)
  2  2     1.07080947E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19493665E-01   # cos(theta_b)
  1  2     6.94499004E-01   # sin(theta_b)
  2  1    -6.94499004E-01   # -sin(theta_b)
  2  2     7.19493665E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07236216E-01   # cos(theta_tau)
  1  2     7.06977323E-01   # sin(theta_tau)
  2  1    -7.06977323E-01   # -sin(theta_tau)
  2  2     7.07236216E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21941682E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190492E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43559855E-01   # tanbeta(Q)          
         3     2.44977952E+02   # vev(Q)              
         4     1.10510314E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190492E+03  # The gauge couplings
     1     3.66655553E-01   # gprime(Q) DRbar
     2     6.37247996E-01   # g(Q) DRbar
     3     1.02057611E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190492E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190492E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190492E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190492E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17066709E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190492E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87050186E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190492E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38832278E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190492E+03  # The soft SUSY breaking masses at the scale Q
         1     4.62600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.05089035E+05   # M^2_Hd              
        22     6.77737387E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40143633E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.67068036E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24240421E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24240421E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24232404E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24232404E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24222400E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24222400E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24226438E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24226438E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24240421E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24240421E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24232404E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24232404E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24222400E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24222400E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24226438E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24226438E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.24624017E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.24624017E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.33201188E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.33201188E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.57842236E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.57842236E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48679798E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.69917551E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41498185E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.37510965E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73999095E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26280675E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.72365450E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86571610E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88759761E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67244422E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.82849561E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.79491510E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.81919363E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40613238E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.79852211E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53412964E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.64448174E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.93163702E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71604856E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.01102027E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28534885E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.80566069E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43045405E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.93369816E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.94368079E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.71108739E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.96128881E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.59268675E-04   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     5.75483802E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     3.48482418E-04   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.41123196E-03   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     3.59268675E-04   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     5.75483802E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     3.48482418E-04   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.41123196E-03   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.61805953E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.84116388E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.07552612E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.16280077E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.59105543E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27794570E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.31850182E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63219627E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.94372340E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.44809856E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.62691533E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.48077283E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.61805953E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.84116388E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.07552612E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.16280077E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.59105543E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27794570E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.31850182E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63219627E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.94372340E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.44809856E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.62691533E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.48077283E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.65187513E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.90342428E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.55793772E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.73363674E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.30645349E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.97584442E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.61651815E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64741006E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.91110454E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.13333475E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.97235760E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.33492908E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.02414236E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.67337488E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.61273165E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94157068E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.23939435E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.74580706E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.61191556E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.11157747E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.32211616E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.61273165E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94157068E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.23939435E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.74580706E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.61191556E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.11157747E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.32211616E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.63033120E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.91518177E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.23345010E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.74117299E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.59436489E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.76185662E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.31712296E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.04430824E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.14291487E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.39623223E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.14291487E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.39623223E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.86921706E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.05096474E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88302615E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88301725E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88302615E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88301725E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.15617992E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45188344E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.59045507E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28281519E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.26827878E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.55654877E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77601300E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77597229E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.08772882E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77490722E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76581600E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.07018119E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99766042E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.18684393E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.92611631E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.92611631E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.08927212E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.08927212E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.35023975E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.35023975E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     5.22458847E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     5.22458847E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.92611631E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.92611631E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.08927212E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.08927212E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.35023975E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.35023975E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     5.22458847E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     5.22458847E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.20616451E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.24675610E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.17467176E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.74790370E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.74790370E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.40571242E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.40571242E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     9.27936792E-05    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     9.27936792E-05    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.51522392E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.51522392E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.74790370E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.74790370E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.40571242E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.40571242E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     9.27936792E-05    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     9.27936792E-05    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.51522392E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.51522392E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.05096673E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.74312395E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45121386E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.62823900E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28188088E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28188088E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.69107518E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.45266059E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.08363175E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.65519896E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76832674E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.56556744E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53943020E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.60734511E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76128683E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77695910E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77695910E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13805506E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13805506E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.07813070E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.07813070E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13960598E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13960598E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.19535928E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.19535928E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13805506E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13805506E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.07813070E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.07813070E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13960598E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13960598E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.19535928E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.19535928E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88475078E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60877669E-01    2           5        -5   # BR(h -> b       bb     )
     7.92836557E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81307049E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68830924E-04    2           3        -3   # BR(h -> s       sb     )
     2.82978638E-02    2           4        -4   # BR(h -> c       cb     )
     2.95925374E-02    2          21        21   # BR(h -> g       g      )
     6.98879215E-04    2          22        22   # BR(h -> gam     gam    )
     2.31108539E-04    2          24       -24   # BR(h -> W+      W-     )
     6.81483214E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86134022E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46379258E-04    2           5        -5   # BR(H -> b       bb     )
     3.77060478E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33291759E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85102529E-07    2           3        -3   # BR(H -> s       sb     )
     1.10884416E-05    2           4        -4   # BR(H -> c       cb     )
     9.94638883E-01    2           6        -6   # BR(H -> t       tb     )
     1.42272797E-03    2          21        21   # BR(H -> g       g      )
     4.41789558E-06    2          22        22   # BR(H -> gam     gam    )
     1.68200010E-06    2          23        22   # BR(H -> Z       gam    )
     3.77072143E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86526081E-04    2          23        23   # BR(H -> Z       Z      )
     1.39961559E-03    2          25        25   # BR(H -> h       h      )
     1.01747839E-18    2          36        36   # BR(H -> A       A      )
     1.61630746E-09    2          23        36   # BR(H -> Z       A      )
     4.48770084E-10    2          24       -37   # BR(H -> W+      H-     )
     4.48770084E-10    2         -24        37   # BR(H -> W-      H+     )
     8.11057975E-08    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.13032876E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     5.32805791E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     3.13032876E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     5.32805791E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     4.57099898E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.33362609E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     4.57099898E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.33362609E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20068448E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38996240E-04    2           5        -5   # BR(A -> b       bb     )
     3.54828356E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25431117E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87741787E-07    2           3        -3   # BR(A -> s       sb     )
     1.02310080E-05    2           4        -4   # BR(A -> c       cb     )
     9.97526851E-01    2           6        -6   # BR(A -> t       tb     )
     1.70720754E-03    2          21        21   # BR(A -> g       g      )
     5.53903268E-06    2          22        22   # BR(A -> gam     gam    )
     2.07309069E-06    2          23        22   # BR(A -> Z       gam    )
     3.41857908E-04    2          23        25   # BR(A -> Z       h      )
     1.31448354E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05683172E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95563599E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66130463E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29426383E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42828539E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83957070E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02692068E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.98152986E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.57160615E-04    2          24        25   # BR(H+ -> W+      h      )
     1.03149013E-11    2          24        36   # BR(H+ -> W+      A      )
     7.21217349E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     7.21217349E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
