################################################################
#
# MC@NLO/JIMMY/HERWIG W+W- -> tau+nu tau-nu
#
# Responsible person(s)
#   Feb 19, 2010 : Tiesheng Dai (Tiesheng.Dai@cern.ch)
#
################################################################
#
# Job options file
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# ... Herwig
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#
### Match W and Z mass and width with MC@NLO event file
### to avoid miss leading information from event generator log file
#
Herwig.HerwigCommand += [ "rmass 198 80.40", "rmass 199 80.40", "gamw 2.141",
                          "rmass 200 91.19", "gamz 2.495",
                          "maxpr 5",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0341.105927'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo0341.105927.07TeV_WpWm_taunutaunu.TXT.v1'
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo0341.105927.10TeV_WpWm_taunutaunu.TXT.v1'
except NameError:
  pass
                
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
