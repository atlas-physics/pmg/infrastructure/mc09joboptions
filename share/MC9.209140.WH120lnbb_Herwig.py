###############################################################
#
# Job options file
#
# Herwig WH production with a higgs and W pT filter
# Author: Giacinto Piacquadio (2/5/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )
Herwig.HerwigCommand += ["iproc 12605",
                         "rmass 201 120.0",
			 "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

# Add the filters:
# Higgs and W filter
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

ParticleFilterW= ParticleFilter(name = "ParticleFilterW",
                                StatusReq=195,
                                PDG=24,
                                Ptcut = 100.*GeV,
                                Etacut = 3.0,
                                Energycut = 1000000000.0)

LeptonFilter= LeptonFilter(name = "LeptonFilter")
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 3.0 

topAlg += LeptonFilter
topAlg += ParticleFilterHiggs
topAlg += ParticleFilterW

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterW" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.04492*0.9
# 5000/111320=0.04492
#==============================================================
#
# End of job options file
#
###############################################################
