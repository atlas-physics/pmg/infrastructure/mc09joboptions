# Higgs via gluon fusion with H->tautau->hh
#--------------------------------------------------------------
# Junichi TANAKA, 2010
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += ["rmass 201 300.0",
                         "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment_HadronicDecay.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.116450.ggHtautau_MA300TB20_7TeV.TXT.v1'
except NameError:
  pass
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################
