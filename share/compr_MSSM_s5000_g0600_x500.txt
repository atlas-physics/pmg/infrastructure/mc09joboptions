#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019078E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190784E+03   # EWSB                
         1     5.08800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889147E+01   # W+
        25     8.65563326E+01   # h
        35     1.00954631E+03   # H
        36     1.00000000E+03   # A
        37     1.00338792E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04404054E+03   # ~d_L
   2000001     5.04405636E+03   # ~d_R
   1000002     5.04407612E+03   # ~u_L
   2000002     5.04406818E+03   # ~u_R
   1000003     5.04404054E+03   # ~s_L
   2000003     5.04405636E+03   # ~s_R
   1000004     5.04407612E+03   # ~c_L
   2000004     5.04406818E+03   # ~c_R
   1000005     5.04382485E+03   # ~b_1
   2000005     5.04427317E+03   # ~b_2
   1000006     4.90858276E+03   # ~t_1
   2000006     5.04264244E+03   # ~t_2
   1000011     4.99998822E+03   # ~e_L
   2000011     4.99998828E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998822E+03   # ~mu_L
   2000013     4.99998828E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015299E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.99996176E+02   # ~g
   1000022     5.00132428E+02   # ~chi_10
   1000023    -1.01757645E+03   # ~chi_20
   1000025     1.01993681E+03   # ~chi_30
   1000035     5.02219712E+03   # ~chi_40
   1000024     1.01607752E+03   # ~chi_1+
   1000037     5.02219695E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.96279614E-01   # N_11
  1  2    -1.47572124E-03   # N_12
  1  3     6.03153554E-02   # N_13
  1  4    -6.15370717E-02   # N_14
  2  1     8.63669108E-04   # N_21
  2  2    -3.77613059E-04   # N_22
  2  3     7.07130022E-01   # N_23
  2  4     7.07082911E-01   # N_24
  3  1     8.61751041E-02   # N_31
  3  2     1.92825224E-02   # N_32
  3  3    -7.04368404E-01   # N_33
  3  4     7.04320373E-01   # N_34
  4  1     1.91152019E-04   # N_41
  4  2    -9.99812914E-01   # N_42
  4  3    -1.39406378E-02   # N_43
  4  4     1.34073895E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97129666E-02   # U_11
  1  2     9.99805681E-01   # U_12
  2  1     9.99805681E-01   # U_21
  2  2     1.97129666E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89587356E-02   # V_11
  1  2     9.99820267E-01   # V_12
  2  1     9.99820267E-01   # V_21
  2  2     1.89587356E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04491077E-01   # cos(theta_t)
  1  2     9.94525824E-01   # sin(theta_t)
  2  1    -9.94525824E-01   # -sin(theta_t)
  2  2     1.04491077E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19473287E-01   # cos(theta_b)
  1  2     6.94520114E-01   # sin(theta_b)
  2  1    -6.94520114E-01   # -sin(theta_b)
  2  2     7.19473287E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173074E-01   # cos(theta_tau)
  1  2     7.07040482E-01   # sin(theta_tau)
  2  1    -7.07040482E-01   # -sin(theta_tau)
  2  2     7.07173074E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22385920E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190784E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.42864646E-01   # tanbeta(Q)          
         3     2.44091668E+02   # vev(Q)              
         4     1.10546067E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190784E+03  # The gauge couplings
     1     3.65889257E-01   # gprime(Q) DRbar
     2     6.34849478E-01   # g(Q) DRbar
     3     1.01824854E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17633762E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87616128E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38873502E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190784E+03  # The soft SUSY breaking masses at the scale Q
         1     5.08800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03570122E+05   # M^2_Hd              
        22     6.88316896E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39080153E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.08969324E-09   # gluino decays
#          BR         NDA      ID1       ID2
     1.66600317E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     8.54876912E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.89495638E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.54876912E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.89495638E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.34330246E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.85707819E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.99846057E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07111737E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.05470931E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.11898089E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.55534638E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.64131770E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.04276701E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.38795784E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.38884695E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.84011792E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.34089773E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.17237604E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.34612518E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.80783612E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.02497071E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.38549767E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.20784877E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.35587438E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11792903E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.65681904E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.29771689E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     9.31543376E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.98350979E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.45793782E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.01016719E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.08225562E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09445595E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.57048719E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.38253857E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.90653388E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.34727785E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.08360623E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.44541223E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.00612126E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.16135272E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.92370718E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.32351122E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97682244E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.45364616E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.28149531E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.32965631E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.31848468E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.70283297E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66953175E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.34724814E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.14887216E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61214749E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.05699538E-07    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.15447720E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.24211207E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.30829577E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97683657E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.36803878E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.41223699E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.97216013E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.94352123E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.22966904E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91528322E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.34727785E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.08360623E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.44541223E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.00612126E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.16135272E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.92370718E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.32351122E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97682244E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.45364616E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.28149531E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.32965631E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.31848468E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.70283297E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66953175E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.34724814E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.14887216E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61214749E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.05699538E-07    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.15447720E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.24211207E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.30829577E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97683657E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.36803878E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.41223699E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.97216013E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.94352123E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.22966904E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91528322E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.54593467E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.84441159E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.06232279E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.33713336E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.18746633E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.60910418E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.93036495E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.99664322E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.96280545E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.54593467E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.84441159E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.06232279E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.33713336E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.18746633E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.60910418E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.93036495E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.99664322E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.96280545E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.63686275E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.89480493E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.60672408E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     8.01260974E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.94622500E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.63212171E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.89946558E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.18957936E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.52920922E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.27518864E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.54097976E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95374439E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.15785811E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.59857348E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.02483015E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.54097976E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95374439E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.15785811E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.59857348E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.02483015E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55860063E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92700184E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.15206064E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.59159194E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.70607195E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.01299817E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51213435E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.18212494E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07986637E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07986637E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07306199E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08641459E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08641459E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05793158E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02851315E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78529745E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.34303806E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71595974E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.70813803E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85273066E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.32077884E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.32086785E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.54287870E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31648743E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.26807372E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05113011E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99833524E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.66476118E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.10135851E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03638697E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99896361E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51211115E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.30950428E-07    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78500920E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.75903534E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71559450E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71559450E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.00268961E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.20778287E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     5.52317588E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.11727254E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28925477E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.48970823E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84358096E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.33677680E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.24967682E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32454772E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32454772E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04301161E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04301161E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     5.06664629E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.06664629E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04301161E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04301161E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     5.06664629E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.06664629E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27185127E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27185127E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12440389E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12440389E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04016617E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04016617E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04016617E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04016617E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04016617E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04016617E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.23596383E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45541057E-01    2           5        -5   # BR(h -> b       bb     )
     8.11792488E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87689963E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56390547E-04    2           3        -3   # BR(h -> s       sb     )
     2.77506668E-02    2           4        -4   # BR(h -> c       cb     )
     4.20255005E-02    2          21        21   # BR(h -> g       g      )
     1.10338027E-03    2          22        22   # BR(h -> gam     gam    )
     1.16864186E-03    2          24       -24   # BR(h -> W+      W-     )
     2.87424241E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86183218E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47107693E-04    2           5        -5   # BR(H -> b       bb     )
     3.76457102E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33078463E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.84792983E-07    2           3        -3   # BR(H -> s       sb     )
     1.11065494E-05    2           4        -4   # BR(H -> c       cb     )
     9.96304531E-01    2           6        -6   # BR(H -> t       tb     )
     1.42478903E-03    2          21        21   # BR(H -> g       g      )
     4.73355102E-06    2          22        22   # BR(H -> gam     gam    )
     1.68498920E-06    2          23        22   # BR(H -> Z       gam    )
     3.84911991E-04    2          24       -24   # BR(H -> W+      W-     )
     1.90405264E-04    2          23        23   # BR(H -> Z       Z      )
     1.39276186E-03    2          25        25   # BR(H -> h       h      )
     1.10668509E-18    2          36        36   # BR(H -> A       A      )
     1.75074193E-09    2          23        36   # BR(H -> Z       A      )
     4.76838050E-10    2          24       -37   # BR(H -> W+      H-     )
     4.76838050E-10    2         -24        37   # BR(H -> W-      H+     )
     2.01113659E-09    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20769357E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39396663E-04    2           5        -5   # BR(A -> b       bb     )
     3.53828815E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25077781E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87224713E-07    2           3        -3   # BR(A -> s       sb     )
     1.02323106E-05    2           4        -4   # BR(A -> c       cb     )
     9.97653858E-01    2           6        -6   # BR(A -> t       tb     )
     1.70741282E-03    2          21        21   # BR(A -> g       g      )
     5.53953471E-06    2          22        22   # BR(A -> gam     gam    )
     2.07459706E-06    2          23        22   # BR(A -> Z       gam    )
     3.45790733E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     5.05737339E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97155699E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65580049E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29231813E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43832666E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.82618945E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02833916E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99590785E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.61835911E-04    2          24        25   # BR(H+ -> W+      h      )
     1.15700930E-11    2          24        36   # BR(H+ -> W+      A      )
