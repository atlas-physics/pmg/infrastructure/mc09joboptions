#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11048074E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.10480735E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     3.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.00000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07367937E+01   # W+
        25     1.21278343E+02   # h
        35     1.00088466E+03   # H
        36     1.00000000E+03   # A
        37     1.00378306E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04705918E+03   # ~d_L
   2000001     5.04680677E+03   # ~d_R
   1000002     5.04649464E+03   # ~u_L
   2000002     5.04662766E+03   # ~u_R
   1000003     5.04705918E+03   # ~s_L
   2000003     5.04680677E+03   # ~s_R
   1000004     5.04649464E+03   # ~c_L
   2000004     5.04662766E+03   # ~c_R
   1000005     2.40784127E+02   # ~b_1
   2000005     5.04680800E+03   # ~b_2
   1000006     5.92130304E+02   # ~t_1
   2000006     5.07460793E+03   # ~t_2
   1000011     5.00019090E+03   # ~e_L
   2000011     5.00017743E+03   # ~e_R
   1000012     4.99963165E+03   # ~nu_eL
   1000013     5.00019090E+03   # ~mu_L
   2000013     5.00017743E+03   # ~mu_R
   1000014     4.99963165E+03   # ~nu_muL
   1000015     4.99934136E+03   # ~tau_1
   2000015     5.00102744E+03   # ~tau_2
   1000016     4.99963165E+03   # ~nu_tauL
   1000021     4.50804231E+02   # ~g
   1000022     5.91064648E+01   # ~chi_10
   1000023     6.87512481E+02   # ~chi_20
   1000025    -1.00162775E+03   # ~chi_30
   1000035     1.01500880E+03   # ~chi_40
   1000024     6.87473095E+02   # ~chi_1+
   1000037     1.01468158E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98965856E-01   # N_11
  1  2    -2.47264561E-03   # N_12
  1  3     4.38952424E-02   # N_13
  1  4    -1.15893089E-02   # N_14
  2  1     1.09708040E-02   # N_21
  2  2     9.78746295E-01   # N_22
  2  3    -1.61193324E-01   # N_23
  2  4     1.26301400E-01   # N_24
  3  1     2.27431464E-02   # N_31
  3  2    -2.51909021E-02   # N_32
  3  3    -7.05822028E-01   # N_33
  3  4    -7.07575743E-01   # N_34
  4  1     3.78101716E-02   # N_41
  4  2    -2.03506743E-01   # N_42
  4  3    -6.88407717E-01   # N_43
  4  4     6.95162004E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73551306E-01   # U_11
  1  2     2.28468499E-01   # U_12
  2  1     2.28468499E-01   # U_21
  2  2     9.73551306E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83810915E-01   # V_11
  1  2     1.79209610E-01   # V_12
  2  1     1.79209610E-01   # V_21
  2  2     9.83810915E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998917E-01   # cos(theta_t)
  1  2     1.47173327E-03   # sin(theta_t)
  2  1    -1.47173327E-03   # -sin(theta_t)
  2  2     9.99998917E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999882E-01   # cos(theta_b)
  1  2     4.85798298E-04   # sin(theta_b)
  2  1    -4.85798298E-04   # -sin(theta_b)
  2  2     9.99999882E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04275694E-01   # cos(theta_tau)
  1  2     7.09926578E-01   # sin(theta_tau)
  2  1    -7.09926578E-01   # -sin(theta_tau)
  2  2     7.04275694E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10074236E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10480735E+03  # DRbar Higgs Parameters
         1     9.95260678E+02   # mu(Q)               
         2     4.80390456E+00   # tanbeta(Q)          
         3     2.43935291E+02   # vev(Q)              
         4     9.99941253E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.10480735E+03  # The gauge couplings
     1     3.60660334E-01   # gprime(Q) DRbar
     2     6.40300514E-01   # g(Q) DRbar
     3     1.06244314E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10480735E+03  # The trilinear couplings
  1  1     1.03045075E+03   # A_u(Q) DRbar
  2  2     1.03045075E+03   # A_c(Q) DRbar
  3  3     1.31800944E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10480735E+03  # The trilinear couplings
  1  1     7.32352041E+02   # A_d(Q) DRbar
  2  2     7.32352041E+02   # A_s(Q) DRbar
  3  3     8.28811103E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10480735E+03  # The trilinear couplings
  1  1     4.37698690E+02   # A_e(Q) DRbar
  2  2     4.37698690E+02   # A_mu(Q) DRbar
  3  3     4.38061545E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10480735E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.80004612E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10480735E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.17104317E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10480735E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.99230650E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10480735E+03  # The soft SUSY breaking masses at the scale Q
         1     1.47479547E+02   # M_1                 
         2     8.79904746E+02   # M_2                 
         3     1.38576982E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.47178101E+06   # M^2_Hd              
        22     2.36145915E+07   # M^2_Hu              
        31     5.14576153E+03   # M_eL                
        32     5.14576153E+03   # M_muL               
        33     5.14919908E+03   # M_tauL              
        34     4.68379926E+03   # M_eR                
        35     4.68379926E+03   # M_muR               
        36     4.69138824E+03   # M_tauR              
        41     5.02697844E+03   # M_q1L               
        42     5.02697844E+03   # M_q2L               
        43     3.00230593E+03   # M_q3L               
        44     5.29838257E+03   # M_uR                
        45     5.29838257E+03   # M_cR                
        46     6.75728663E+03   # M_tR                
        47     4.98716439E+03   # M_dR                
        48     4.98716439E+03   # M_sR                
        49     4.99198891E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40829027E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.27498705E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     5.88067368E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.12319680E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98876803E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.68138387E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.40963105E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26607130E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.87417337E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.62771967E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.48970300E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.30880828E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.84140828E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.76174127E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.73324784E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     3.70317130E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     8.26147864E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     1.89512862E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.58176659E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.17182382E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.22891549E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.69775797E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.50026996E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.50175173E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.25650413E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.92078417E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.24476325E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88474601E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.74910699E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.63264351E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     2.61182840E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     4.59173597E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.34671415E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.35215175E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.77778561E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.80937932E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.92343182E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.15547654E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.89599181E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.90732749E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.15713821E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.44808343E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.30412052E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.39914229E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.97263510E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.64282461E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.53145892E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65942862E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.77788050E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.90752428E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.88261914E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.27494563E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.18024567E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.70268332E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.13178862E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.44884667E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.21984177E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.72059523E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.01919311E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.21473492E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.16256482E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91262545E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.77778561E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.80937932E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.92343182E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.15547654E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.89599181E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.90732749E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.15713821E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.44808343E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.30412052E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.39914229E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.97263510E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.64282461E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.53145892E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65942862E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.77788050E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.90752428E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.88261914E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.27494563E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.18024567E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.70268332E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.13178862E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.44884667E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.21984177E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.72059523E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.01919311E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.21473492E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.16256482E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91262545E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.52280503E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80852483E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91909847E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.41488027E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.54140257E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70415512E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.00038410E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58673194E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98092522E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15901592E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76781180E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31479513E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.52280503E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80852483E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91909847E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.41488027E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.54140257E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70415512E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.00038410E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58673194E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98092522E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15901592E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76781180E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31479513E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.57761930E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54751180E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13854883E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.43906792E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.48960331E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18086647E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.37861854E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60046504E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47305277E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01424250E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.84613140E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64907959E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93023043E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.79105034E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.52434561E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97898532E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84528467E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.15802078E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45183955E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82293619E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84538635E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.52434561E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97898532E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84528467E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.15802078E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45183955E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82293619E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84538635E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.54719173E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94416411E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83535618E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.14351156E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44677343E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80449720E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16909351E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.00018447E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.83870007E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.29528848E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.08415121E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.59740490E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.92715104E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.51399853E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.39344385E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.33198371E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.42219890E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.71967317E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.13408115E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.56881959E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.56369089E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98489714E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98489714E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.62641894E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.02279576E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.07225163E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.09675081E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.09675081E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.02303924E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.46837784E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.11033221E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.11033221E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.71575227E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.71575227E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.60457699E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02612307E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97839624E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.19180228E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.19180228E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.97378513E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.06101521E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.85834629E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.85834629E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.09456429E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.09456429E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.78696935E-03   # h decays
#          BR         NDA      ID1       ID2
     6.77768221E-01    2           5        -5   # BR(h -> b       bb     )
     6.94117966E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.45683095E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.23446663E-04    2           3        -3   # BR(h -> s       sb     )
     2.17413321E-02    2           4        -4   # BR(h -> c       cb     )
     7.09450563E-02    2          21        21   # BR(h -> g       g      )
     2.16244875E-03    2          22        22   # BR(h -> gam     gam    )
     1.17786828E-03    2          22        23   # BR(h -> Z       gam    )
     1.38615253E-01    2          24       -24   # BR(h -> W+      W-     )
     1.70527631E-02    2          23        23   # BR(h -> Z       Z      )
     3.56131243E-04    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.32690028E+00   # H decays
#          BR         NDA      ID1       ID2
     1.34173718E-01    2           5        -5   # BR(H -> b       bb     )
     2.05325788E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.25831680E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03869781E-04    2           3        -3   # BR(H -> s       sb     )
     9.16065845E-06    2           4        -4   # BR(H -> c       cb     )
     8.19772102E-01    2           6        -6   # BR(H -> t       tb     )
     1.14001978E-03    2          21        21   # BR(H -> g       g      )
     3.78413895E-06    2          22        22   # BR(H -> gam     gam    )
     1.25625067E-06    2          23        22   # BR(H -> Z       gam    )
     3.18714271E-03    2          24       -24   # BR(H -> W+      W-     )
     1.57652279E-03    2          23        23   # BR(H -> Z       Z      )
     1.02311811E-02    2          25        25   # BR(H -> h       h      )
     4.47037218E-22    2          36        36   # BR(H -> A       A      )
     2.52506039E-13    2          23        36   # BR(H -> Z       A      )
     1.64229254E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.99101064E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.56277725E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40512119E+00   # A decays
#          BR         NDA      ID1       ID2
     1.30130717E-01    2           5        -5   # BR(A -> b       bb     )
     1.98880266E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.03037776E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00910181E-04    2           3        -3   # BR(A -> s       sb     )
     8.53477615E-06    2           4        -4   # BR(A -> c       cb     )
     8.32143653E-01    2           6        -6   # BR(A -> t       tb     )
     1.60804488E-03    2          21        21   # BR(A -> g       g      )
     5.33499563E-06    2          22        22   # BR(A -> gam     gam    )
     1.89069386E-06    2          23        22   # BR(A -> Z       gam    )
     2.97962873E-03    2          23        25   # BR(A -> Z       h      )
     2.01598762E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10469677E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35839977E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.07976874E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.03587504E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.19677733E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.33095412E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.91353917E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09407125E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.52720014E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.09232229E-03    2          24        25   # BR(H+ -> W+      h      )
     4.30571344E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63465899E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.08672679E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
