#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.1a                   |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    hep-ph/0311167                          |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.14349492E+03
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.1a        # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.33         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   MSSM                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.75000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.43494920E+02   # EWSB_scale          
         1     4.95500000E+01   # M1                  
         2     1.50000000E+02   # M2                  
         3     2.29800000E+02   # M3                  
        11    -5.00000000E+02   # A_t                 
        23     5.00000000E+02   # MU(EWSB             
        24     2.50000000E+02   # M_A(pole!)          
        25     5.00000000E+01   # tanbeta             
        26     1.00000000E+00   # sgn(mu)             
        31     3.50000000E+02   # M_e_L               
        33     3.50000000E+02   # M_tau_L             
        34     3.50000000E+02   # M_e_R               
        36     3.50000000E+02   # M_tau_R             
        41     1.00000000E+03   # M-qu_L              
        43     5.00000000E+02   # M_Q_L               
        44     1.00000000E+03   # M_u_R               
        46     3.98900000E+01   # M_t_R               
        47     1.00000000E+03   # M_u_R               
        49     5.00000000E+02   # M_b_R               
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04931663E+01   # W+
        25     1.15518663E+02   # h
        35     2.53528491E+02   # H
        36     1.58113883E+02   # A
        37     2.62612888E+02   # H+
         5     5.42335571E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.00952349E+03   # ~d_L
   2000001     1.00808938E+03   # ~d_R
   1000002     1.00632388E+03   # ~u_L
   2000002     1.00710238E+03   # ~u_R
   1000003     1.00952349E+03   # ~s_L
   2000003     1.00808938E+03   # ~s_R
   1000004     1.00632388E+03   # ~c_L
   2000004     1.00710238E+03   # ~c_R
   1000005     4.27698501E+02   # ~b_1
   2000005     5.71867535E+02   # ~b_2
   1000006     1.10000000E+02   # ~t_1
   2000006     5.00000000E+02   # ~t_2
   1000011     3.53111669E+02   # ~e_L
   2000011     3.52776117E+02   # ~e_R
   1000012     3.44036569E+02   # ~nu_eL
   1000013     3.53111669E+02   # ~mu_L
   2000013     3.52776117E+02   # ~mu_R
   1000014     3.44036569E+02   # ~nu_muL
   1000015     2.79741816E+02   # ~tau_1
   2000015     4.13388719E+02   # ~tau_2
   1000016     3.44036569E+02   # ~nu_tauL
   1000021     5.50000000E+02   # ~g
   1000022     7.50000000E+01   # ~chi_10
   1000023     1.54101674E+02   # ~chi_20
   1000025    -5.11266186E+02   # ~chi_30
   1000035     5.16522995E+02   # ~chi_40
   1000024     1.54091267E+02   # ~chi_1+
   1000037     5.18830663E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.95878765E-01   # N_11
  1  2    -9.09934992E-03   # N_12
  1  3     8.96063539E-02   # N_13
  1  4    -1.06484285E-02   # N_14
  2  1     2.48253526E-02   # N_21
  2  2     9.83692376E-01   # N_22
  2  3    -1.69505233E-01   # N_23
  2  4     5.47812662E-02   # N_24
  3  1     5.45809736E-02   # N_31
  3  2    -8.24127362E-02   # N_32
  3  3    -6.99129030E-01   # N_33
  3  4    -7.08129690E-01   # N_34
  4  1     6.80448752E-02   # N_41
  4  2    -1.59607807E-01   # N_42
  4  3    -6.88808592E-01   # N_43
  4  4     7.03873544E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.70780665E-01   # U_11
  1  2     2.39968539E-01   # U_12
  2  1     2.39968539E-01   # U_21
  2  2     9.70780665E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.96979506E-01   # V_11
  1  2     7.76650798E-02   # V_12
  2  1     7.76650798E-02   # V_21
  2  2     9.96979506E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     2.89040240E-01   # cos(theta_t)
  1  2     9.57316948E-01   # sin(theta_t)
  2  1    -9.57316948E-01   # -sin(theta_t)
  2  2     2.89040240E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.00034267E-01   # cos(theta_b)
  1  2     7.14109253E-01   # sin(theta_b)
  2  1    -7.14109253E-01   # -sin(theta_b)
  2  2     7.00034267E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06202188E-01   # cos(theta_tau)
  1  2     7.08010219E-01   # sin(theta_tau)
  2  1    -7.08010219E-01   # -sin(theta_tau)
  2  2     7.06202188E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.05768543E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.43494920E+02  # DRbar Higgs Parameters
         1     5.00000000E+02   # mu(Q)               
         2     4.99538069E+01   # tanbeta(Q)          
         3     2.47662636E+02   # vev(Q)              
         4     3.84134213E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.43494920E+02  # The gauge couplings
     1     3.56835852E-01   # gprime(Q) DRbar
     2     6.42583167E-01   # g(Q) DRbar
     3     1.14170305E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.43494920E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3    -5.00000000E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  1.43494920E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  1.43494920E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.43494920E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.23280682E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.43494920E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     8.14238254E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.43494920E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.29068065E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.43494920E+02  # The soft SUSY breaking masses at the scale Q
         1     4.95500000E+01   # M1                  
         2     1.50000000E+02   # M2                  
         3     2.29800000E+02   # M3                  
        21    -1.88395834E+05   # M^2_Hd              
        22    -2.74878113E+05   # M^2_Hu              
        31     3.50000000E+02   # M_e_L               
        32     3.50000000E+02   # M_muL               
        33     3.50000000E+02   # M_tau_L             
        34     3.50000000E+02   # M_e_R               
        35     3.50000000E+02   # M_muR               
        36     3.50000000E+02   # M_tau_R             
        41     1.00000000E+03   # M-qu_L              
        42     1.00000000E+03   # M_q2L               
        43     5.00000000E+02   # M_Q_L               
        44     1.00000000E+03   # M_u_R               
        45     1.00000000E+03   # M_cR                
        46     3.98900000E+01   # M_t_R               
        47     1.00000000E+03   # M_u_R               
        48     1.00000000E+03   # M_sR                
        49     5.00000000E+02   # M_b_R

