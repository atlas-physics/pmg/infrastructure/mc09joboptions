#
# Job options file
# Martin Schmitz, Nov 02 2009
# (mschmitz@cern.ch)
#
#==============================================================

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]

topAlg += sherpa

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

#from RecExConfig.RecFlags  import rec
#rec.doTruth = True

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC09JobOptions.SherpaEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'Sherpa010103.209239.Z3jetstotautauEW'
evgenConfig.efficiency = 1.00*0.95
evgenConfig.minevents = 500
