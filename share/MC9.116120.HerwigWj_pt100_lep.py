# W+jets production with a lepton filter and pT min = 100 GeV
# Adam Davison <adamd@hep.ucl.ac.uk>
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += ["iproc 12100",    # note that 10000 is added to the herwig process number
                         "taudec TAUOLA",
                         "ptmin 100."]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter= topAlg.LeptonFilter
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 2.7

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
     pass

#---------------------------------------------------------------
#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.2158
