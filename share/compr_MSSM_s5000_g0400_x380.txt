#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018904E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00189040E+03   # EWSB                
         1     3.87000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.18000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889056E+01   # W+
        25     8.59630158E+01   # h
        35     1.00946240E+03   # H
        36     1.00000000E+03   # A
        37     1.00337785E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04435144E+03   # ~d_L
   2000001     5.04436711E+03   # ~d_R
   1000002     5.04438667E+03   # ~u_L
   2000002     5.04437880E+03   # ~u_R
   1000003     5.04435144E+03   # ~s_L
   2000003     5.04436711E+03   # ~s_R
   1000004     5.04438667E+03   # ~c_L
   2000004     5.04437880E+03   # ~c_R
   1000005     5.04413623E+03   # ~b_1
   2000005     5.04458342E+03   # ~b_2
   1000006     4.91032120E+03   # ~t_1
   2000006     5.04361960E+03   # ~t_2
   1000011     4.99998834E+03   # ~e_L
   2000011     4.99998840E+03   # ~e_R
   1000012     5.00002326E+03   # ~nu_eL
   1000013     4.99998834E+03   # ~mu_L
   2000013     4.99998840E+03   # ~mu_R
   1000014     5.00002326E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015322E+03   # ~tau_2
   1000016     5.00002326E+03   # ~nu_tauL
   1000021     4.00283133E+02   # ~g
   1000022     3.80052603E+02   # ~chi_10
   1000023    -1.01764853E+03   # ~chi_20
   1000025     1.01928066E+03   # ~chi_30
   1000035     5.02219896E+03   # ~chi_40
   1000024     1.01614848E+03   # ~chi_1+
   1000037     5.02219880E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97545913E-01   # N_11
  1  2    -1.16816383E-03   # N_12
  1  3     4.88403467E-02   # N_13
  1  4    -5.01538350E-02   # N_14
  2  1     9.28196512E-04   # N_21
  2  2    -3.73627692E-04   # N_22
  2  3     7.07130882E-01   # N_23
  2  4     7.07081972E-01   # N_24
  3  1     7.00089635E-02   # N_31
  3  2     1.93104874E-02   # N_32
  3  3    -7.05256079E-01   # N_33
  3  4     7.05223165E-01   # N_34
  4  1     1.86298229E-04   # N_41
  4  2    -9.99812783E-01   # N_42
  4  3    -1.39427061E-02   # N_43
  4  4     1.34151171E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97159454E-02   # U_11
  1  2     9.99805622E-01   # U_12
  2  1     9.99805622E-01   # U_21
  2  2     1.97159454E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89697167E-02   # V_11
  1  2     9.99820059E-01   # V_12
  2  1     9.99820059E-01   # V_21
  2  2     1.89697167E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03070024E-01   # cos(theta_t)
  1  2     9.94674102E-01   # sin(theta_t)
  2  1    -9.94674102E-01   # -sin(theta_t)
  2  2     1.03070024E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19382343E-01   # cos(theta_b)
  1  2     6.94614314E-01   # sin(theta_b)
  2  1    -6.94614314E-01   # -sin(theta_b)
  2  2     7.19382343E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173186E-01   # cos(theta_tau)
  1  2     7.07040370E-01   # sin(theta_tau)
  2  1    -7.07040370E-01   # -sin(theta_tau)
  2  2     7.07173186E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21974626E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00189040E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43471905E-01   # tanbeta(Q)          
         3     2.44164138E+02   # vev(Q)              
         4     1.10454237E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00189040E+03  # The gauge couplings
     1     3.65894699E-01   # gprime(Q) DRbar
     2     6.34873146E-01   # g(Q) DRbar
     3     1.02550031E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00189040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17019029E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86962657E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00189040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38881588E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00189040E+03  # The soft SUSY breaking masses at the scale Q
         1     3.87000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.18000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03393310E+05   # M^2_Hd              
        22     6.76179159E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39090562E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.58389179E-12   # gluino decays
#          BR         NDA      ID1       ID2
     7.72679442E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     2.43433588E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.25054503E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.43433588E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.25054503E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.36229397E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.95589129E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.94745182E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.04311007E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.03070567E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.06511325E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.66632583E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.75239099E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.80208773E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.34236456E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.34501969E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.57937933E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.80893457E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.26693197E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.26564289E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.35381145E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.14149157E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.28960060E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.06768535E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.77638614E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.12173510E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.59545474E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.36010375E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.39886076E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.09972569E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.39150615E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.86648916E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.46711852E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09326411E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.50945962E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.44456752E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.03507109E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.47002992E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.03591802E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.00777047E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.64714034E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.12908433E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.78882004E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.25894637E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97745840E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.57715663E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.19878973E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.59742606E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.47727879E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.20927318E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67864349E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.46999961E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.08617346E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60339784E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.80762278E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.12242328E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.09277426E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.24420444E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97747409E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.49093265E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.19447553E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.65393458E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.78440664E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.10177727E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91767674E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.47002992E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.03591802E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.00777047E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.64714034E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.12908433E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.78882004E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.25894637E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97745840E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.57715663E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.19878973E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.59742606E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.47727879E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.20927318E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67864349E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.46999961E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.08617346E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60339784E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.80762278E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.12242328E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.09277426E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.24420444E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97747409E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.49093265E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.19447553E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.65393458E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.78440664E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.10177727E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91767674E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.60286942E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87904218E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.25979953E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.92631404E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.16939554E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.63183052E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95442788E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.01153872E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.55641068E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.60286942E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87904218E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.25979953E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.92631404E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.16939554E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.63183052E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95442788E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.01153872E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.55641068E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.65103908E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91877511E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.56819405E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.63571579E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.92995362E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64636575E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.92802816E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.13803446E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.67816235E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.21771329E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.59770793E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96752509E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.30475867E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.23531431E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.00987200E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.59770793E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96752509E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.30475867E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.23531431E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.00987200E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.61533064E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94097241E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29861897E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.23202353E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.66843723E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.91559538E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51531439E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.07057129E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07953078E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07953078E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07272875E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08601046E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08601046E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05773695E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02830472E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78589869E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.93364098E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71655699E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71214958E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85216077E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31774848E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31780077E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.62189992E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31343797E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.28423662E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.03139626E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99709790E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.90210059E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.07175406E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.77244285E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99822756E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51529846E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.53223452E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78561323E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.62474455E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71619972E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71619972E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.31996077E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.54834467E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.60772656E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.09220045E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28669828E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.45675375E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84671114E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.29068767E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.26604087E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32153817E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32153817E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04281333E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04281333E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.81144926E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.81144926E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04281333E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04281333E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.81144926E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.81144926E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27089842E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27089842E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12339078E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12339078E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03999073E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03999073E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03999073E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03999073E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03999073E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03999073E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.22146236E-03   # h decays
#          BR         NDA      ID1       ID2
     8.46180772E-01    2           5        -5   # BR(h -> b       bb     )
     8.11334215E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87537611E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56991147E-04    2           3        -3   # BR(h -> s       sb     )
     2.77863270E-02    2           4        -4   # BR(h -> c       cb     )
     4.15088410E-02    2          21        21   # BR(h -> g       g      )
     1.08538292E-03    2          22        22   # BR(h -> gam     gam    )
     1.08840753E-03    2          24       -24   # BR(h -> W+      W-     )
     2.72319686E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85431034E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47942378E-04    2           5        -5   # BR(H -> b       bb     )
     3.77571879E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33472540E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85350803E-07    2           3        -3   # BR(H -> s       sb     )
     1.11069059E-05    2           4        -4   # BR(H -> c       cb     )
     9.96313593E-01    2           6        -6   # BR(H -> t       tb     )
     1.42488816E-03    2          21        21   # BR(H -> g       g      )
     4.73452691E-06    2          22        22   # BR(H -> gam     gam    )
     1.68595028E-06    2          23        22   # BR(H -> Z       gam    )
     3.76330315E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86159814E-04    2          23        23   # BR(H -> Z       Z      )
     1.39530807E-03    2          25        25   # BR(H -> h       h      )
     1.05648893E-18    2          36        36   # BR(H -> A       A      )
     1.67776015E-09    2          23        36   # BR(H -> Z       A      )
     4.49638104E-10    2          24       -37   # BR(H -> W+      H-     )
     4.49638104E-10    2         -24        37   # BR(H -> W-      H+     )
     1.72179650E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20171701E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40065166E-04    2           5        -5   # BR(A -> b       bb     )
     3.54691793E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25382842E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87671024E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308844E-05    2           4        -4   # BR(A -> c       cb     )
     9.97514795E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718538E-03    2          21        21   # BR(A -> g       g      )
     5.53878113E-06    2          22        22   # BR(A -> gam     gam    )
     2.07430867E-06    2          23        22   # BR(A -> Z       gam    )
     3.38139775E-04    2          23        25   # BR(A -> Z       h      )
     1.46188773E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05078155E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.98326361E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66525170E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29565911E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44581800E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84901413E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02839420E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99598652E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.53872774E-04    2          24        25   # BR(H+ -> W+      h      )
     1.14141418E-11    2          24        36   # BR(H+ -> W+      A      )
