from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "EvgenJobOptions/MC9_Herwig_Common.py" )
Herwig.HerwigCommand += [ "iproc 13000",
                          "susyfile susy_SU3.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "EvgenJobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "EvgenJobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from EvgenJobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from EvgenJobOptions.SUSYEvgenConfig import evgenConfig

#==============================================================
#
# End of job options file
#
###############################################################
