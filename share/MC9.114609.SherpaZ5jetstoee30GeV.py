# prepared by Frank Siegert, Aug'10
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()
# set alpha_s scale factor in parton shower (value recommended by Sherpa authors for W/Z)
params="""
CSS_AS_FS_FAC=0.4
CSS_AS_IS_FAC=0.4
"""
sherpa.Parameters = params.strip().splitlines()
#
topAlg += sherpa
from MC09JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010202.114609.SherpaZ5jetstoee30GeV.TXT.v1'
evgenConfig.efficiency = 0.9
