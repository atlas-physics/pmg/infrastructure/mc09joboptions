###############################################################
#
# Job options file for Wt at high pT + lepton filter
# G. Piacquadio & A. Davison
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
MissingEtFilter= MissingEtFilter(name = "MissingEtFilter",
                                 MEtcut = 100.*GeV)

topAlg += MissingEtFilter

try:
     StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc35.209716.Wt'
evgenConfig.efficiency = 0.0932
#0.0972 +/- 0.0008

#==============================================================
#
# End of job options file
#
###############################################################
