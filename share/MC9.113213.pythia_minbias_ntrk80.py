# ND min bias sample
# ( > 80 stable, charged particles with pT>100MeV, |eta|<2.5)
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()

ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 100.
ChargedTracksFilter.Etacut = 2.5
# NB filter cuts on nChargedTrack > nTrackCut (so this sample actually has >=81)
ChargedTracksFilter.NTracks = 80

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "ChargedTracksFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
# evgenConfig.efficiency = 0.9

# smaller minevents for sqrt(s)=900 GeV
evgenConfig.minevents=500
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.minevents=5000
except NameError:
  pass

evgenConfig.maxeventsfactor=10.
                          
#==============================================================
#
# End of job options file
#
###############################################################

