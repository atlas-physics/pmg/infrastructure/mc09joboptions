#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019026E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190261E+03   # EWSB                
         1     5.43800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05862345E+01   # W+
        25     7.05190436E+01   # h
        35     1.00936913E+03   # H
        36     1.00000000E+03   # A
        37     1.00328653E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.99864766E+02   # ~d_L
   2000001     6.00002536E+02   # ~d_R
   1000002     6.00174432E+02   # ~u_L
   2000002     6.00105035E+02   # ~u_R
   1000003     5.99864766E+02   # ~s_L
   2000003     6.00002536E+02   # ~s_R
   1000004     6.00174432E+02   # ~c_L
   2000004     6.00105035E+02   # ~c_R
   1000005     5.04726399E+03   # ~b_1
   2000005     5.04771153E+03   # ~b_2
   1000006     4.90968262E+03   # ~t_1
   2000006     5.04698240E+03   # ~t_2
   1000011     4.99998819E+03   # ~e_L
   2000011     4.99998831E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998819E+03   # ~mu_L
   2000013     4.99998831E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982373E+03   # ~tau_1
   2000015     5.00015337E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.15145045E+03   # ~g
   1000022     5.20016110E+02   # ~chi_10
   1000023    -1.01780283E+03   # ~chi_20
   1000025     1.02030822E+03   # ~chi_30
   1000035     4.96012236E+03   # ~chi_40
   1000024     1.01626223E+03   # ~chi_1+
   1000037     4.96012219E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.95946924E-01   # N_11
  1  2    -1.57791800E-03   # N_12
  1  3     6.29878394E-02   # N_13
  1  4    -6.41854169E-02   # N_14
  2  1     8.46664278E-04   # N_21
  2  2    -3.79657852E-04   # N_22
  2  3     7.07129933E-01   # N_23
  2  4     7.07083020E-01   # N_24
  3  1     8.99386942E-02   # N_31
  3  2     1.96975066E-02   # N_32
  3  3    -7.04128544E-01   # N_33
  3  4     7.04078144E-01   # N_34
  4  1     1.99763029E-04   # N_41
  4  2    -9.99804668E-01   # N_42
  4  3    -1.42402153E-02   # N_43
  4  4     1.37040905E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01364950E-02   # U_11
  1  2     9.99797240E-01   # U_12
  2  1     9.99797240E-01   # U_21
  2  2     2.01364950E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93781918E-02   # V_11
  1  2     9.99812225E-01   # V_12
  2  1     9.99812225E-01   # V_21
  2  2     1.93781918E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07095122E-01   # cos(theta_t)
  1  2     9.94248779E-01   # sin(theta_t)
  2  1    -9.94248779E-01   # -sin(theta_t)
  2  2     1.07095122E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19489272E-01   # cos(theta_b)
  1  2     6.94503555E-01   # sin(theta_b)
  2  1    -6.94503555E-01   # -sin(theta_b)
  2  2     7.19489272E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07238247E-01   # cos(theta_tau)
  1  2     7.06975291E-01   # sin(theta_tau)
  2  1    -7.06975291E-01   # -sin(theta_tau)
  2  2     7.07238247E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21984797E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190261E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43461176E-01   # tanbeta(Q)          
         3     2.44639315E+02   # vev(Q)              
         4     1.10576699E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190261E+03  # The gauge couplings
     1     3.66588931E-01   # gprime(Q) DRbar
     2     6.37166198E-01   # g(Q) DRbar
     3     1.01951675E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190261E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190261E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190261E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190261E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17164112E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190261E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87159270E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190261E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38835754E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190261E+03  # The soft SUSY breaking masses at the scale Q
         1     5.43800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04908861E+05   # M^2_Hd              
        22     6.79843211E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40108770E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.50581688E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24322120E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24322120E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24314235E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24314235E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24304395E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24304395E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24308368E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24308368E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24322120E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24322120E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24314235E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24314235E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24304395E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24304395E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24308368E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24308368E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.08123460E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.08123460E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.16236036E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.16236036E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.25817106E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.25817106E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48974891E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.73203312E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41589414E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.36926212E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.74164042E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26469060E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.15669668E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86581139E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88269184E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67486667E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.06552432E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.82755934E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.50048583E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.41375420E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.77950102E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53138690E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.45075986E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.05789539E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71827672E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.26686266E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.29390156E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.77306020E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43187926E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.93752242E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.07015499E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.71346322E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.20879540E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.32174450E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     1.18948029E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     7.52249406E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     2.96780700E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     7.32174450E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     1.18948029E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     7.52249406E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     2.96780700E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.57644880E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80995785E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.25801130E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.43932743E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.74047534E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.28841982E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.54844120E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.61472330E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.92402864E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.73481692E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.59646213E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.02931647E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.57644880E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80995785E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.25801130E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.43932743E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.74047534E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.28841982E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.54844120E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.61472330E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.92402864E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.73481692E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.59646213E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.02931647E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.64107401E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.88311104E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.58759365E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     8.68170588E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53598929E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.98726011E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.07571606E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.63657743E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.88726738E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.17350803E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.02822284E-02    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56736163E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.11146933E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.13835550E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.57127439E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.92649475E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.12122898E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.89860053E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.76375879E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.12099615E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.55243147E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.57127439E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.92649475E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.12122898E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.89860053E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.76375879E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.12099615E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.55243147E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.58887491E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.89997862E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.11556266E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.89085765E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.74301987E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.78806673E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.54679641E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.03423613E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.13237060E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     2.30490841E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     2.13237060E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     2.30490841E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.91125442E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.02236091E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.87873938E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.87873161E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.87873938E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.87873161E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.05041377E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47714543E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.30898361E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30725961E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.28553568E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.59645373E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.79036347E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.79032040E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.47915996E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.78925444E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.77626095E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.06223596E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99842611E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.47739213E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.45612283E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.45612283E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     1.30134203E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     1.30134203E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     9.07019638E-07    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     9.07019638E-07    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.25440603E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.25440603E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.45612283E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.45612283E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     1.30134203E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     1.30134203E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     9.07019638E-07    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     9.07019638E-07    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.25440603E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.25440603E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.19725095E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.56069171E-05    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.27222169E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.76109485E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     3.76109485E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.31188834E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.31188834E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.65413379E-05    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.65413379E-05    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.28077248E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.28077248E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     3.76109485E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     3.76109485E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.31188834E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.31188834E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.65413379E-05    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.65413379E-05    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.28077248E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.28077248E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.02236349E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.80452177E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47648212E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.64027040E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30630370E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30630370E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.46260060E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.73562804E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.47394987E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.68336806E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.78260844E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.60951488E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.57161828E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.65657509E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.77169368E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.79131202E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.79131202E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13784801E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13784801E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.14689031E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.14689031E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13943838E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13943838E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.36726264E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.36726264E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13784801E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13784801E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.14689031E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.14689031E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13943838E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13943838E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.36726264E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.36726264E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.86562525E-03   # h decays
#          BR         NDA      ID1       ID2
     8.61615058E-01    2           5        -5   # BR(h -> b       bb     )
     7.91535377E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80870175E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.69501967E-04    2           3        -3   # BR(h -> s       sb     )
     2.83282513E-02    2           4        -4   # BR(h -> c       cb     )
     2.89981477E-02    2          21        21   # BR(h -> g       g      )
     6.80351990E-04    2          22        22   # BR(h -> gam     gam    )
     2.11579863E-04    2          24       -24   # BR(h -> W+      W-     )
     6.27014624E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85398602E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46706202E-04    2           5        -5   # BR(H -> b       bb     )
     3.77549909E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33464774E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85342436E-07    2           3        -3   # BR(H -> s       sb     )
     1.11071206E-05    2           4        -4   # BR(H -> c       cb     )
     9.96307371E-01    2           6        -6   # BR(H -> t       tb     )
     1.42499071E-03    2          21        21   # BR(H -> g       g      )
     4.74059281E-06    2          22        22   # BR(H -> gam     gam    )
     1.68483815E-06    2          23        22   # BR(H -> Z       gam    )
     3.76700623E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86342155E-04    2          23        23   # BR(H -> Z       Z      )
     1.40228084E-03    2          25        25   # BR(H -> h       h      )
     1.00582819E-18    2          36        36   # BR(H -> A       A      )
     1.59672647E-09    2          23        36   # BR(H -> Z       A      )
     4.48948107E-10    2          24       -37   # BR(H -> W+      H-     )
     4.48948107E-10    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     5.20108409E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38935110E-04    2           5        -5   # BR(A -> b       bb     )
     3.54726887E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25395247E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87689775E-07    2           3        -3   # BR(A -> s       sb     )
     1.02323621E-05    2           4        -4   # BR(A -> c       cb     )
     9.97658873E-01    2           6        -6   # BR(A -> t       tb     )
     1.70743177E-03    2          21        21   # BR(A -> g       g      )
     5.53975454E-06    2          22        22   # BR(A -> gam     gam    )
     2.07323284E-06    2          23        22   # BR(A -> Z       gam    )
     3.41129426E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     5.05047468E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95987292E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66505743E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29559044E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43084748E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84866384E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02839949E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99595681E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.56847403E-04    2          24        25   # BR(H+ -> W+      h      )
     9.95386669E-12    2          24        36   # BR(H+ -> W+      A      )
