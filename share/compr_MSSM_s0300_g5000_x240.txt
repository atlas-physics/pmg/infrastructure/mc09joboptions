#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019063E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190629E+03   # EWSB                
         1     2.54400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.16000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05878048E+01   # W+
        25     7.14217194E+01   # h
        35     1.00939485E+03   # H
        36     1.00000000E+03   # A
        37     1.00334852E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.99615232E+02   # ~d_L
   2000001     2.99920058E+02   # ~d_R
   1000002     3.00297251E+02   # ~u_L
   2000002     3.00145475E+02   # ~u_R
   1000003     2.99615232E+02   # ~s_L
   2000003     2.99920058E+02   # ~s_R
   1000004     3.00297251E+02   # ~c_L
   2000004     3.00145475E+02   # ~c_R
   1000005     5.04614435E+03   # ~b_1
   2000005     5.04659362E+03   # ~b_2
   1000006     4.90994137E+03   # ~t_1
   2000006     5.04669318E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998828E+03   # ~e_R
   1000012     5.00002356E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998828E+03   # ~mu_R
   1000014     5.00002356E+03   # ~nu_muL
   1000015     4.99982260E+03   # ~tau_1
   2000015     5.00015445E+03   # ~tau_2
   1000016     5.00002356E+03   # ~nu_tauL
   1000021     5.12666812E+03   # ~g
   1000022     2.40028720E+02   # ~chi_10
   1000023    -1.01789424E+03   # ~chi_20
   1000025     1.01895771E+03   # ~chi_30
   1000035     4.96448131E+03   # ~chi_40
   1000024     1.01633377E+03   # ~chi_1+
   1000037     4.96448114E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98313154E-01   # N_11
  1  2    -9.63524852E-04   # N_12
  1  3     4.03120349E-02   # N_13
  1  4    -4.17714879E-02   # N_14
  2  1     1.03119232E-03   # N_21
  2  2    -3.77607310E-04   # N_22
  2  3     7.07132814E-01   # N_23
  2  4     7.07079895E-01   # N_24
  3  1     5.80495151E-02   # N_31
  3  2     1.98518290E-02   # N_32
  3  3    -7.05785425E-01   # N_33
  3  4     7.05764190E-01   # N_34
  4  1     1.90137703E-04   # N_41
  4  2    -9.99802397E-01   # N_42
  4  3    -1.43198214E-02   # N_43
  4  4     1.37866836E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.02491738E-02   # U_11
  1  2     9.99794964E-01   # U_12
  2  1     9.99794964E-01   # U_21
  2  2     2.02491738E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94950891E-02   # V_11
  1  2     9.99809953E-01   # V_12
  2  1     9.99809953E-01   # V_21
  2  2     1.94950891E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04840670E-01   # cos(theta_t)
  1  2     9.94489032E-01   # sin(theta_t)
  2  1    -9.94489032E-01   # -sin(theta_t)
  2  2     1.04840670E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19480937E-01   # cos(theta_b)
  1  2     6.94512189E-01   # sin(theta_b)
  2  1    -6.94512189E-01   # -sin(theta_b)
  2  2     7.19480937E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07222878E-01   # cos(theta_tau)
  1  2     7.06990665E-01   # sin(theta_tau)
  2  1    -7.06990665E-01   # -sin(theta_tau)
  2  2     7.07222878E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21642794E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190629E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44101132E-01   # tanbeta(Q)          
         3     2.46224891E+02   # vev(Q)              
         4     1.10403842E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190629E+03  # The gauge couplings
     1     3.66882789E-01   # gprime(Q) DRbar
     2     6.37425686E-01   # g(Q) DRbar
     3     1.02854004E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190629E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190629E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190629E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16479074E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86374928E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190629E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38818141E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190629E+03  # The soft SUSY breaking masses at the scale Q
         1     2.54400000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.16000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.06025029E+05   # M^2_Hd              
        22     6.63965049E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40216058E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.89086611E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24567974E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24567974E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24559258E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24559258E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24548461E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24548461E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24552807E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24552807E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24567974E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24567974E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24559258E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24559258E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24548461E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24548461E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24552807E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24552807E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     6.41271695E-05    2     1000005        -5   # BR(~g -> ~b_1  bb)
     6.41271695E-05    2    -1000005         5   # BR(~g -> ~b_1* b )
     7.05125242E-05    2     2000005        -5   # BR(~g -> ~b_2  bb)
     7.05125242E-05    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.19660251E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.19660251E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.46267339E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.69417956E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41215720E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38199163E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73643322E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.24849298E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.39031334E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86687242E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89349269E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.63791326E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.70972815E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.84074208E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.89965022E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.33098274E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.85416749E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.54888392E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.13241766E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.84073129E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71110200E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.04077971E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.22150600E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.87504233E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42739005E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.49795201E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.85237618E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70667289E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.95484487E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.43172925E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     1.19663595E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     7.45547444E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     2.97491843E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     7.43172925E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     1.19663595E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     7.45547444E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     2.97491843E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.69908528E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88052740E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.29215632E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.86186944E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.03813748E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27356245E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.20788548E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66481396E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96888233E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.81817432E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.11078556E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.27634109E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.69908528E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88052740E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.29215632E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.86186944E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.03813748E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27356245E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.20788548E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66481396E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96888233E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.81817432E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.11078556E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.27634109E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67222574E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92877724E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.51017106E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25084407E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.19612915E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.96122552E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.39576868E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66777855E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94206521E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.05274862E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.91828436E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.22322188E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.61106067E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.44986799E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.69335480E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95561071E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.61623880E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.09969567E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.05792607E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10920108E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.21134988E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.69335480E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95561071E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.61623880E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.09969567E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.05792607E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10920108E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.21134988E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.71095048E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92950774E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.60937919E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.08632460E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.04204259E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72707457E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.20670516E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.35760669E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.50345298E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     4.86250167E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     4.50345298E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     4.86250167E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.81268091E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.07089555E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88301193E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88300222E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88301193E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88300222E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.16665137E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47491428E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.36796760E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30409983E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.29813760E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.53049108E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76627778E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76621973E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.90309504E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76514803E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76104108E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.56307953E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99460858E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.12496064E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     2.01636888E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     2.01636888E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     3.90821154E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     3.90821154E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     2.06771808E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     2.06771808E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     9.77295110E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     9.77295110E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     2.01636888E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     2.01636888E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     3.90821154E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     3.90821154E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     2.06771808E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     2.06771808E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     9.77295110E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     9.77295110E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.04568934E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.95744319E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.24852402E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     5.48690859E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     5.48690859E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.13410960E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.13410960E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     4.34247539E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     4.34247539E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.83597522E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.83597522E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     5.48690859E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     5.48690859E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.13410960E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.13410960E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     4.34247539E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     4.34247539E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.83597522E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.83597522E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.07089671E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.40582658E-10    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47424093E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.60734397E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30319414E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30319414E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.74955678E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.84906671E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     5.87610530E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.59770869E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.75868829E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.47535295E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.52257626E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.50081251E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75656680E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76722780E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76722780E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.10385029E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.10385029E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.93927543E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.93927543E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.10532250E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.10532250E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.84821382E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.84821382E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.10385029E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.10385029E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.93927543E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.93927543E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.10532250E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.10532250E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.84821382E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.84821382E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88633159E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60812240E-01    2           5        -5   # BR(h -> b       bb     )
     7.92945882E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81343782E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68775471E-04    2           3        -3   # BR(h -> s       sb     )
     2.82968816E-02    2           4        -4   # BR(h -> c       cb     )
     2.96445635E-02    2          21        21   # BR(h -> g       g      )
     7.00170014E-04    2          22        22   # BR(h -> gam     gam    )
     2.32809276E-04    2          24       -24   # BR(h -> W+      W-     )
     6.86285115E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86201333E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46634121E-04    2           5        -5   # BR(H -> b       bb     )
     3.77454668E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33431105E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85302631E-07    2           3        -3   # BR(H -> s       sb     )
     1.10740268E-05    2           4        -4   # BR(H -> c       cb     )
     9.93345875E-01    2           6        -6   # BR(H -> t       tb     )
     1.42051257E-03    2          21        21   # BR(H -> g       g      )
     4.46263015E-06    2          22        22   # BR(H -> gam     gam    )
     1.68045255E-06    2          23        22   # BR(H -> Z       gam    )
     3.75753652E-04    2          24       -24   # BR(H -> W+      W-     )
     1.85874096E-04    2          23        23   # BR(H -> Z       Z      )
     1.39888133E-03    2          25        25   # BR(H -> h       h      )
     1.01384307E-18    2          36        36   # BR(H -> A       A      )
     1.61613893E-09    2          23        36   # BR(H -> Z       A      )
     4.35008907E-10    2          24       -37   # BR(H -> W+      H-     )
     4.35008907E-10    2         -24        37   # BR(H -> W-      H+     )
     3.31931873E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.56500594E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     9.46380554E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     5.56500594E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     9.46380554E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     8.10618802E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     2.36692314E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     8.10618802E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     2.36692314E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.19474589E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39533529E-04    2           5        -5   # BR(A -> b       bb     )
     3.55641673E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25718622E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88162910E-07    2           3        -3   # BR(A -> s       sb     )
     1.02309626E-05    2           4        -4   # BR(A -> c       cb     )
     9.97522422E-01    2           6        -6   # BR(A -> t       tb     )
     1.70720938E-03    2          21        21   # BR(A -> g       g      )
     5.53907138E-06    2          22        22   # BR(A -> gam     gam    )
     2.07376343E-06    2          23        22   # BR(A -> Z       gam    )
     3.41089048E-04    2          23        25   # BR(A -> Z       h      )
     1.36024269E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05715007E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95960851E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66541324E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29571622E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43094939E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84943562E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02575341E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.96981465E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.55968920E-04    2          24        25   # BR(H+ -> W+      h      )
     1.09136540E-11    2          24        36   # BR(H+ -> W+      A      )
     1.30755865E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.30755865E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
