###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# AMBT1 "MARKUS1" tune - for systematic studies
# reference JO: MC9.115001.pythia_minbias_AMBT1.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_PythiaAMBT1_Common.py" )

#--------------------------------------------------------------
# AMBT1 systematic variation: MARKUS1
Pythia.PythiaCommand += [
                              "pypars parp 84 0.45",
                              "pypars parp 90 0.193"]

#--------------------------------------------------------------

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

