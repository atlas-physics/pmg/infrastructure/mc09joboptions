#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018662E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00186624E+03   # EWSB                
         1     5.63000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05851090E+01   # W+
        25     6.97798780E+01   # h
        35     1.00921700E+03   # H
        36     1.00000000E+03   # A
        37     1.00333735E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.49621452E+02   # ~d_L
   2000001     1.49976937E+02   # ~d_R
   1000002     1.50416867E+02   # ~u_L
   2000002     1.50240522E+02   # ~u_R
   1000003     1.49621452E+02   # ~s_L
   2000003     1.49976937E+02   # ~s_R
   1000004     1.50416867E+02   # ~c_L
   2000004     1.50240522E+02   # ~c_R
   1000005     5.04673171E+03   # ~b_1
   2000005     5.04717832E+03   # ~b_2
   1000006     4.91361507E+03   # ~t_1
   2000006     5.04876527E+03   # ~t_2
   1000011     4.99998847E+03   # ~e_L
   2000011     4.99998853E+03   # ~e_R
   1000012     5.00002300E+03   # ~nu_eL
   1000013     4.99998847E+03   # ~mu_L
   2000013     4.99998853E+03   # ~mu_R
   1000014     5.00002300E+03   # ~nu_muL
   1000015     4.99982268E+03   # ~tau_1
   2000015     5.00015492E+03   # ~tau_2
   1000016     5.00002300E+03   # ~nu_tauL
   1000021     5.12980209E+03   # ~g
   1000022     1.19948563E+02   # ~chi_10
   1000023    -1.01805533E+03   # ~chi_20
   1000025     1.01860850E+03   # ~chi_30
   1000035     4.96446885E+03   # ~chi_40
   1000024     1.01649306E+03   # ~chi_1+
   1000037     4.96446869E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98906224E-01   # N_11
  1  2    -7.46089711E-04   # N_12
  1  3     3.22087361E-02   # N_13
  1  4    -3.38880095E-02   # N_14
  2  1     1.18655229E-03   # N_21
  2  2    -3.68378744E-04   # N_22
  2  3     7.07134915E-01   # N_23
  2  4     7.07077555E-01   # N_24
  3  1     4.67430746E-02   # N_31
  3  2     1.98719270E-02   # N_32
  3  3    -7.06199491E-01   # N_33
  3  4     7.06188693E-01   # N_34
  4  1     1.83200452E-04   # N_41
  4  2    -9.99802188E-01   # N_42
  4  3    -1.43209016E-02   # N_43
  4  4     1.38008712E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.02507767E-02   # U_11
  1  2     9.99794932E-01   # U_12
  2  1     9.99794932E-01   # U_21
  2  2     2.02507767E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.95152284E-02   # V_11
  1  2     9.99809560E-01   # V_12
  2  1     9.99809560E-01   # V_21
  2  2     1.95152284E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03006603E-01   # cos(theta_t)
  1  2     9.94680672E-01   # sin(theta_t)
  2  1    -9.94680672E-01   # -sin(theta_t)
  2  2     1.03006603E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19260572E-01   # cos(theta_b)
  1  2     6.94740404E-01   # sin(theta_b)
  2  1    -6.94740404E-01   # -sin(theta_b)
  2  2     7.19260572E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07170123E-01   # cos(theta_tau)
  1  2     7.07043434E-01   # sin(theta_tau)
  2  1    -7.07043434E-01   # -sin(theta_tau)
  2  2     7.07170123E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20719609E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00186624E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45463097E-01   # tanbeta(Q)          
         3     2.46293032E+02   # vev(Q)              
         4     1.10142506E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00186624E+03  # The gauge couplings
     1     3.67447494E-01   # gprime(Q) DRbar
     2     6.37537616E-01   # g(Q) DRbar
     3     1.04372192E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15126472E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.84953502E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38836661E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00186624E+03  # The soft SUSY breaking masses at the scale Q
         1     5.63000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.06150343E+05   # M^2_Hd              
        22     6.36697286E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40277078E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.32042725E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24557156E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24557156E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24552097E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24552097E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24545818E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24545818E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24548337E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24548337E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24557156E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24557156E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24552097E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24552097E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24545818E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24545818E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24548337E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24548337E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     6.79826899E-05    2     1000005        -5   # BR(~g -> ~b_1  bb)
     6.79826899E-05    2    -1000005         5   # BR(~g -> ~b_1* b )
     7.45083150E-05    2     2000005        -5   # BR(~g -> ~b_2  bb)
     7.45083150E-05    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.16827330E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.16827330E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.40950738E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.77219421E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.40867252E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38341328E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73069478E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21934078E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.48620797E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86671004E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89448115E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.61765034E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.99845787E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.87014497E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.13097448E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.18012505E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.93627494E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.57738595E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.52091180E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.91655664E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.70219065E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.83299391E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.08565340E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.96070287E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43477265E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.79112221E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.92806473E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.69852736E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.75161016E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.93507755E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     6.29087284E-02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     3.84250362E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.55240835E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     3.93507755E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     6.29087284E-02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     3.84250362E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.55240835E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.74357350E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.89903139E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.74157483E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.03526068E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.00475134E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.25965354E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.20119732E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.68252334E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97988721E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.29530663E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.00998371E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.73594707E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.74357350E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.89903139E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.74157483E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.03526068E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.00475134E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.25965354E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.20119732E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.68252334E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97988721E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.29530663E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.00998371E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.73594707E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.68341314E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93981539E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.49723958E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.16271096E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.18927103E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94889754E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.38200949E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67875059E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95620272E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.00425013E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.51137145E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.21672252E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.57569710E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.43683157E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.73749943E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95951752E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.05245057E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.37733378E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.02431726E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10039335E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.20463757E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.73749943E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95951752E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.05245057E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.37733378E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.02431726E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10039335E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.20463757E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.75509931E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93356877E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.04449764E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.37374525E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.00862137E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.70180265E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.20003945E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.02574941E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.83893278E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     7.36726224E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     6.83893278E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     7.36726224E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.71587610E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.07912960E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88405346E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88401977E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88405346E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88401977E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.00400488E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47213321E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.91489326E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.29879236E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.29629184E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.52037088E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76189949E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76182507E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.57031296E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76075975E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.75887731E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.12232187E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99034318E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.15475573E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.86285872E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.86285872E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     7.85568616E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     7.85568616E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     3.36095108E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     3.36095108E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     1.96416241E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     1.96416241E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.86285872E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.86285872E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     7.85568616E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     7.85568616E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     3.36095108E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     3.36095108E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     1.96416241E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     1.96416241E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     8.96965518E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.17220259E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.27194314E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.24813228E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     6.24813228E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     9.68548768E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     9.68548768E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     8.90737253E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     8.90737253E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.42166853E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.42166853E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     6.24813228E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     6.24813228E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     9.68548768E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     9.68548768E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     8.90737253E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     8.90737253E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.42166853E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.42166853E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.07913482E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.30517823E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47149814E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52754161E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.29791659E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.29791659E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.23147155E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.19935522E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.54920219E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.46145202E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.75458434E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.29551801E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.51619208E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.21939514E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75455445E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76284980E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76284980E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.08345646E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.08345646E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.80120576E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.80120576E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.08476086E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.08476086E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.50301856E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.50301856E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.08345646E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.08345646E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.80120576E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.80120576E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.08476086E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.08476086E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.50301856E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.50301856E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.84811768E-03   # h decays
#          BR         NDA      ID1       ID2
     8.62228892E-01    2           5        -5   # BR(h -> b       bb     )
     7.90312709E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80459058E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.70069355E-04    2           3        -3   # BR(h -> s       sb     )
     2.83765275E-02    2           4        -4   # BR(h -> c       cb     )
     2.84966936E-02    2          21        21   # BR(h -> g       g      )
     6.62246702E-04    2          22        22   # BR(h -> gam     gam    )
     1.95624553E-04    2          24       -24   # BR(h -> W+      W-     )
     5.82162550E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84797290E+01   # H decays
#          BR         NDA      ID1       ID2
     2.48135939E-04    2           5        -5   # BR(H -> b       bb     )
     3.79748602E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34242018E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86452343E-07    2           3        -3   # BR(H -> s       sb     )
     1.10684945E-05    2           4        -4   # BR(H -> c       cb     )
     9.92801190E-01    2           6        -6   # BR(H -> t       tb     )
     1.41983493E-03    2          21        21   # BR(H -> g       g      )
     4.62821455E-06    2          22        22   # BR(H -> gam     gam    )
     1.68053049E-06    2          23        22   # BR(H -> Z       gam    )
     3.56469767E-04    2          24       -24   # BR(H -> W+      W-     )
     1.76333818E-04    2          23        23   # BR(H -> Z       Z      )
     1.40382529E-03    2          25        25   # BR(H -> h       h      )
     9.16179647E-19    2          36        36   # BR(H -> A       A      )
     1.47307194E-09    2          23        36   # BR(H -> Z       A      )
     3.79388952E-10    2          24       -37   # BR(H -> W+      H-     )
     3.79388952E-10    2         -24        37   # BR(H -> W-      H+     )
     3.67337710E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.62967043E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.12801332E-04    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     6.62967043E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     1.12801332E-04    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     9.65110378E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     2.82051408E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     9.65110378E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     2.82051408E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.17951298E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40906894E-04    2           5        -5   # BR(A -> b       bb     )
     3.57717474E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26452414E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89237941E-07    2           3        -3   # BR(A -> s       sb     )
     1.02315104E-05    2           4        -4   # BR(A -> c       cb     )
     9.97575837E-01    2           6        -6   # BR(A -> t       tb     )
     1.70732452E-03    2          21        21   # BR(A -> g       g      )
     5.53940729E-06    2          22        22   # BR(A -> gam     gam    )
     2.07249903E-06    2          23        22   # BR(A -> Z       gam    )
     3.24005470E-04    2          23        25   # BR(A -> Z       h      )
     9.79954835E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04501348E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97937178E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68479853E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30256888E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44364830E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89623793E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02535625E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.96493565E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.37945507E-04    2          24        25   # BR(H+ -> W+      h      )
     1.07587643E-11    2          24        36   # BR(H+ -> W+      A      )
     1.56042397E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.56042397E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
