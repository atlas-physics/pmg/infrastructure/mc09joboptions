#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy gg->bt_H+/ (mH+=400GeV,tan(beta)=7) 
# 3 photon EF
#
# Responsible person(s)
#   23 Jan, 2008 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

include("MC09JobOptions/MC9_Herwig_Common_14TeV.py")
topAlg.Herwig.HerwigCommand += [ "iproc 3839",              #iproc gg->bt_H+ + ch. conjg
                                  "susyfile susy_mHp400tanb7.txt",#isawig input file
                                  "taudec TAUOLA",           #taudec tau dcay package
                                  "effmin 0.0000000001"]     #To fix early termination
#Setup tauola and photos. 
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )
                                 
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg +=MultiLeptonFilter()
topAlg.MultiLeptonFilter.Ptcut = 7000.
topAlg.MultiLeptonFilter.NLeptons = 3
#Also, Eta is cut at 10.0 by default

try:
  StreamEVGEN.RequireAlgs = ["MultiLeptonFilter"]
except Exception, e:
  pass

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency =  0.144
from MC09JobOptions.SUSYEvgenConfig import evgenConfig
# 5000/31114=0.1607 (x0.9=0.144)
#==============================================================
#
# End of job options file
#
#####################################
