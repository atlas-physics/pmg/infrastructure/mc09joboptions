# Alpgen Z(->mumu)+bb+3p
#--------------------------------------------------------------
# File prepared by Soshi Tsuno (Soshi.Tsuno@cern.ch) Dec 2008
#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA"
                        ]
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# no filter so far.

from MC09JobOptions.AlpgenEvgenConfig import evgenConfig
 
# input file names need updating for MC9
#evgenConfig.inputfilebase = 'alpgen.109308.ZmumubbNp3_pt20_nofilter'
evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.109308.ZmumubbNp3_pt20_nofilter_7Tev.TXT.v1'

# 7TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.1538
# Alpgen cross section = 2.45526797+-0.01083370 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.3777 pb
# Integrated Luminosity = 13237.2517 pb-1
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 41000 events
#
# 10TeV
# Filter efficiency  = 1.0
# MLM matching efficiency = 0.1467
# Alpgen cross section = 6.38472925+-0.03026015 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 0.9369 pb
# Number of events in 4-vector file of Alpgen = 5000/(eff*0.8) = 43000 events
#
# Filter efficiency x safety factor = eff x 90% = 0.90
evgenConfig.efficiency = 0.90
evgenConfig.minevents=5000
#==============================================================
#
# End of job options file
#
###############################################################
