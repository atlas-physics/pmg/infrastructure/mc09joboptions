# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg") 


include ( "MC09JobOptions/MC9_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by Oldrich Kepka 2012
#--------------------------------------------------------------
Pythia.PythiaCommand += [
                              "pysubs msel 1"]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
topAlg += ChargedTracksFilter()

ChargedTracksFilter = topAlg.ChargedTracksFilter
ChargedTracksFilter.Ptcut = 100.
ChargedTracksFilter.Etacut = 2.5
ChargedTracksFilter.NTracks = 150

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "ChargedTracksFilter" ]
except Exception, e:
     pass

                             
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
evgenConfig.minevents = 100
