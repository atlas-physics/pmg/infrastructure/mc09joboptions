###############################################################
#
# Job options file
# Created by Pavel Staroba on the request of Lashkar Kashif
# in Rel. 14.2.23.4 (December 2008)
# MC9.106034.McAtNloWplustaunu_1Lepton.py without filter
#
#===============================================================

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 
# needed (dummy) default 
from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()
Herwig = topAlg.Herwig 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  pass

Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment_LeptonicDecay.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC09JobOptions.McAtNloEvgenConfig  import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'mcatnlo33.106034.McAtNloWplustaunu_1Lepton'
evgenConfig.efficiency = 0.90
#==============================================================
#
# End of job options file
#
###############################################################
