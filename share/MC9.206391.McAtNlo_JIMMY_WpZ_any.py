###############################################################
#
# Job options file
# WpZ -> any production with MC@NLO 3.10 and Herwig/Herwig
#
# Author : Martin Schmitz mschmitz@cern.ch
#


# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_14TeV.py" )
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )
###Tauola.TauolaCommand += [ "tauola polar 0" ] # remove this line from rel14

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )



from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0310.206391.WpZ_any_v3'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
