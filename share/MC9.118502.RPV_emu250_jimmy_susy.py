from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_HerwigRpv_Common_7TeV.py" )
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_HerwigRpv_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.HerwigRpv_iConf import HerwigRpv
  Herwig = HerwigRpv("Herwig")
  topAlg += Herwig


Herwig.HerwigCommand += [ "iproc 14080",
                          "susyfile susy_RPV_emu250.txt",
                          "taudec TAUOLA" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos (switch off for MC09; consistency with 106486-106489, 106498)
# include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC09JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
