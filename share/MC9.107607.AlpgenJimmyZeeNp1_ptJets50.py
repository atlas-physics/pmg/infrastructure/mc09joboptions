###############################################################
#
# Job options file
#
# Alpgen Z->ee+1parton (exclusive) no filtering cut applied,
# 
#
# Responsible person(s)
#	Louis Helary
#==============================================================
# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig


Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------



#Information on sample:
#Filter efficiency  = 1.0000
#MLM matching efficiency = 0.585
#Number of Matrix Elements in input file  = 1100
#Alpgen cross section = 55.6 pb
#Herwig cross section = Alpgen cross section * eff(MLM) =32.7pb
#Cross section after filtering = 32.7pb
#Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) =

# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing

from MC09JobOptions.AlpgenEvgenConfig import evgenConfig 
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'MC9.107607.AlpgenJimmyZeeNp1_ptJets50'
evgenConfig.efficiency = 0.454



#==============================================================
#
# End of job options file
#
###############################################################



