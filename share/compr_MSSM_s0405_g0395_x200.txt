#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018494E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00184937E+03   # EWSB                
         1     2.10000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05883020E+01   # W+
        25     6.85195959E+01   # h
        35     1.00913532E+03   # H
        36     1.00000000E+03   # A
        37     1.00327174E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.04744623E+02   # ~d_L
   2000001     4.04931853E+02   # ~d_R
   1000002     4.05165460E+02   # ~u_L
   2000002     4.05071280E+02   # ~u_R
   1000003     4.04744623E+02   # ~s_L
   2000003     4.04931853E+02   # ~s_R
   1000004     4.05165460E+02   # ~c_L
   2000004     4.05071280E+02   # ~c_R
   1000005     5.04577532E+03   # ~b_1
   2000005     5.04621990E+03   # ~b_2
   1000006     4.91241020E+03   # ~t_1
   2000006     5.04748387E+03   # ~t_2
   1000011     4.99998854E+03   # ~e_L
   2000011     4.99998865E+03   # ~e_R
   1000012     5.00002281E+03   # ~nu_eL
   1000013     4.99998854E+03   # ~mu_L
   2000013     4.99998865E+03   # ~mu_R
   1000014     5.00002281E+03   # ~nu_muL
   1000015     4.99982377E+03   # ~tau_1
   2000015     5.00015402E+03   # ~tau_2
   1000016     5.00002281E+03   # ~nu_tauL
   1000021     3.95271046E+02   # ~g
   1000022     1.99895773E+02   # ~chi_10
   1000023    -1.01803797E+03   # ~chi_20
   1000025     1.01896073E+03   # ~chi_30
   1000035     4.96110074E+03   # ~chi_40
   1000024     1.01649347E+03   # ~chi_1+
   1000037     4.96110057E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98491670E-01   # N_11
  1  2    -8.99047127E-04   # N_12
  1  3     3.80763903E-02   # N_13
  1  4    -3.95444676E-02   # N_14
  2  1     1.03729158E-03   # N_21
  2  2    -3.67904691E-04   # N_22
  2  3     7.07132495E-01   # N_23
  2  4     7.07080210E-01   # N_24
  3  1     5.48932949E-02   # N_31
  3  2     1.97616966E-02   # N_32
  3  3    -7.05911355E-01   # N_33
  3  4     7.05893307E-01   # N_34
  4  1     1.86748511E-04   # N_41
  4  2    -9.99804247E-01   # N_42
  4  3    -1.42471848E-02   # N_43
  4  4     1.37277508E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01464968E-02   # U_11
  1  2     9.99797039E-01   # U_12
  2  1     9.99797039E-01   # U_21
  2  2     2.01464968E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94117955E-02   # V_11
  1  2     9.99811573E-01   # V_12
  2  1     9.99811573E-01   # V_21
  2  2     1.94117955E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00298360E-01   # cos(theta_t)
  1  2     9.94957406E-01   # sin(theta_t)
  2  1    -9.94957406E-01   # -sin(theta_t)
  2  2     1.00298360E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19232235E-01   # cos(theta_b)
  1  2     6.94769740E-01   # sin(theta_b)
  2  1    -6.94769740E-01   # -sin(theta_b)
  2  2     7.19232235E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07216077E-01   # cos(theta_tau)
  1  2     7.06997468E-01   # sin(theta_tau)
  2  1    -7.06997468E-01   # -sin(theta_tau)
  2  2     7.07216077E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20778784E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00184937E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45230734E-01   # tanbeta(Q)          
         3     2.44821704E+02   # vev(Q)              
         4     1.10389815E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00184937E+03  # The gauge couplings
     1     3.66961088E-01   # gprime(Q) DRbar
     2     6.37497529E-01   # g(Q) DRbar
     3     1.04303179E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00184937E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00184937E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00184937E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00184937E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15309002E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00184937E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85147613E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00184937E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38852506E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00184937E+03  # The soft SUSY breaking masses at the scale Q
         1     2.10000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04813357E+05   # M^2_Hd              
        22     6.41094909E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40245495E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.21736621E-03   # gluino decays
#          BR         NDA      ID1       ID2
     3.33155009E-07    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.14453295E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.85546218E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.14453295E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.85546218E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.40255525E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.61793825E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.06251990E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07460742E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.06482327E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.12896014E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.52535718E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.43941913E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.80837816E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.39722225E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.40109289E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.51597915E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.13403693E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.15521298E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.50421165E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.90058358E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.84313859E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.67850894E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.31164380E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.72274788E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.26134091E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.67000979E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.28122272E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.52822378E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.80320283E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.81713895E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.08085927E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.17680918E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.12663680E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.58189096E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.36768235E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.16864721E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.31071149E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     2.61583062E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.38416938E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     6.48489384E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     8.53512926E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.46487074E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.23771108E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81652650E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.18347350E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.30649553E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.99501694E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.00498306E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.31071149E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.61583062E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.38416938E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     6.48489384E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     8.53512926E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.46487074E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.23771108E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81652650E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.18347350E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.30649553E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.99501694E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.00498306E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.71373579E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.88290677E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.45302377E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.29565131E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.22375257E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.24609838E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.44505299E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66979013E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97221301E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.92013675E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.77770691E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.40181603E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.71373579E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.88290677E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.45302377E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.29565131E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.22375257E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.24609838E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.44505299E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66979013E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97221301E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.92013675E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.77770691E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.40181603E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67544259E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93157395E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.50078977E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.91373437E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.43287584E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94856980E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.86934011E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67100542E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.94557135E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.04700095E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.49598466E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46284456E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.97713959E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92918437E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.70801196E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95349115E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.57817706E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.87820039E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.24568129E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.08706041E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.44885853E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.70801196E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95349115E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.57817706E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.87820039E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.24568129E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.08706041E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.44885853E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.72561600E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92743827E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.57142879E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.86804935E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.22671600E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.70051950E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.44360533E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.82507589E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.71898653E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     4.00874835E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.71898653E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     4.00874835E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.84544530E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06336456E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88544994E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88543991E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88544994E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88543991E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.91435481E-07    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.43515080E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.26713445E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.26558738E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.26070394E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.54087795E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76852047E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76845265E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     5.24948993E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76740590E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76393818E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.05282884E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99395554E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.82403136E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.37347290E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.37347290E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     3.05298336E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     3.05298336E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.54901709E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.54901709E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     7.63415522E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     7.63415522E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.37347290E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.37347290E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     3.05298336E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     3.05298336E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.54901709E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.54901709E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     7.63415522E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     7.63415522E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     9.81534003E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.35859200E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.42241265E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.08274139E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.08274139E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     7.89753493E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     7.89753493E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     3.79164278E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     3.79164278E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     1.97482171E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     1.97482171E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.08274139E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.08274139E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     7.89753493E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     7.89753493E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     3.79164278E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     3.79164278E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     1.97482171E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     1.97482171E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06336854E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.80757161E-11    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.43452778E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52678131E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.26470346E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.26470346E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.66631274E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.53259416E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     5.22514131E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.47924281E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76119057E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.33951855E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53413736E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.25509935E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75958414E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76947096E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76947096E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.16754116E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.16754116E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.88421613E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.88421613E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.16903334E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.16903334E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.71057060E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.71057060E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.16754116E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.16754116E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.88421613E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.88421613E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.16903334E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.16903334E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.71057060E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.71057060E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.81717875E-03   # h decays
#          BR         NDA      ID1       ID2
     8.63163553E-01    2           5        -5   # BR(h -> b       bb     )
     7.89041324E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80048299E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.71766548E-04    2           3        -3   # BR(h -> s       sb     )
     2.84571694E-02    2           4        -4   # BR(h -> c       cb     )
     2.76634921E-02    2          21        21   # BR(h -> g       g      )
     6.37557179E-04    2          22        22   # BR(h -> gam     gam    )
     1.71016633E-04    2          24       -24   # BR(h -> W+      W-     )
     5.12649363E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84285062E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49532256E-04    2           5        -5   # BR(H -> b       bb     )
     3.79983046E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34324894E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86567688E-07    2           3        -3   # BR(H -> s       sb     )
     1.10835391E-05    2           4        -4   # BR(H -> c       cb     )
     9.94128352E-01    2           6        -6   # BR(H -> t       tb     )
     1.42211774E-03    2          21        21   # BR(H -> g       g      )
     4.40154063E-06    2          22        22   # BR(H -> gam     gam    )
     1.68488017E-06    2          23        22   # BR(H -> Z       gam    )
     3.50571097E-04    2          24       -24   # BR(H -> W+      W-     )
     1.73416165E-04    2          23        23   # BR(H -> Z       Z      )
     1.40665598E-03    2          25        25   # BR(H -> h       h      )
     8.78569242E-19    2          36        36   # BR(H -> A       A      )
     1.41056157E-09    2          23        36   # BR(H -> Z       A      )
     3.74631120E-10    2          24       -37   # BR(H -> W+      H-     )
     3.74631120E-10    2         -24        37   # BR(H -> W-      H+     )
     3.42197855E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.14495046E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.04901523E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.14495046E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.04901523E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.04141652E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.76335297E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.04141652E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.76335297E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.18218794E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41836515E-04    2           5        -5   # BR(A -> b       bb     )
     3.57357109E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26325026E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89051256E-07    2           3        -3   # BR(A -> s       sb     )
     1.02312575E-05    2           4        -4   # BR(A -> c       cb     )
     9.97551174E-01    2           6        -6   # BR(A -> t       tb     )
     1.70727826E-03    2          21        21   # BR(A -> g       g      )
     5.53921202E-06    2          22        22   # BR(A -> gam     gam    )
     2.07408939E-06    2          23        22   # BR(A -> Z       gam    )
     3.18394735E-04    2          23        25   # BR(A -> Z       h      )
     1.27421189E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04115034E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00393029E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68556889E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30284120E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45924158E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89818594E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02657137E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97687829E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32450074E-04    2          24        25   # BR(H+ -> W+      h      )
     9.75003425E-12    2          24        36   # BR(H+ -> W+      A      )
     9.66028814E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     9.66028814E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
