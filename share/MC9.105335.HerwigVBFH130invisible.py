###############################################################
#
# Job options file
#
# VBF Invisible Higgs production with H(130)->ZZ->4nu
#
# Responsible person(s)
#   8 Oct, 2008-xx xxx, 20xx: Malachi Schram (malachi.schram@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Herwig
Herwig.HerwigCommand += [ "iproc 11911", # VBF Higgs with h->ZZ decay
                          "modbos 1 6", # Z(1)->nunu
                          "modbos 2 6", # Z(2)->nunu
                          "rmass 201 130.0", # Higgs mass
                          "taudec TAUOLA" # for tau decay  
		        ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
