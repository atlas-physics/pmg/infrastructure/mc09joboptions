#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018494E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00184942E+03   # EWSB                
         1     2.82900000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05882957E+01   # W+
        25     6.85118062E+01   # h
        35     1.00913630E+03   # H
        36     1.00000000E+03   # A
        37     1.00327553E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.04744171E+02   # ~d_L
   2000001     4.04931402E+02   # ~d_R
   1000002     4.05165011E+02   # ~u_L
   2000002     4.05070830E+02   # ~u_R
   1000003     4.04744171E+02   # ~s_L
   2000003     4.04931402E+02   # ~s_R
   1000004     4.05165011E+02   # ~c_L
   2000004     4.05070830E+02   # ~c_R
   1000005     5.04577456E+03   # ~b_1
   2000005     5.04621914E+03   # ~b_2
   1000006     4.91241545E+03   # ~t_1
   2000006     5.04748448E+03   # ~t_2
   1000011     4.99998854E+03   # ~e_L
   2000011     4.99998865E+03   # ~e_R
   1000012     5.00002281E+03   # ~nu_eL
   1000013     4.99998854E+03   # ~mu_L
   2000013     4.99998865E+03   # ~mu_R
   1000014     5.00002281E+03   # ~nu_muL
   1000015     4.99982377E+03   # ~tau_1
   2000015     5.00015402E+03   # ~tau_2
   1000016     5.00002281E+03   # ~nu_tauL
   1000021     3.95268333E+02   # ~g
   1000022     2.70040671E+02   # ~chi_10
   1000023    -1.01803800E+03   # ~chi_20
   1000025     1.01919279E+03   # ~chi_30
   1000035     4.96110075E+03   # ~chi_40
   1000024     1.01649351E+03   # ~chi_1+
   1000037     4.96110058E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98195782E-01   # N_11
  1  2    -9.98049269E-04   # N_12
  1  3     4.17514224E-02   # N_13
  1  4    -4.31393568E-02   # N_14
  2  1     9.80675293E-04   # N_21
  2  2    -3.67882999E-04   # N_22
  2  3     7.07131652E-01   # N_23
  2  4     7.07081134E-01   # N_24
  3  1     6.00348549E-02   # N_31
  3  2     1.97573717E-02   # N_32
  3  3    -7.05704368E-01   # N_33
  3  4     7.05681803E-01   # N_34
  4  1     1.89558708E-04   # N_41
  4  2    -9.99804238E-01   # N_42
  4  3    -1.42474643E-02   # N_43
  4  4     1.37280372E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01468594E-02   # U_11
  1  2     9.99797031E-01   # U_12
  2  1     9.99797031E-01   # U_21
  2  2     2.01468594E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94121690E-02   # V_11
  1  2     9.99811566E-01   # V_12
  2  1     9.99811566E-01   # V_21
  2  2     1.94121690E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00316610E-01   # cos(theta_t)
  1  2     9.94955566E-01   # sin(theta_t)
  2  1    -9.94955566E-01   # -sin(theta_t)
  2  2     1.00316610E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19232259E-01   # cos(theta_b)
  1  2     6.94769716E-01   # sin(theta_b)
  2  1    -6.94769716E-01   # -sin(theta_b)
  2  2     7.19232259E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07216161E-01   # cos(theta_tau)
  1  2     7.06997384E-01   # sin(theta_tau)
  2  1    -7.06997384E-01   # -sin(theta_tau)
  2  2     7.07216161E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20778000E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00184942E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45232511E-01   # tanbeta(Q)          
         3     2.44826255E+02   # vev(Q)              
         4     1.10434270E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00184942E+03  # The gauge couplings
     1     3.66960235E-01   # gprime(Q) DRbar
     2     6.37497514E-01   # g(Q) DRbar
     3     1.04302310E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00184942E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00184942E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00184942E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00184942E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15307978E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00184942E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85144619E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00184942E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38849716E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00184942E+03  # The soft SUSY breaking masses at the scale Q
         1     2.82900000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04924336E+05   # M^2_Hd              
        22     6.40960350E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40245516E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.50473024E-04   # gluino decays
#          BR         NDA      ID1       ID2
     5.00054522E-07    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.14636587E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.85362990E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.14636587E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.85362990E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.45269958E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.61754980E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.06600615E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07467157E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.06400741E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.12906490E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.52565551E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.43931243E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.84227590E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.39722853E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.40068204E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.51693593E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.13407538E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.15526889E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.50418306E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.90172929E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.84302929E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.66923175E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.30696525E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.46212258E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.26137661E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.67002701E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.28132465E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.52976310E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.80309108E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.79832613E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.08539533E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.76891267E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.12667427E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.58190838E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.36779325E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.17014048E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.16201094E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.66737327E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.33262673E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.08014156E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     7.67077119E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.32922881E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.08654440E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.81350047E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.18649953E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.70556155E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.58156432E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.41843568E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.16201094E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.66737327E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.33262673E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.08014156E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     7.67077119E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.32922881E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.08654440E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.81350047E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.18649953E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.70556155E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.58156432E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.41843568E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.69577794E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87393509E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.07226331E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.18094252E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.24309881E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.25220329E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.44892774E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66263034E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96667902E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.89059132E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.33120864E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.67981278E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.69577794E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87393509E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.07226331E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.18094252E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.24309881E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.25220329E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.44892774E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66263034E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96667902E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.89059132E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.33120864E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.67981278E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.67098482E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92603409E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.50642098E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.46081930E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.43669283E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95376104E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.87699383E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66650741E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93860283E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.06739689E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.18960725E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46678737E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.98227569E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.93708804E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.69017258E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95117705E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.41327644E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07952479E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.26502124E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.09270598E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.45272108E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.69017258E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95117705E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.41327644E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07952479E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.26502124E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.09270598E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.45272108E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.70777591E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92506201E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.40694323E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.06357019E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.24595552E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.71299565E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.44744393E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.49379451E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.45716661E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.72651970E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.45716661E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.72651970E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.85632627E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06337989E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88544130E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88543127E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88544130E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88543127E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.91589924E-07    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.43528819E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.04199604E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.26572536E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.25906281E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.54085529E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76851094E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76844345E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.34956207E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76739566E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76282616E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.74230034E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99544643E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.36533847E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.44541145E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.44541145E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.53567950E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.53567950E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.38464961E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.38464961E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     6.34060807E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     6.34060807E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.44541145E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.44541145E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.53567950E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.53567950E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.38464961E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.38464961E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     6.34060807E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     6.34060807E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.06353016E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.51496092E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.38861964E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.01777369E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.01777369E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     8.72122262E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     8.72122262E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.78931776E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.78931776E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.18078905E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.18078905E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.01777369E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.01777369E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     8.72122262E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     8.72122262E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.78931776E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.78931776E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.18078905E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.18078905E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06338389E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.86617479E-10    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.43467997E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52626100E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.26482723E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.26482723E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.55469342E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.83952869E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.32243771E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.47904441E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76118730E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.33943364E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53223718E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.25138233E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75848269E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76945453E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76945453E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.16749358E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.16749358E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.94133339E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.94133339E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.16900553E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.16900553E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.85336468E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.85336468E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.16749358E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.16749358E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.94133339E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.94133339E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.16900553E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.16900553E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.85336468E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.85336468E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.81701000E-03   # h decays
#          BR         NDA      ID1       ID2
     8.63170964E-01    2           5        -5   # BR(h -> b       bb     )
     7.89024412E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80042554E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.71769520E-04    2           3        -3   # BR(h -> s       sb     )
     2.84572884E-02    2           4        -4   # BR(h -> c       cb     )
     2.76580100E-02    2          21        21   # BR(h -> g       g      )
     6.37388120E-04    2          22        22   # BR(h -> gam     gam    )
     1.70872254E-04    2          24       -24   # BR(h -> W+      W-     )
     5.12237791E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84283917E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49535352E-04    2           5        -5   # BR(H -> b       bb     )
     3.79985628E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34325807E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86568947E-07    2           3        -3   # BR(H -> s       sb     )
     1.10835363E-05    2           4        -4   # BR(H -> c       cb     )
     9.94128365E-01    2           6        -6   # BR(H -> t       tb     )
     1.42211664E-03    2          21        21   # BR(H -> g       g      )
     4.40153580E-06    2          22        22   # BR(H -> gam     gam    )
     1.68487494E-06    2          23        22   # BR(H -> Z       gam    )
     3.50587953E-04    2          24       -24   # BR(H -> W+      W-     )
     1.73424506E-04    2          23        23   # BR(H -> Z       Z      )
     1.40665783E-03    2          25        25   # BR(H -> h       h      )
     8.79034120E-19    2          36        36   # BR(H -> A       A      )
     1.41132118E-09    2          23        36   # BR(H -> Z       A      )
     3.73735675E-10    2          24       -37   # BR(H -> W+      H-     )
     3.73735675E-10    2         -24        37   # BR(H -> W-      H+     )
     2.89273540E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.14497088E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.04906108E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.14497088E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.04906108E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.04144788E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.76336443E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.04144788E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.76336443E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.18223316E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41836682E-04    2           5        -5   # BR(A -> b       bb     )
     3.57355334E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26324398E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89050287E-07    2           3        -3   # BR(A -> s       sb     )
     1.02311297E-05    2           4        -4   # BR(A -> c       cb     )
     9.97538718E-01    2           6        -6   # BR(A -> t       tb     )
     1.70725698E-03    2          21        21   # BR(A -> g       g      )
     5.53914308E-06    2          22        22   # BR(A -> gam     gam    )
     2.07406027E-06    2          23        22   # BR(A -> Z       gam    )
     3.18406610E-04    2          23        25   # BR(A -> Z       h      )
     1.39886125E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04114933E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00395785E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68559740E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30285128E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45925924E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89824981E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02657115E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97687799E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32469281E-04    2          24        25   # BR(H+ -> W+      h      )
     9.80660573E-12    2          24        36   # BR(H+ -> W+      A      )
     9.66034097E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     9.66034097E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
