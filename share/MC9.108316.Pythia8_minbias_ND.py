###############################################################
#
# Job options file
# Pythia8 minimum bias (ND) samples
# contact: James Monk
#==============================================================

# ... Main generator : Pythia8

MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 4
	
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
  
import AthenaServices
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ("MC09JobOptions/MC9_Pythia8_Common.py")

Pythia8.Commands += ["SoftQCD:minBias = on"]

#
from MC09JobOptions.Pythia8EvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
