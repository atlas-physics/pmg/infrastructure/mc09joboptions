###############################################################
#
# Job options file
#
#==============================================================
##
## Job config for HERWIG+JIMMY in "min bias" mode. The JIMMY multiple scattering
## model does not extend down to arbitrary scales, so this JO fragment should be
## used with care, ensuring in analysis (via cuts or otherwise) that effects
## from below the MPI cutoff scale do not strongly influence physics studies.
##
## This process has been specifically requested for MPI model comparison studies
## in underlying event analyses at sqrt(s) > 900 GeV.
##
## author: Andy Buckley, April '10

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg")  

try:
  if runArgs.ecmEnergy == 900.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_900GeV.py" ) 
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
  if runArgs.ecmEnergy == 14000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

#
Herwig.HerwigCommand += [ "iproc 11500"]

# set JMUEO=0 and PTMIN=PTJIM for lowest pT Jimmy can do
# (PTJIM values are for MC09 tune)
Herwig.HerwigCommand += [ "jmueo 0"]

try:
  if runArgs.ecmEnergy == 900.0:
    Herwig.HerwigCommand += [ "ptmin 3.0"]
  if runArgs.ecmEnergy == 7000.0:
    Herwig.HerwigCommand += [ "ptmin 5.2"]
  if runArgs.ecmEnergy == 10000.0:
    Herwig.HerwigCommand += [ "ptmin 5.8"]
  if runArgs.ecmEnergy == 14000.0:
    Herwig.HerwigCommand += [ "ptmin 6.3"]
except NameError:
  pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

#==============================================================
#
# End of job options file
#
###############################################################
