#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy gg->bt_H+/ (mH+=400GeV,tan(beta)=7) 
# 3 photon EF
# SUSY point close to kinematic limit: Mu = 200 GeV, M2 = 310GeV
#
# Responsible person(s)
#   2 Mar, 2009 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence()

include("MC09JobOptions/MC9_Herwig_Common_14TeV.py")
topAlg.Herwig.HerwigCommand += [ "iproc 3839",              #iproc gg->bt_H+ + ch. conjg
                                  "susyfile susy_mHp400tanb15_2.txt",#isawig input file
                                  "taudec TAUOLA",           #taudec tau dcay package
                                  "effmin 0.0000000001"]     #To fix early termination
#Setup tauola and photos. 
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )
                                 
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg +=MultiLeptonFilter()
topAlg.MultiLeptonFilter.Ptcut = 7000.
topAlg.MultiLeptonFilter.NLeptons = 3
#Also, Eta is cut at 10.0 by default

try:
  StreamEVGEN.RequireAlgs = ["MultiLeptonFilter"]
except Exception, e:
  pass

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency =  0.1355
from MC09JobOptions.SUSYEvgenConfig import evgenConfig
# 5000/33210=0.1506 (x0.9=0.1355)
#==============================================================
#
# End of job options file
#
#####################################



