#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019049E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190487E+03   # EWSB                
         1     3.16800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05864918E+01   # W+
        25     7.13659020E+01   # h
        35     1.00939138E+03   # H
        36     1.00000000E+03   # A
        37     1.00330184E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.49748413E+02   # ~d_L
   2000001     4.49937010E+02   # ~d_R
   1000002     4.50172268E+02   # ~u_L
   2000002     4.50077313E+02   # ~u_R
   1000003     4.49748413E+02   # ~s_L
   2000003     4.49937010E+02   # ~s_R
   1000004     4.50172268E+02   # ~c_L
   2000004     4.50077313E+02   # ~c_R
   1000005     5.04736251E+03   # ~b_1
   2000005     5.04781043E+03   # ~b_2
   1000006     4.90985799E+03   # ~t_1
   2000006     5.04721954E+03   # ~t_2
   1000011     4.99998817E+03   # ~e_L
   2000011     4.99998829E+03   # ~e_R
   1000012     5.00002353E+03   # ~nu_eL
   1000013     4.99998817E+03   # ~mu_L
   2000013     4.99998829E+03   # ~mu_R
   1000014     5.00002353E+03   # ~nu_muL
   1000015     4.99982348E+03   # ~tau_1
   2000015     5.00015359E+03   # ~tau_2
   1000016     5.00002353E+03   # ~nu_tauL
   1000021     5.16013219E+03   # ~g
   1000022     2.99998214E+02   # ~chi_10
   1000023    -1.01781857E+03   # ~chi_20
   1000025     1.01908584E+03   # ~chi_30
   1000035     4.96310079E+03   # ~chi_40
   1000024     1.01627439E+03   # ~chi_1+
   1000037     4.96310062E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98042107E-01   # N_11
  1  2    -1.04606219E-03   # N_12
  1  3     4.35152602E-02   # N_13
  1  4    -4.49141556E-02   # N_14
  2  1     9.88434016E-04   # N_21
  2  2    -3.79421435E-04   # N_22
  2  3     7.07132080E-01   # N_23
  2  4     7.07080688E-01   # N_24
  3  1     6.25375099E-02   # N_31
  3  2     1.97470511E-02   # N_32
  3  3    -7.05597320E-01   # N_33
  3  4     7.05571779E-01   # N_34
  4  1     1.90579543E-04   # N_41
  4  2    -9.99804389E-01   # N_42
  4  3    -1.42500746E-02   # N_43
  4  4     1.37143461E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01505428E-02   # U_11
  1  2     9.99796957E-01   # U_12
  2  1     9.99796957E-01   # U_21
  2  2     2.01505428E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93927959E-02   # V_11
  1  2     9.99811942E-01   # V_12
  2  1     9.99811942E-01   # V_21
  2  2     1.93927959E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07039286E-01   # cos(theta_t)
  1  2     9.94254792E-01   # sin(theta_t)
  2  1    -9.94254792E-01   # -sin(theta_t)
  2  2     1.07039286E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19493817E-01   # cos(theta_b)
  1  2     6.94498846E-01   # sin(theta_b)
  2  1    -6.94498846E-01   # -sin(theta_b)
  2  2     7.19493817E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07236157E-01   # cos(theta_tau)
  1  2     7.06977382E-01   # sin(theta_tau)
  2  1    -7.06977382E-01   # -sin(theta_tau)
  2  2     7.07236157E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21943976E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190487E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43555034E-01   # tanbeta(Q)          
         3     2.44969030E+02   # vev(Q)              
         4     1.10431483E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190487E+03  # The gauge couplings
     1     3.66656046E-01   # gprime(Q) DRbar
     2     6.37247823E-01   # g(Q) DRbar
     3     1.02058069E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190487E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190487E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190487E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190487E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17069996E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190487E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87057454E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190487E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38837750E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190487E+03  # The soft SUSY breaking masses at the scale Q
         1     3.16800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04751849E+05   # M^2_Hd              
        22     6.78148516E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.22000000E+02   # M_q1L               
        42     2.22000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.22000000E+02   # M_uR                
        45     2.22000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.22000000E+02   # M_dR                
        48     2.22000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40143547E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.67076279E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24240258E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24240258E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24232241E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24232241E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24222236E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24222236E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24226275E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24226275E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24240258E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24240258E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24232241E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24232241E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24222236E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24222236E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24226275E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24226275E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.24626710E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.24626710E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.33203973E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.33203973E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.57967389E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.57967389E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48779614E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.65799794E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41410095E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38170949E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73838977E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26297545E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.39188463E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86536191E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89139835E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67116021E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.82763398E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.79540317E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.81840192E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40784668E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.81562955E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53828065E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.98787915E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.93113484E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71398938E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.00962945E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28710075E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.83858154E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42539990E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.79875430E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.94315449E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70893444E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.95990644E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     2.41859140E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     3.90913566E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     2.46286329E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     9.76081589E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     2.41859140E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     3.90913566E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     2.46286329E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     9.76081589E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.67401072E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87146097E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.96395343E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.63408274E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.53584109E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.25868856E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.30744756E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65452814E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96379274E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.04471599E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.61982164E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.90828990E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.67401072E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87146097E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.96395343E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.63408274E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.53584109E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.25868856E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.30744756E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65452814E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96379274E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.04471599E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.61982164E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.90828990E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.66579753E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92351815E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.52666927E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.74715424E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29554327E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95934427E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59464805E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66141507E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93543608E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.07857043E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.54807841E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.32368393E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.00440028E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.65083858E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.66843618E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95203590E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.50179304E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.33397473E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.55663497E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.09377596E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.31107124E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.66843618E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95203590E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.50179304E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.33397473E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.55663497E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.09377596E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.31107124E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.68603712E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92583724E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.49520709E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.31466810E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.53937468E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.72222811E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.30614844E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.72654672E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.37417590E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.64615740E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.37417590E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.64615740E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.85959333E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.05093398E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88304358E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88303468E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88304358E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88303468E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.15598651E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45160214E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.02649106E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28254044E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27489341E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.55659135E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77603207E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77599007E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     6.94991751E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77492785E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76976554E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.97851191E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99571277E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.09796398E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.71974632E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.71974632E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.74412758E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.74412758E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.54604155E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.54604155E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     6.86216853E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     6.86216853E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.71974632E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.71974632E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.74412758E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.74412758E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.54604155E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.54604155E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     6.86216853E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     6.86216853E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.09273806E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.35728824E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.36418970E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.39958275E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.39958275E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.00531730E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.00531730E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.62690550E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.62690550E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.51396872E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.51396872E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.39958275E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.39958275E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.00531730E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.00531730E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.62690550E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.62690550E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.51396872E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.51396872E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.05093592E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.18316759E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45090140E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63051500E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28163602E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28163602E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.59730145E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.21262473E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     6.91977021E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.65568621E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76833248E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.56595291E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.54676581E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.62114411E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.76520858E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77699265E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77699265E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13815882E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13815882E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.95430902E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.95430902E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13966806E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13966806E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.88580313E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.88580313E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13815882E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13815882E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.95430902E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.95430902E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13966806E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13966806E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.88580313E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.88580313E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.88508153E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60864712E-01    2           5        -5   # BR(h -> b       bb     )
     7.92858754E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81314501E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68819180E-04    2           3        -3   # BR(h -> s       sb     )
     2.82973786E-02    2           4        -4   # BR(h -> c       cb     )
     2.96029850E-02    2          21        21   # BR(h -> g       g      )
     6.99205834E-04    2          22        22   # BR(h -> gam     gam    )
     2.31462351E-04    2          24       -24   # BR(h -> W+      W-     )
     6.82466150E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86136893E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46374519E-04    2           5        -5   # BR(H -> b       bb     )
     3.77053282E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33289215E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85099038E-07    2           3        -3   # BR(H -> s       sb     )
     1.10884511E-05    2           4        -4   # BR(H -> c       cb     )
     9.94638789E-01    2           6        -6   # BR(H -> t       tb     )
     1.42273180E-03    2          21        21   # BR(H -> g       g      )
     4.41791255E-06    2          22        22   # BR(H -> gam     gam    )
     1.68200686E-06    2          23        22   # BR(H -> Z       gam    )
     3.77040201E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86510267E-04    2          23        23   # BR(H -> Z       Z      )
     1.39961357E-03    2          25        25   # BR(H -> h       h      )
     1.01564737E-18    2          36        36   # BR(H -> A       A      )
     1.61332488E-09    2          23        36   # BR(H -> Z       A      )
     4.50828123E-10    2          24       -37   # BR(H -> W+      H-     )
     4.50828123E-10    2         -24        37   # BR(H -> W-      H+     )
     2.76510875E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.13023633E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     5.32789794E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     3.13023633E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     5.32789794E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     4.57086476E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.33358617E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     4.57086476E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.33358617E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20080153E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38988329E-04    2           5        -5   # BR(A -> b       bb     )
     3.54816745E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25427012E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87735725E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308823E-05    2           4        -4   # BR(A -> c       cb     )
     9.97514594E-01    2           6        -6   # BR(A -> t       tb     )
     1.70718648E-03    2          21        21   # BR(A -> g       g      )
     5.53896414E-06    2          22        22   # BR(A -> gam     gam    )
     2.07306631E-06    2          23        22   # BR(A -> Z       gam    )
     3.41824700E-04    2          23        25   # BR(A -> Z       h      )
     1.43768764E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05684028E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95555959E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66122803E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29423676E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42823640E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83939753E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02692118E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.98153088E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.57123500E-04    2          24        25   # BR(H+ -> W+      h      )
     1.01749160E-11    2          24        36   # BR(H+ -> W+      A      )
     7.21185448E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     7.21185448E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
