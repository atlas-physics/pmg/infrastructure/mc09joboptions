###############################################################
#
# Job options file
# Bernardo Resende
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc35.105502.tchan'
evgenConfig.efficiency = 0.95

#==============================================================
#
# End of job options file
#
###############################################################

