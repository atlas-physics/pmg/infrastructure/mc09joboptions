###############################################################
#
# Job options file
#
# W->tau.nu + (0-3) Jets (QCD Diagrams only) by Sherpa
#
# Responsible person(s):
#
#   18/Mar/2009 - Wolfgang Mader (Wolfgang.Mader@cern.ch)
#
#==============================================================
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Sherpa
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
topAlg += sherpa

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 1
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.Ptcut = 15000.0

try:
    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.SherpaEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'Sherpa010103.209532.W3jetstotaunu_1lepPt15'
evgenConfig.minevents = 1000
evgenConfig.efficiency = 0.05
#==============================================================
#
# End of job options file
#
###############################################################
