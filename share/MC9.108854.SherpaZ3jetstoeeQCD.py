#
# Job options file
# Susanne Kuehn, December 16 2008
# (Susanne.Kuehn@cern.ch)
#
#==============================================================

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]

topAlg += sherpa

#--------------------------------------------------------------
# Truth Jets
#--------------------------------------------------------------

#from RecExConfig.RecFlags  import rec
#rec.doTruth = True

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC09JobOptions.SherpaEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'Sherpa10103.108854.SherpaZ3jetstoeeQCD'
evgenConfig.efficiency = 0.90
