###############################################################
#
# Job options file
#
# bbA, A->tautau->lh with MA=300GeV, tanBeta=20 by Sherpa
#
# Responsible person(s):
#
#   18/Mar/2009 - Wolfgang Mader (Wolfgang.Mader@cern.ch)
#
#==============================================================
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Sherpa
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
topAlg += sherpa

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.SherpaEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'Sherpa010103.209123.SherpabbAtautaulhMA300TB20'
evgenConfig.efficiency = 0.95
#==============================================================
#
# End of job options file
#
###############################################################
