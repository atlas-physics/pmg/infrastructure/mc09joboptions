###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# File prepared by Maarten Boonekamp Nov 2005
#--------------------------------------------------------------

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "EvgenJobOptions/MC9_Herwig_Common.py" )
Herwig.HerwigCommand += [ "iproc 11451",
                          "taudec TAUOLA"]

# ... Tauola
include ( "EvgenJobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "EvgenJobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7


#--------------------------------------------------------------
# Root output
#--------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from EvgenJobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.51

#==============================================================
#
# End of job options file
#
###############################################################
