###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    print runArgs 
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC09JobOptions.AlpgenEvgenConfig import evgenConfig
 
# inputfilebase
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107662.ZmumuNp2_pt20_7tev.TXT.v3'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107662.ZmumuNp2.pt20.v2'    
except NameError:
  pass

#
# 7 TeV - Information on sample 107662
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.28
# 7 TeV - Number of Matrix Elements in input file  = 23500
# 7 TeV - Alpgen cross section = 143.5 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 39.6 pb
# 7 TeV - Cross section after filtering = 39.6 pb
# 7 TeV - Lumi/5000 events post-filter = 5000/eff(Filter)/XS(jimmy) = 126.1 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 5556 events on average,
# 7 TeV - of which only 5000 will be used in further processing
#
evgenConfig.efficiency = 0.90000
evgenConfig.minevents = 5000
# 10 TeV - Information on sample 107662
# 10 TeV - Filter efficiency  = 1.000
# 10 TeV - MLM matching efficiency = 0.27
# 10 TeV - Actual number of Matrix Elements in input file = 2500
# 10 TeV - Alpgen cross section = 258.6 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 69.4 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 7.209 pb-1
#==============================================================
#
# End of job options file
#
###############################################################
