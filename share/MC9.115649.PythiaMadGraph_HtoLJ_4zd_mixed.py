from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC09JobOptions/MC9_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += [ "pyinit user madgraph",
                          "pydat1 parj 90 20000.",
                          "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.MadGraphEvgenConfig import evgenConfig
#from MC09JobOptions.PythiaEvgenConfig import evgenConfig
#evgenConfig.generators = ["Pythia","Lhef"]
evgenConfig.efficiency = 0.90
evgenConfig.inputfilebase = "group09.phys-gener.MadGraph.115649.HtoLJ_4zd_mixed.TXT.v2"
