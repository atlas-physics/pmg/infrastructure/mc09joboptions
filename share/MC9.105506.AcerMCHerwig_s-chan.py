###############################################################
#
# Job options file
# Borut Paul Kersevan 
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig = topAlg.Herwig
Herwig.HerwigCommand += [ "iproc acermc" ]
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.AcerMCHWEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc35.105501.schan'
evgenConfig.efficiency = 0.95


#==============================================================
#
# End of job options file
#
###############################################################

