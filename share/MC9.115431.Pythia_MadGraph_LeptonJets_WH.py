###############################################################
#
# Job options file
# prepared by Andrii Tykhonov 
# 
# Higgs decay to hidden sector which results in "electron jets"
#
#==============================================================
# ... Main generator : Pythia

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC09JobOptions/MC9_PythiaMC09p_Common.py" ) 
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
Pythia.PythiaCommand = ["pyinit user lhef"]
#--------------------------------------------------------------
Pythia.PythiaCommand +=  [
            "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
            "pydat1 parj 90 20000", # Turn off FSR.
            "pydat3 mdcy 15 1 0"    # Turn off tau decays.
            ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )
# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["Lhef", "Pythia"]
evgenConfig.inputfilebase = 'group10.phys-gener.Pythia_MadGraph.115431.WH_unweight_7TeV.TXT.v1'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################

