###############################################################
#
# Job options file for Wt at high pT + lepton filter
# G. Piacquadio
#
#==============================================================
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.PythiaCommand += [ "pyinit user acermc",
			 "pydat1 parj 90 20000.",
			 "pydat3 mdcy 15 1 0" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
ParticleFilterW= ParticleFilter(name = "ParticleFilterW",
                                Energycut=1000000000000,
                                StatusReq=-1,
                                PDG=24,
                                Ptcut = 150.*GeV,
                                Etacut = 2.8)


ParticleFilterTop= ParticleFilter(name = "ParticleFilterTop",
                                  Energycut=1000000000000,
                                  StatusReq=-1,
                                  PDG=6,
                                  Ptcut = 100.*GeV,
                                  Etacut = 2.8)

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
LeptonFilter= LeptonFilter(name = "LeptonFilter")
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 2.7

topAlg += LeptonFilter
topAlg += ParticleFilterTop
topAlg += ParticleFilterW

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterTop" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterW" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.AcerMCEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'acermc35.209717.Wt'
evgenConfig.efficiency = 0.092
#0.0976 +/- 0.0009

#==============================================================
#
# End of job options file
#
###############################################################
