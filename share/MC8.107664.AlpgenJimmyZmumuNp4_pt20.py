###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "EvgenJobOptions/MC9_Herwig_Common.py" )

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "EvgenJobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "EvgenJobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from EvgenJobOptions.AlpgenEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'alpgen.107664.ZmumuNp4.pt20.v3'
# Information on sample 107664
# Filter efficiency  = 1.0000
# MLM matching efficiency = 0.13
# Number of Matrix Elements in input file  = 5000
# Alpgen cross section = 45.83 pb
# Herwig cross section = Alpgen cross section * eff(MLM) = 6.08 pb
# Cross section after filtering = 6.08 pb
# Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 82.20 pb-1
#
# Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# of which only 500 will be used in further processing
evgenConfig.efficiency = 0.90000
#==============================================================
#
# End of job options file
#
###############################################################
