###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# File prepared by Maarten Boonekamp Nov 2005
#--------------------------------------------------------------

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

Herwig.HerwigCommand += [ "iproc 11451",
                          "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 10000.
LeptonFilter.Etacut = 2.7


#--------------------------------------------------------------
# Root output
#--------------------------------------------------------------

try:
     StreamEVGEN.RequireAlgs +=  [ "LeptonFilter" ]
except Exception, e:
     pass


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.51

#==============================================================
#
# End of job options file
#
###############################################################
