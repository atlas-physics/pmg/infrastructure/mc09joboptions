###############################################################
#
# Job options file
#
# W + (0-3) Jets (QCD Diagrams only) by Sherpa
#
# Responsible person(s):
#
#   18/Mar/2009 - Wolfgang Mader (Wolfgang.Mader@cern.ch)
#
#==============================================================
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Sherpa
from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]
topAlg += sherpa

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 1
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.Ptcut = 15000.0

try:
    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.SherpaEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'Sherpa010103.209521.Z3jetstomumu_1lepPt15_v2'
evgenConfig.efficiency = 0.75
#==============================================================
#
# End of job options file
#
###############################################################
