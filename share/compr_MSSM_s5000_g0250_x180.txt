#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018669E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00186685E+03   # EWSB                
         1     1.84600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.14400000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888974E+01   # W+
        25     8.51628083E+01   # h
        35     1.00935113E+03   # H
        36     1.00000000E+03   # A
        37     1.00336495E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04503408E+03   # ~d_L
   2000001     5.04504952E+03   # ~d_R
   1000002     5.04506880E+03   # ~u_L
   2000002     5.04506105E+03   # ~u_R
   1000003     5.04503408E+03   # ~s_L
   2000003     5.04504952E+03   # ~s_R
   1000004     5.04506880E+03   # ~c_L
   2000004     5.04506105E+03   # ~c_R
   1000005     5.04481956E+03   # ~b_1
   2000005     5.04526513E+03   # ~b_2
   1000006     4.91292373E+03   # ~t_1
   2000006     5.04521717E+03   # ~t_2
   1000011     4.99998851E+03   # ~e_L
   2000011     4.99998857E+03   # ~e_R
   1000012     5.00002292E+03   # ~nu_eL
   1000013     4.99998851E+03   # ~mu_L
   2000013     4.99998857E+03   # ~mu_R
   1000014     5.00002292E+03   # ~nu_muL
   1000015     4.99982413E+03   # ~tau_1
   2000015     5.00015355E+03   # ~tau_2
   1000016     5.00002292E+03   # ~nu_tauL
   1000021     2.49985857E+02   # ~g
   1000022     1.79991318E+02   # ~chi_10
   1000023    -1.01774547E+03   # ~chi_20
   1000025     1.01862821E+03   # ~chi_30
   1000035     5.02220118E+03   # ~chi_40
   1000024     1.01624386E+03   # ~chi_1+
   1000037     5.02220103E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98576352E-01   # N_11
  1  2    -8.53321796E-04   # N_12
  1  3     3.69501055E-02   # N_13
  1  4    -3.84607808E-02   # N_14
  2  1     1.06741717E-03   # N_21
  2  2    -3.68108815E-04   # N_22
  2  3     7.07132805E-01   # N_23
  2  4     7.07079855E-01   # N_24
  3  1     5.33300925E-02   # N_31
  3  2     1.93360323E-02   # N_32
  3  3    -7.05976923E-01   # N_33
  3  4     7.05959350E-01   # N_34
  4  1     1.78725996E-04   # N_41
  4  2    -9.99812610E-01   # N_42
  4  3    -1.39452379E-02   # N_43
  4  4     1.34255057E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97196102E-02   # U_11
  1  2     9.99805550E-01   # U_12
  2  1     9.99805550E-01   # U_21
  2  2     1.97196102E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89844908E-02   # V_11
  1  2     9.99819778E-01   # V_12
  2  1     9.99819778E-01   # V_21
  2  2     1.89844908E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.01901476E-01   # cos(theta_t)
  1  2     9.94794496E-01   # sin(theta_t)
  2  1    -9.94794496E-01   # -sin(theta_t)
  2  2     1.01901476E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19252925E-01   # cos(theta_b)
  1  2     6.94748321E-01   # sin(theta_b)
  2  1    -6.94748321E-01   # -sin(theta_b)
  2  2     7.19252925E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173131E-01   # cos(theta_tau)
  1  2     7.07040425E-01   # sin(theta_tau)
  2  1    -7.07040425E-01   # -sin(theta_tau)
  2  2     7.07173131E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21407592E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00186685E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44313669E-01   # tanbeta(Q)          
         3     2.44260863E+02   # vev(Q)              
         4     1.10258675E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00186685E+03  # The gauge couplings
     1     3.65901256E-01   # gprime(Q) DRbar
     2     6.34901190E-01   # g(Q) DRbar
     3     1.03482590E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00186685E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00186685E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00186685E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00186685E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16185221E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00186685E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86087889E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00186685E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38895197E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00186685E+03  # The soft SUSY breaking masses at the scale Q
         1     1.84600000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.14400000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03219807E+05   # M^2_Hd              
        22     6.59870854E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39102886E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.47643491E-10   # gluino decays
#          BR         NDA      ID1       ID2
     1.40783643E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     8.82260369E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.99274607E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.82260369E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.99274607E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.42150691E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.05099943E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.90484560E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01299107E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.00395807E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.00664912E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.78591718E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.86440292E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.63667438E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.29383611E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.29760874E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.36191003E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.69222803E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.36681147E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.19013386E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.90779406E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.26109994E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.19623105E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.92855363E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.38540311E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.19663749E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.53003044E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.42650691E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.46976459E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.21962365E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.32445296E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.71338004E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.38586852E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.16182828E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.44579072E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.50921800E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.15211101E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.59852591E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.98780116E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.34912214E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.43972659E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.18466531E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.65862001E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.37008660E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97805667E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.70640605E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.11556103E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.31453329E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.27256654E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.66354345E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.68761631E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.59849434E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.02349113E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.66704866E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.19339266E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.17820745E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.94744794E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.35578409E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97807484E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.61956114E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.97576510E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.48512662E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.11775741E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.96075321E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92003049E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.59852591E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.98780116E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.34912214E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.43972659E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.18466531E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.65862001E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.37008660E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97805667E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.70640605E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.11556103E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.31453329E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.27256654E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.66354345E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.68761631E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.59849434E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.02349113E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.66704866E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.19339266E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.17820745E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.94744794E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.35578409E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97807484E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.61956114E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.97576510E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.48512662E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.11775741E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.96075321E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92003049E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.66355522E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.90919369E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.68738424E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.92986872E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.15059311E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.65603085E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97378663E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.04987520E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.62028677E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.66355522E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.90919369E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.68738424E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.92986872E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.15059311E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.65603085E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97378663E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.04987520E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.62028677E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66611813E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93806326E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.53911187E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.72669257E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.91307005E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66155655E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95198624E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.07613700E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.28860338E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.15845531E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.65806466E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97642981E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.67504910E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.59444185E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99489940E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.65806466E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97642981E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.67504910E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.59444185E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99489940E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.67569053E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95008898E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.66798616E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.58495142E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.62993866E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     8.32239761E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51943885E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.71241768E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07905242E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07905242E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07225349E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08543518E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08543518E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05745851E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02800804E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78669415E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.35872230E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71734395E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71551763E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85141088E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31373005E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31373639E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.04333385E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30939397E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29599496E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.67726723E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99342760E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.57239682E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.92817370E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.96168196E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99603832E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51942875E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.25732790E-10    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78641136E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.43307521E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71700056E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71700056E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.54071242E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.03654218E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.03354949E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.05816964E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28331803E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.41170956E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84876215E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.22437945E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27810175E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31756254E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31756254E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04253290E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04253290E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.42680769E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.42680769E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04253290E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04253290E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.42680769E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.42680769E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26954304E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26954304E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12195768E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12195768E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03974090E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03974090E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03974090E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03974090E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03974090E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03974090E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.20207969E-03   # h decays
#          BR         NDA      ID1       ID2
     8.47036589E-01    2           5        -5   # BR(h -> b       bb     )
     8.10643577E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87306733E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.57756137E-04    2           3        -3   # BR(h -> s       sb     )
     2.78326314E-02    2           4        -4   # BR(h -> c       cb     )
     4.08151311E-02    2          21        21   # BR(h -> g       g      )
     1.06137653E-03    2          22        22   # BR(h -> gam     gam    )
     9.91695607E-04    2          24       -24   # BR(h -> W+      W-     )
     2.53155715E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84396205E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49041518E-04    2           5        -5   # BR(H -> b       bb     )
     3.79116921E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34018716E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86123828E-07    2           3        -3   # BR(H -> s       sb     )
     1.11073796E-05    2           4        -4   # BR(H -> c       cb     )
     9.96325690E-01    2           6        -6   # BR(H -> t       tb     )
     1.42501929E-03    2          21        21   # BR(H -> g       g      )
     4.73584046E-06    2          22        22   # BR(H -> gam     gam    )
     1.68725344E-06    2          23        22   # BR(H -> Z       gam    )
     3.64850987E-04    2          24       -24   # BR(H -> W+      W-     )
     1.80480893E-04    2          23        23   # BR(H -> Z       Z      )
     1.39878991E-03    2          25        25   # BR(H -> h       h      )
     9.92649429E-19    2          36        36   # BR(H -> A       A      )
     1.58481995E-09    2          23        36   # BR(H -> Z       A      )
     4.15350722E-10    2          24       -37   # BR(H -> W+      H-     )
     4.15350722E-10    2         -24        37   # BR(H -> W-      H+     )
     3.62461443E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.19227545E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40993787E-04    2           5        -5   # BR(A -> b       bb     )
     3.55971103E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25835075E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88333591E-07    2           3        -3   # BR(A -> s       sb     )
     1.02312233E-05    2           4        -4   # BR(A -> c       cb     )
     9.97547846E-01    2           6        -6   # BR(A -> t       tb     )
     1.70725659E-03    2          21        21   # BR(A -> g       g      )
     5.53899023E-06    2          22        22   # BR(A -> gam     gam    )
     2.07438077E-06    2          23        22   # BR(A -> Z       gam    )
     3.27971726E-04    2          23        25   # BR(A -> Z       h      )
     1.22176494E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04167179E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.99838984E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.67838212E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30030069E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45549759E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.88072284E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02847023E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99609178E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.43212269E-04    2          24        25   # BR(H+ -> W+      h      )
     1.12182108E-11    2          24        36   # BR(H+ -> W+      A      )
