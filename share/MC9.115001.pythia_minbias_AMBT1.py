###############################################################
#
# Job options file for min bias (ND)
#
# author: C. Gwenlan (June'10)
# AMBT1 tune [ATLAS-CONF-2010-031]
# equivalent MC9 JO: MC9.105001.pythia_minbias.py
#===============================================================

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC09JobOptions/MC9_PythiaAMBT1_Common.py" )

Pythia.PythiaCommand += [  "pysubs msel 1" ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig

evgenConfig.efficiency = 0.9


#==============================================================
#
# End of job options file
#
###############################################################

