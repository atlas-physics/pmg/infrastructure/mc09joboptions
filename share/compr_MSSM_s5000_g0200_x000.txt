#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018540E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00185402E+03   # EWSB                
         1     1.80000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     8.24000000E+01   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888905E+01   # W+
        25     8.47349946E+01   # h
        35     1.00929251E+03   # H
        36     1.00000000E+03   # A
        37     1.00335815E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04544290E+03   # ~d_L
   2000001     5.04545821E+03   # ~d_R
   1000002     5.04547734E+03   # ~u_L
   2000002     5.04546965E+03   # ~u_R
   1000003     5.04544290E+03   # ~s_L
   2000003     5.04545821E+03   # ~s_R
   1000004     5.04547734E+03   # ~c_L
   2000004     5.04546965E+03   # ~c_R
   1000005     5.04522876E+03   # ~b_1
   2000005     5.04567344E+03   # ~b_2
   1000006     4.91437488E+03   # ~t_1
   2000006     5.04612594E+03   # ~t_2
   1000011     4.99998860E+03   # ~e_L
   2000011     4.99998866E+03   # ~e_R
   1000012     5.00002274E+03   # ~nu_eL
   1000013     4.99998860E+03   # ~mu_L
   2000013     4.99998866E+03   # ~mu_R
   1000014     5.00002274E+03   # ~nu_muL
   1000015     4.99982414E+03   # ~tau_1
   2000015     5.00015373E+03   # ~tau_2
   1000016     5.00002274E+03   # ~nu_tauL
   1000021     1.99993594E+02   # ~g
   1000022    -9.87258591E-01   # ~chi_10
   1000023    -1.01779803E+03   # ~chi_20
   1000025     1.01825626E+03   # ~chi_30
   1000035     5.02220232E+03   # ~chi_40
   1000024     1.01629550E+03   # ~chi_1+
   1000037     5.02220218E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99036476E-01   # N_11
  1  2    -6.76652885E-04   # N_12
  1  3     3.01344873E-02   # N_13
  1  4    -3.18994329E-02   # N_14
  2  1     1.24714825E-03   # N_21
  2  2    -3.65101532E-04   # N_22
  2  3     7.07135372E-01   # N_23
  2  4     7.07076995E-01   # N_24
  3  1     4.38695098E-02   # N_31
  3  2     1.93476156E-02   # N_32
  3  3    -7.06298080E-01   # N_33
  3  4     7.06289005E-01   # N_34
  4  1     1.72346480E-04   # N_41
  4  2    -9.99812522E-01   # N_42
  4  3    -1.39463652E-02   # N_43
  4  4     1.34309561E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97212759E-02   # U_11
  1  2     9.99805517E-01   # U_12
  2  1     9.99805517E-01   # U_21
  2  2     1.97212759E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89922686E-02   # V_11
  1  2     9.99819631E-01   # V_12
  2  1     9.99819631E-01   # V_21
  2  2     1.89922686E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.01437102E-01   # cos(theta_t)
  1  2     9.94841954E-01   # sin(theta_t)
  2  1    -9.94841954E-01   # -sin(theta_t)
  2  2     1.01437102E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19180660E-01   # cos(theta_b)
  1  2     6.94823127E-01   # sin(theta_b)
  2  1    -6.94823127E-01   # -sin(theta_b)
  2  2     7.19180660E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173043E-01   # cos(theta_tau)
  1  2     7.07040513E-01   # sin(theta_tau)
  2  1    -7.07040513E-01   # -sin(theta_tau)
  2  2     7.07173043E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21097397E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00185402E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.44775824E-01   # tanbeta(Q)          
         3     2.44310129E+02   # vev(Q)              
         4     1.10077847E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00185402E+03  # The gauge couplings
     1     3.65904684E-01   # gprime(Q) DRbar
     2     6.34915554E-01   # g(Q) DRbar
     3     1.03979253E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00185402E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00185402E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00185402E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00185402E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15731252E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00185402E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85615466E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00185402E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38905125E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00185402E+03  # The soft SUSY breaking masses at the scale Q
         1     1.80000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     8.24000000E+01   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03247737E+05   # M^2_Hd              
        22     6.50900640E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39109210E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.08479953E-09   # gluino decays
#          BR         NDA      ID1       ID2
     3.14661250E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.90431684E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.36128460E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.90431684E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.36128460E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.81906180E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.09546944E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.88185591E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98188427E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.90726107E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.97779336E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.84510651E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.91818354E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.57891787E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27019442E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.27431706E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.27447962E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.22508656E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.41524515E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.15684951E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.70289162E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.31904761E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.14432734E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.87018444E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.81437569E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.24817154E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.49816738E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.45889457E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.03837548E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.27790111E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.28590298E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.63235264E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.37142889E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.21018305E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.41522005E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.54032423E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.74214741E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.66138512E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.96252744E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.85736429E-10    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.84691062E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.23047037E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.59846047E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.46167814E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97836097E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.76944821E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.07164163E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.44854847E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.50398738E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.20733058E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.69228499E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.66135257E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.99040542E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.82341823E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.94656958E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.22409897E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.88001590E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.44757675E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97838113E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68244612E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.86051404E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.13840967E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.40850269E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.84339347E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92125390E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.66138512E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.96252744E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.85736429E-10    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.84691062E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.23047037E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.59846047E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.46167814E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97836097E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.76944821E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.07164163E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.44854847E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.50398738E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.20733058E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.69228499E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.66135257E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.99040542E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.82341823E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.94656958E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.22409897E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.88001590E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.44757675E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97838113E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68244612E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.86051404E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.13840967E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.40850269E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.84339347E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92125390E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.68152776E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.92363005E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.44809225E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.49140141E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14524894E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66315902E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98230086E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.42937360E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.76848483E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.68152776E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.92363005E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.44809225E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.49140141E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14524894E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66315902E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98230086E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.42937360E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.76848483E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.67053078E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94644807E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.55291300E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.89156448E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90833721E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66607002E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96304799E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.04219699E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.18583939E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.14179321E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67575395E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97908258E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.24171721E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.71833761E-05    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99131652E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.67575395E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97908258E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.24171721E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.71833761E-05    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99131652E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.69338218E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95280087E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.23317957E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.69274261E-05    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.61975252E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.24932434E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99257238E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.42762024E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000037     7.52154581E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.30009536E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07881175E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07881175E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07201439E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08514328E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08514328E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05731691E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02785688E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78710117E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.32325138E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71774627E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71695912E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85103132E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31165739E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31164135E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.33133472E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30730521E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.30105632E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.52589529E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.98363368E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.31689745E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.18794128E-04    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     9.40091385E-07    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000025     6.66110660E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.90567551E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.97860262E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.50853099E-07    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.34881932E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000035     7.52153752E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.61371771E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78681509E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.32742635E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71741554E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71741554E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42390003E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.48607523E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.32319455E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.03986784E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28156863E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.38721500E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84950618E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.18804956E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.28331360E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31553547E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31553547E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04239334E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04239334E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.11573371E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.11573371E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04239334E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04239334E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.11573371E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.11573371E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26886519E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26886519E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12124326E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12124326E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03961183E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03961183E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03961183E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03961183E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03961183E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03961183E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.58002801E-03   # h decays
#          BR         NDA      ID1       ID2
     7.19956561E-01    2           5        -5   # BR(h -> b       bb     )
     6.88318584E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43958985E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.59109152E-04    2           3        -3   # BR(h -> s       sb     )
     2.36648537E-02    2           4        -4   # BR(h -> c       cb     )
     3.43597490E-02    2          21        21   # BR(h -> g       g      )
     8.90868840E-04    2          22        22   # BR(h -> gam     gam    )
     8.02665643E-04    2          24       -24   # BR(h -> W+      W-     )
     2.06815123E-04    2          23        23   # BR(h -> Z       Z      )
     1.50483560E-01    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.83831485E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49630252E-04    2           5        -5   # BR(H -> b       bb     )
     3.79965719E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34318768E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86548455E-07    2           3        -3   # BR(H -> s       sb     )
     1.11076307E-05    2           4        -4   # BR(H -> c       cb     )
     9.96332199E-01    2           6        -6   # BR(H -> t       tb     )
     1.42508833E-03    2          21        21   # BR(H -> g       g      )
     4.73654575E-06    2          22        22   # BR(H -> gam     gam    )
     1.68795692E-06    2          23        22   # BR(H -> Z       gam    )
     3.58710791E-04    2          24       -24   # BR(H -> W+      W-     )
     1.77443299E-04    2          23        23   # BR(H -> Z       Z      )
     1.40068214E-03    2          25        25   # BR(H -> h       h      )
     9.60238026E-19    2          36        36   # BR(H -> A       A      )
     1.53757544E-09    2          23        36   # BR(H -> Z       A      )
     3.98155050E-10    2          24       -37   # BR(H -> W+      H-     )
     3.98155050E-10    2         -24        37   # BR(H -> W-      H+     )
     3.94651646E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.18699829E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41495365E-04    2           5        -5   # BR(A -> b       bb     )
     3.56682132E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26086423E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88701908E-07    2           3        -3   # BR(A -> s       sb     )
     1.02316151E-05    2           4        -4   # BR(A -> c       cb     )
     9.97586040E-01    2           6        -6   # BR(A -> t       tb     )
     1.70733001E-03    2          21        21   # BR(A -> g       g      )
     5.53921621E-06    2          22        22   # BR(A -> gam     gam    )
     2.07446082E-06    2          23        22   # BR(A -> Z       gam    )
     3.22534201E-04    2          23        25   # BR(A -> Z       h      )
     8.87719158E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.03668260E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00640351E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68560575E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30285423E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.46062570E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89816685E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02851190E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99614811E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.37505031E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11163861E-11    2          24        36   # BR(H+ -> W+      A      )
