## **********************************************************************
## $Id: MC9.105659.Pythia_MadGraph_BB500ttWW2lm.py,v 1.1 2008-08-22 07:18:33 osamu Exp $
##
## Authors:
##  David Berge and Michael Wilson, CERN
## **********************************************************************

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_PythiaMC09p_Common.py" )
Pythia.PythiaCommand += ["pyinit user madgraph"]

## ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

## ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.95
evgenConfig.generators = ["Pythia","MadGraph"]
evgenConfig.inputfilebase = "MadGraph.105659.Pythia_MadGraph_BB500ttWW2lm"
