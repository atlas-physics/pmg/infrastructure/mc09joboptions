#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018662E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00186624E+03   # EWSB                
         1     5.63000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05851090E+01   # W+
        25     6.97798780E+01   # h
        35     1.00921700E+03   # H
        36     1.00000000E+03   # A
        37     1.00333735E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.99621452E+02   # ~d_L
   2000001     1.99976937E+02   # ~d_R
   1000002     2.00416867E+02   # ~u_L
   2000002     2.00240522E+02   # ~u_R
   1000003     1.99621452E+02   # ~s_L
   2000003     1.99976937E+02   # ~s_R
   1000004     2.00416867E+02   # ~c_L
   2000004     2.00240522E+02   # ~c_R
   1000005     5.04673171E+03   # ~b_1
   2000005     5.04717832E+03   # ~b_2
   1000006     4.91361507E+03   # ~t_1
   2000006     5.04876527E+03   # ~t_2
   1000011     4.99998847E+03   # ~e_L
   2000011     4.99998853E+03   # ~e_R
   1000012     5.00002300E+03   # ~nu_eL
   1000013     4.99998847E+03   # ~mu_L
   2000013     4.99998853E+03   # ~mu_R
   1000014     5.00002300E+03   # ~nu_muL
   1000015     4.99982268E+03   # ~tau_1
   2000015     5.00015492E+03   # ~tau_2
   1000016     5.00002300E+03   # ~nu_tauL
   1000021     5.12980209E+03   # ~g
   1000022     1.19964563E+02   # ~chi_10
   1000023    -1.01805533E+03   # ~chi_20
   1000025     1.01860850E+03   # ~chi_30
   1000035     4.96446885E+03   # ~chi_40
   1000024     1.01649306E+03   # ~chi_1+
   1000037     4.96446869E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98906224E-01   # N_11
  1  2    -7.46089711E-04   # N_12
  1  3     3.22087361E-02   # N_13
  1  4    -3.38880095E-02   # N_14
  2  1     1.18655229E-03   # N_21
  2  2    -3.68378744E-04   # N_22
  2  3     7.07134915E-01   # N_23
  2  4     7.07077555E-01   # N_24
  3  1     4.67430746E-02   # N_31
  3  2     1.98719270E-02   # N_32
  3  3    -7.06199491E-01   # N_33
  3  4     7.06188693E-01   # N_34
  4  1     1.83200452E-04   # N_41
  4  2    -9.99802188E-01   # N_42
  4  3    -1.43209016E-02   # N_43
  4  4     1.38008712E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.02507767E-02   # U_11
  1  2     9.99794932E-01   # U_12
  2  1     9.99794932E-01   # U_21
  2  2     2.02507767E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.95152284E-02   # V_11
  1  2     9.99809560E-01   # V_12
  2  1     9.99809560E-01   # V_21
  2  2     1.95152284E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03006603E-01   # cos(theta_t)
  1  2     9.94680672E-01   # sin(theta_t)
  2  1    -9.94680672E-01   # -sin(theta_t)
  2  2     1.03006603E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19260572E-01   # cos(theta_b)
  1  2     6.94740404E-01   # sin(theta_b)
  2  1    -6.94740404E-01   # -sin(theta_b)
  2  2     7.19260572E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07170123E-01   # cos(theta_tau)
  1  2     7.07043434E-01   # sin(theta_tau)
  2  1    -7.07043434E-01   # -sin(theta_tau)
  2  2     7.07170123E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20719609E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00186624E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45463097E-01   # tanbeta(Q)          
         3     2.46293032E+02   # vev(Q)              
         4     1.10142506E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00186624E+03  # The gauge couplings
     1     3.67447494E-01   # gprime(Q) DRbar
     2     6.37537616E-01   # g(Q) DRbar
     3     1.04372192E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00186624E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15126472E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.84953502E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00186624E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38836661E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00186624E+03  # The soft SUSY breaking masses at the scale Q
         1     5.63000000E+01   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.06150343E+05   # M^2_Hd              
        22     6.36697286E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     2.00000000E+01   # M_q1L               
        42     2.00000000E+01   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     2.00000000E+01   # M_uR                
        45     2.00000000E+01   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     2.00000000E+01   # M_dR                
        48     2.00000000E+01   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40277078E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.26397394E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24558662E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24558662E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24551909E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24551909E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24543535E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24543535E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24546894E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24546894E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24558662E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24558662E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24551909E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24551909E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24543535E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24543535E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24546894E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24546894E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     6.80972331E-05    2     1000005        -5   # BR(~g -> ~b_1  bb)
     6.80972331E-05    2    -1000005         5   # BR(~g -> ~b_1* b )
     7.46284168E-05    2     2000005        -5   # BR(~g -> ~b_2  bb)
     7.46284168E-05    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.17074423E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.17074423E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.40950735E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.77219295E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.40867255E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.38341331E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73069484E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21934078E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.48620608E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86671005E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.89448116E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.61765035E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.99845788E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.87014499E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.13097448E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.18012501E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.93627428E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.57738596E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.52091182E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.91655665E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.70219071E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.83299393E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.08565336E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.96070223E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43477266E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.79112224E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.92806474E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.69852743E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.75161018E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.48273250E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     2.38449454E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.48769561E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     5.93720004E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.48273250E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     2.38449454E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.48769561E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     5.93720004E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.74357145E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.89903136E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.74157566E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.03526251E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.00475316E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.25965423E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.20119768E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.68252251E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97988720E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.29530703E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.00998433E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.73594913E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.74357145E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.89903136E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.74157566E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.03526251E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.00475316E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.25965423E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.20119768E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.68252251E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97988720E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.29530703E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.00998433E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.73594913E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.68341263E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.93981538E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.49724125E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.16271192E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.18927140E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94889813E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.38201021E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67875008E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95620271E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.00425167E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.51137253E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.21672289E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.57569790E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.43683231E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.73749736E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95951750E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.05245150E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.37733421E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.02431910E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10039399E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.20463794E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.73749736E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95951750E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.05245150E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.37733421E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.02431910E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10039399E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.20463794E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.75509725E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93356875E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.04449857E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.37374567E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.00862320E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.70180409E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.20003981E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.01950989E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.63329538E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     7.14689522E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     6.63329538E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     7.14689522E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.72439619E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.07706125E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88363410E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88361104E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88363410E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88361104E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.00467975E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47513930E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.91752293E-05    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30168193E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.29917973E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.52340939E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76308381E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76300934E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.57268781E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76194331E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76005959E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.12222459E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99035886E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.15451772E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.81113809E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.81113809E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     7.63784392E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     7.63784392E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     3.26814644E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     3.26814644E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     1.90979379E-06    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     1.90979379E-06    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.81113809E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.81113809E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     7.63784392E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     7.63784392E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     3.26814644E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     3.26814644E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     1.90979379E-06    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     1.90979379E-06    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     8.94991418E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.18355685E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.29262124E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.08818491E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     6.08818491E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     9.43787244E-03    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     9.43787244E-03    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     8.68070156E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     8.68070156E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.35987885E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.35987885E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     6.08818491E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     6.08818491E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     9.43787244E-03    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     9.43787244E-03    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     8.68070156E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     8.68070156E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.35987885E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.35987885E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.07706647E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.30874183E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47450379E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52856840E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30080557E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30080557E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.23434186E-05    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.20084762E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.55156276E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.46310656E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.75576374E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.29773320E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.51922777E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.22290352E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75573382E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76403476E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76403476E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.08660979E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.08660979E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.80183295E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.80183295E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.08795781E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.08795781E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.50459362E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.50459362E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.08660979E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.08660979E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.80183295E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.80183295E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.08795781E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.08795781E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.50459362E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.50459362E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.84812352E-03   # h decays
#          BR         NDA      ID1       ID2
     8.62226323E-01    2           5        -5   # BR(h -> b       bb     )
     7.90310210E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80458171E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.70067236E-04    2           3        -3   # BR(h -> s       sb     )
     2.83764378E-02    2           4        -4   # BR(h -> c       cb     )
     2.84986891E-02    2          21        21   # BR(h -> g       g      )
     6.63164269E-04    2          22        22   # BR(h -> gam     gam    )
     1.95623934E-04    2          24       -24   # BR(h -> W+      W-     )
     5.82160709E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84731352E+01   # H decays
#          BR         NDA      ID1       ID2
     2.48176437E-04    2           5        -5   # BR(H -> b       bb     )
     3.79800259E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34260278E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86477706E-07    2           3        -3   # BR(H -> s       sb     )
     1.10700001E-05    2           4        -4   # BR(H -> c       cb     )
     9.92936240E-01    2           6        -6   # BR(H -> t       tb     )
     1.42001499E-03    2          21        21   # BR(H -> g       g      )
     4.57305685E-06    2          22        22   # BR(H -> gam     gam    )
     1.68075909E-06    2          23        22   # BR(H -> Z       gam    )
     3.56518258E-04    2          24       -24   # BR(H -> W+      W-     )
     1.76357804E-04    2          23        23   # BR(H -> Z       Z      )
     1.40401625E-03    2          25        25   # BR(H -> h       h      )
     9.16304274E-19    2          36        36   # BR(H -> A       A      )
     1.47327232E-09    2          23        36   # BR(H -> Z       A      )
     3.79440560E-10    2          24       -37   # BR(H -> W+      H-     )
     3.79440560E-10    2         -24        37   # BR(H -> W-      H+     )
     3.67378873E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.37497972E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     1.08473338E-04    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     6.37497972E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     1.08473338E-04    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     9.28244826E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     2.71250010E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     9.28244826E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     2.71250010E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.17951301E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40913297E-04    2           5        -5   # BR(A -> b       bb     )
     3.57717472E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26452413E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89237940E-07    2           3        -3   # BR(A -> s       sb     )
     1.02315104E-05    2           4        -4   # BR(A -> c       cb     )
     9.97575831E-01    2           6        -6   # BR(A -> t       tb     )
     1.70732451E-03    2          21        21   # BR(A -> g       g      )
     5.53940725E-06    2          22        22   # BR(A -> gam     gam    )
     2.07249902E-06    2          23        22   # BR(A -> Z       gam    )
     3.24005468E-04    2          23        25   # BR(A -> Z       h      )
     9.79946846E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04440035E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97995794E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68524641E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30272720E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44401090E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89731925E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02548088E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.96614694E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.37986583E-04    2          24        25   # BR(H+ -> W+      h      )
     1.07600720E-11    2          24        36   # BR(H+ -> W+      A      )
     1.49983632E-03    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     1.49983632E-03    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
