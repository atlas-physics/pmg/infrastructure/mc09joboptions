# Z+jets production with a lepton filter and pT min = 150 GeV
# Author: Giacinto Piacquadio (31/10/2008)
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig.HerwigCommand += ["iproc 12150",    # note that 10000 is added to the herwig process number
                         "taudec TAUOLA",
                         "ptmin 150."]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

# Electron or Muon filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()

LeptonFilter= topAlg.LeptonFilter
LeptonFilter.Ptcut = 15.*GeV
LeptonFilter.Etacut = 2.7

try:
     StreamEVGEN.RequireAlgs += [ "LeptonFilter" ]
except Exception, e:
     pass

#---------------------------------------------------------------
#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.122
# measured 0.132 +/- 0.002
