###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
# needed (dummy) default 
from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()
Herwig = topAlg.Herwig 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  pass

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1


#TTbarLeptonFilter = topAlg.TTbarLeptonFilter
#TTbarLeptonFilter.Ptcut = 1.


#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

try:
     StreamEVGEN.VetoAlgs = [ "MultiLeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.efficiency = 0.31
evgenConfig.inputfilebase = 'mcatnlo33.109088.ttbarnolepfilt'
#evgenConfig.minevents = 500
#16130 ==> 5587
#eff(EF) = .34637321760694358338
#eff(EF)x0.9 = .31173589584624922504

#==============================================================
#
# End of job options file
#
###############################################################
