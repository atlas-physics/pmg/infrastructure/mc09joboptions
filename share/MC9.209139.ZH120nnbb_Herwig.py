###############################################################
#
# Job options file
#
# Herwig ZH production with a higgs and Z pT filter
# Author: Giacinto Piacquadio (8/7/2008)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )
Herwig.HerwigCommand += ["iproc 12705",
                         "rmass 201 120.0",
			 "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

# Add the filters:
from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
ParticleFilterHiggs= ParticleFilter(name = "ParticleFilterHiggs",
                                    StatusReq=195,
                                    PDG=25,
                                    Ptcut = 150.*GeV,
                                    Etacut = 3.0,
                                    Energycut = 1000000000.0)

MissingEtFilter= MissingEtFilter(name = "MissingEtFilter",
				 MEtcut = 100.*GeV)

topAlg += ParticleFilterHiggs
topAlg += MissingEtFilter

try:
     StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]
     StreamEVGEN.RequireAlgs += [ "ParticleFilterHiggs" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.0194
# 5000/yet
#==============================================================
#
# End of job options file
#
###############################################################
