# quark compositeness with pythia & destructive interference
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

#--------------------------------------------------------------
# File prepared by POD & FR
#--------------------------------------------------------------
Pythia.PythiaCommand += [
                         "pysubs msel 51",       # technicolor & compositeness
			 "pysubs ckin 3 70.", 
			 "pysubs ckin 4 140.",
                         "pytcsm rtcm 41 1500.", # compositeness scale
                         "pytcsm rtcm 42 1",     # positive interference
                         "pytcsm itcm 5 2"       # compositeness
                        ]
                              

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9
