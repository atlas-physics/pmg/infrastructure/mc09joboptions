#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019105E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00191052E+03   # EWSB                
         1     4.00000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     1.00000000E+02   # M_q1L               
        42     1.00000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     1.00000000E+02   # M_uR                
        45     1.00000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     1.00000000E+02   # M_dR                
        48     1.00000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05865963E+01   # W+
        25     7.17516550E+01   # h
        35     1.00942069E+03   # H
        36     1.00000000E+03   # A
        37     1.00332683E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.99466231E+02   # ~d_L
   2000001     3.99684854E+02   # ~d_R
   1000002     3.99957469E+02   # ~u_L
   2000002     3.99847456E+02   # ~u_R
   1000003     3.99466231E+02   # ~s_L
   2000003     3.99684854E+02   # ~s_R
   1000004     3.99957469E+02   # ~c_L
   2000004     3.99847456E+02   # ~c_R
   1000005     5.04739468E+03   # ~b_1
   2000005     5.04784328E+03   # ~b_2
   1000006     4.90993690E+03   # ~t_1
   2000006     5.04731499E+03   # ~t_2
   1000011     4.99998814E+03   # ~e_L
   2000011     4.99998826E+03   # ~e_R
   1000012     5.00002360E+03   # ~nu_eL
   1000013     4.99998814E+03   # ~mu_L
   2000013     4.99998826E+03   # ~mu_R
   1000014     5.00002360E+03   # ~nu_muL
   1000015     4.99982317E+03   # ~tau_1
   2000015     5.00015384E+03   # ~tau_2
   1000016     5.00002360E+03   # ~nu_tauL
   1000021     5.16720701E+03   # ~g
   1000022     3.80001572E+02   # ~chi_10
   1000023    -1.01782247E+03   # ~chi_20
   1000025     1.01944982E+03   # ~chi_30
   1000035     4.96415571E+03   # ~chi_40
   1000024     1.01627323E+03   # ~chi_1+
   1000037     4.96415554E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97511927E-01   # N_11
  1  2    -1.20165756E-03   # N_12
  1  3     4.91779344E-02   # N_13
  1  4    -5.04979480E-02   # N_14
  2  1     9.32774468E-04   # N_21
  2  2    -3.79770808E-04   # N_22
  2  3     7.07131304E-01   # N_23
  2  4     7.07081540E-01   # N_24
  3  1     7.04914750E-02   # N_31
  3  2     1.97685245E-02   # N_32
  3  3    -7.05225614E-01   # N_33
  3  4     7.05192874E-01   # N_34
  4  1     1.94528631E-04   # N_41
  4  2    -9.99803789E-01   # N_42
  4  3    -1.42717129E-02   # N_43
  4  4     1.37354699E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01810953E-02   # U_11
  1  2     9.99796341E-01   # U_12
  2  1     9.99796341E-01   # U_21
  2  2     2.01810953E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94226217E-02   # V_11
  1  2     9.99811363E-01   # V_12
  2  1     9.99811363E-01   # V_21
  2  2     1.94226217E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07202871E-01   # cos(theta_t)
  1  2     9.94237167E-01   # sin(theta_t)
  2  1    -9.94237167E-01   # -sin(theta_t)
  2  2     1.07202871E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19511142E-01   # cos(theta_b)
  1  2     6.94480897E-01   # sin(theta_b)
  2  1    -6.94480897E-01   # -sin(theta_b)
  2  2     7.19511142E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07235406E-01   # cos(theta_tau)
  1  2     7.06978133E-01   # sin(theta_tau)
  2  1    -7.06978133E-01   # -sin(theta_tau)
  2  2     7.07235406E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21947979E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00191052E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43591080E-01   # tanbeta(Q)          
         3     2.45399313E+02   # vev(Q)              
         4     1.10469590E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00191052E+03  # The gauge couplings
     1     3.66678548E-01   # gprime(Q) DRbar
     2     6.37271514E-01   # g(Q) DRbar
     3     1.02093111E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00191052E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00191052E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00191052E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00191052E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17035373E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00191052E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87008045E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00191052E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38826892E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00191052E+03  # The soft SUSY breaking masses at the scale Q
         1     4.00000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.05355981E+05   # M^2_Hd              
        22     6.76853348E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     1.00000000E+02   # M_q1L               
        42     1.00000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     1.00000000E+02   # M_uR                
        45     1.00000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     1.00000000E+02   # M_dR                
        48     1.00000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40153512E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.72844236E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24166497E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24166497E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24158280E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24158280E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24148026E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24148026E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24152165E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24152165E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24166497E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24166497E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24158280E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24158280E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24148026E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24148026E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24152165E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24152165E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.39665933E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.39665933E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.48663427E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.48663427E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.86677016E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.86677016E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48617424E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68206195E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41449740E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.37828147E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.73901493E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26234013E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.54240494E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86551314E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88962292E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67308526E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.74612833E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.77526812E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.09969945E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.40526027E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.80838079E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53566963E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.79512468E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.88743108E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71476002E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.81632359E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.28294184E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.82416691E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.42866731E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.38172376E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.89942941E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.70964653E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.77303791E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.02509449E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     1.64708174E-02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.01220895E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     4.06667004E-03   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.02509449E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     1.64708174E-02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.01220895E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     4.06667004E-03   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.64474719E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.85702511E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.87904778E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01625855E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.19624508E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27569016E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.23951979E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.64311395E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95379882E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.09053664E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.61930937E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.81107461E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.64474719E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.85702511E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.87904778E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01625855E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.19624508E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27569016E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.23951979E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.64311395E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95379882E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.09053664E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.61930937E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.81107461E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.65865645E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91367835E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54103450E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.73884531E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.22767334E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.97055860E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.45890673E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65421477E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.92341776E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.10671934E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.76777476E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.25511259E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.89703770E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.51369510E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.63929592E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94815351E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.34792308E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.20806021E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.21630751E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10959659E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.24301375E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.63929592E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94815351E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.34792308E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.20806021E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.21630751E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10959659E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.24301375E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.65689407E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.92185459E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.34171611E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.20486659E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.19987409E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.74909719E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.23824821E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02866731E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.51704844E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.80030698E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.51704844E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.80030698E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.85365289E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06003414E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88352782E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88351912E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88352782E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88351912E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.19680814E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.45539612E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.24005518E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.28612782E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.27510589E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.54405924E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77158231E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.77153914E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     8.89258884E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.77047154E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76336619E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05360103E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99694169E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.87708468E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.99309656E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.99309656E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     2.53008733E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     2.53008733E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.54300076E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.54300076E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     6.32688129E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     6.32688129E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.99309656E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.99309656E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     2.53008733E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     2.53008733E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.54300076E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.54300076E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     6.32688129E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     6.32688129E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.17393123E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.64646909E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.20941723E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.92507106E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.92507106E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.30008976E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.30008976E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.73669548E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.73669548E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.25107561E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.25107561E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.92507106E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.92507106E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.30008976E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.30008976E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.73669548E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.73669548E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.25107561E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.25107561E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06003574E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.62507820E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.45471791E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.62904474E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.28520317E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.28520317E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.31382919E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.82007507E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     8.85713772E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.64937582E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76390324E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.55177701E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.53071216E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.60060475E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75884122E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.77253013E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.77253013E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13518571E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13518571E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.03558178E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.03558178E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13671729E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13671729E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.08898540E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.08898540E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13518571E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13518571E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.03558178E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.03558178E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13671729E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13671729E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.08898540E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.08898540E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.89400373E-03   # h decays
#          BR         NDA      ID1       ID2
     8.60523814E-01    2           5        -5   # BR(h -> b       bb     )
     7.93455411E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.81514982E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.68510872E-04    2           3        -3   # BR(h -> s       sb     )
     2.82813122E-02    2           4        -4   # BR(h -> c       cb     )
     2.98794282E-02    2          21        21   # BR(h -> g       g      )
     7.07877282E-04    2          22        22   # BR(h -> gam     gam    )
     2.41086168E-04    2          24       -24   # BR(h -> W+      W-     )
     7.09152316E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86421123E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46240098E-04    2           5        -5   # BR(H -> b       bb     )
     3.76854217E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33218845E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85000670E-07    2           3        -3   # BR(H -> s       sb     )
     1.10818794E-05    2           4        -4   # BR(H -> c       cb     )
     9.94057299E-01    2           6        -6   # BR(H -> t       tb     )
     1.42170672E-03    2          21        21   # BR(H -> g       g      )
     4.39968164E-06    2          22        22   # BR(H -> gam     gam    )
     1.68080090E-06    2          23        22   # BR(H -> Z       gam    )
     3.79171931E-04    2          24       -24   # BR(H -> W+      W-     )
     1.87564904E-04    2          23        23   # BR(H -> Z       Z      )
     1.39838917E-03    2          25        25   # BR(H -> h       h      )
     1.03072561E-18    2          36        36   # BR(H -> A       A      )
     1.63769514E-09    2          23        36   # BR(H -> Z       A      )
     4.52164291E-10    2          24       -37   # BR(H -> W+      H-     )
     4.52164291E-10    2         -24        37   # BR(H -> W-      H+     )
     1.73943229E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.22052589E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.18090640E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.22052589E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.18090640E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.15316747E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.79645643E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.15316747E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.79645643E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.20043879E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39019806E-04    2           5        -5   # BR(A -> b       bb     )
     3.54868606E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25445345E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87762553E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308142E-05    2           4        -4   # BR(A -> c       cb     )
     9.97507957E-01    2           6        -6   # BR(A -> t       tb     )
     1.70717575E-03    2          21        21   # BR(A -> g       g      )
     5.53894354E-06    2          22        22   # BR(A -> gam     gam    )
     2.07310663E-06    2          23        22   # BR(A -> Z       gam    )
     3.43892890E-04    2          23        25   # BR(A -> Z       h      )
     1.48311747E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05925842E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95398608E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65984886E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29374922E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.42728444E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83603499E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02637649E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97623033E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.59115682E-04    2          24        25   # BR(H+ -> W+      h      )
     1.05605432E-11    2          24        36   # BR(H+ -> W+      A      )
     9.85226623E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     9.85226623E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
