###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with csc_evgen08_trf.py                    #
#                                                         #
#  Revised by C. Ohm for MC9 production 2008-09-22        #
#                                                         #
###########################################################

MASS=600
CASE='gluino'

include("MC09JobOptions/MC9_Pythia_R-Hadron_Common.py")
