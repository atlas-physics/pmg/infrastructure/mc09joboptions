#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019078E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190781E+03   # EWSB                
         1     4.07100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889140E+01   # W+
        25     8.65631928E+01   # h
        35     1.00954282E+03   # H
        36     1.00000000E+03   # A
        37     1.00338058E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04404051E+03   # ~d_L
   2000001     5.04405633E+03   # ~d_R
   1000002     5.04407608E+03   # ~u_L
   2000002     5.04406814E+03   # ~u_R
   1000003     5.04404051E+03   # ~s_L
   2000003     5.04405633E+03   # ~s_R
   1000004     5.04407608E+03   # ~c_L
   2000004     5.04406814E+03   # ~c_R
   1000005     5.04382481E+03   # ~b_1
   2000005     5.04427313E+03   # ~b_2
   1000006     4.90856451E+03   # ~t_1
   2000006     5.04263863E+03   # ~t_2
   1000011     4.99998822E+03   # ~e_L
   2000011     4.99998828E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998822E+03   # ~mu_L
   2000013     4.99998828E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015299E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.99995857E+02   # ~g
   1000022     3.99892072E+02   # ~chi_10
   1000023    -1.01757626E+03   # ~chi_20
   1000025     1.01930850E+03   # ~chi_30
   1000035     5.02219709E+03   # ~chi_40
   1000024     1.01607734E+03   # ~chi_1+
   1000037     5.02219693E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97387221E-01   # N_11
  1  2    -1.21003880E-03   # N_12
  1  3     5.04160909E-02   # N_13
  1  4    -5.17250976E-02   # N_14
  2  1     9.25063897E-04   # N_21
  2  2    -3.77643493E-04   # N_22
  2  3     7.07130936E-01   # N_23
  2  4     7.07081920E-01   # N_24
  3  1     7.22346276E-02   # N_31
  3  2     1.93004300E-02   # N_32
  3  3    -7.05145181E-01   # N_33
  3  4     7.05109867E-01   # N_34
  4  1     1.86967773E-04   # N_41
  4  2    -9.99812926E-01   # N_42
  4  3    -1.39402619E-02   # N_43
  4  4     1.34069962E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97124825E-02   # U_11
  1  2     9.99805690E-01   # U_12
  2  1     9.99805690E-01   # U_21
  2  2     1.97124825E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89582251E-02   # V_11
  1  2     9.99820277E-01   # V_12
  2  1     9.99820277E-01   # V_21
  2  2     1.89582251E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04458555E-01   # cos(theta_t)
  1  2     9.94529241E-01   # sin(theta_t)
  2  1    -9.94529241E-01   # -sin(theta_t)
  2  2     1.04458555E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19473382E-01   # cos(theta_b)
  1  2     6.94520016E-01   # sin(theta_b)
  2  1    -6.94520016E-01   # -sin(theta_b)
  2  2     7.19473382E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173062E-01   # cos(theta_tau)
  1  2     7.07040494E-01   # sin(theta_tau)
  2  1    -7.07040494E-01   # -sin(theta_tau)
  2  2     7.07173062E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22387493E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190781E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.42861282E-01   # tanbeta(Q)          
         3     2.44085489E+02   # vev(Q)              
         4     1.10493370E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190781E+03  # The gauge couplings
     1     3.65889265E-01   # gprime(Q) DRbar
     2     6.34849288E-01   # g(Q) DRbar
     3     1.01824811E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190781E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190781E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190781E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190781E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17636150E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190781E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87621391E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190781E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38877229E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190781E+03  # The soft SUSY breaking masses at the scale Q
         1     4.07100000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.67700000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03289213E+05   # M^2_Hd              
        22     6.88650918E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39080072E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.22931760E-08   # gluino decays
#          BR         NDA      ID1       ID2
     3.45091700E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.88106732E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.34854797E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.88106732E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.34854797E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.81598892E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.85797981E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.98289189E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07098247E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.05752419E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.11874020E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.55446395E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.64144010E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.86728152E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.38797975E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.39079606E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.83834109E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.33839002E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.17217747E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.34636007E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.80756858E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.02511421E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.41234896E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.21279253E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.79000040E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11784387E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.65682793E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.29739562E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     9.31507059E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.98365794E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.50919989E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.00546990E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.02303778E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09436472E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.57049245E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.38220114E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.90612979E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.34731955E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.10704513E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.18113619E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.00509800E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.16129994E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.92344478E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.32338800E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97668820E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.45435950E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.30845590E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.67208880E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.62885902E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.32426998E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.66752528E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.34728935E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16095220E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.68303724E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.60449241E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.15441387E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.24184912E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.30817819E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97670382E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.36821463E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.48266068E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.85105568E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.17628479E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.13301128E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91475570E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.34731955E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.10704513E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.18113619E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.00509800E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.16129994E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.92344478E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.32338800E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97668820E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.45435950E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.30845590E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.67208880E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.62885902E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.32426998E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.66752528E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.34728935E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16095220E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.68303724E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.60449241E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.15441387E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.24184912E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.30817819E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97670382E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.36821463E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.48266068E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.85105568E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.17628479E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.13301128E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91475570E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.59434643E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87461424E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.75465195E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.03672091E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.17129938E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.62843353E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95142379E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.96769821E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85682431E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.59434643E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87461424E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.75465195E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.03672091E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.17129938E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.62843353E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95142379E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.96769821E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85682431E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.64891970E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91578735E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.57555882E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.93170583E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.93200323E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64423556E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.92441026E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.14383527E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.03934922E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.24154033E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.58921223E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96594180E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.31888851E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.39360151E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.00989918E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.58921223E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96594180E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.31888851E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.39360151E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.00989918E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.60683405E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93936053E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.31270354E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.38988448E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.67174935E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.99270030E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51193391E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.18145570E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07991693E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07991693E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07311237E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08646530E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08646530E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05795727E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02853817E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78525264E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.34868834E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71591633E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71109253E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85277877E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.32097759E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.32106118E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.86524308E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31669059E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.28505476E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.03909906E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99728927E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.71072704E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.08146507E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.65786742E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99834213E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51191022E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.63102009E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78495570E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.76948682E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71555958E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71555958E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.77158472E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.66146027E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.85015330E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.11743116E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28941484E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.48985312E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84685483E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.34266671E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.26656134E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32478865E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32478865E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04304114E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04304114E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.84737686E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.84737686E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04304114E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04304114E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.84737686E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.84737686E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27199636E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27199636E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12455293E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12455293E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04018557E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04018557E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04018557E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04018557E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04018557E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04018557E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.23612259E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45532952E-01    2           5        -5   # BR(h -> b       bb     )
     8.11799159E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87692212E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56384805E-04    2           3        -3   # BR(h -> s       sb     )
     2.77504427E-02    2           4        -4   # BR(h -> c       cb     )
     4.20317934E-02    2          21        21   # BR(h -> g       g      )
     1.10359315E-03    2          22        22   # BR(h -> gam     gam    )
     1.16962093E-03    2          24       -24   # BR(h -> W+      W-     )
     2.87604779E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86184635E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47104209E-04    2           5        -5   # BR(H -> b       bb     )
     3.76452163E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33076717E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.84790618E-07    2           3        -3   # BR(H -> s       sb     )
     1.11065588E-05    2           4        -4   # BR(H -> c       cb     )
     9.96304424E-01    2           6        -6   # BR(H -> t       tb     )
     1.42479284E-03    2          21        21   # BR(H -> g       g      )
     4.73356522E-06    2          22        22   # BR(H -> gam     gam    )
     1.68499410E-06    2          23        22   # BR(H -> Z       gam    )
     3.84885691E-04    2          24       -24   # BR(H -> W+      W-     )
     1.90392240E-04    2          23        23   # BR(H -> Z       Z      )
     1.39276446E-03    2          25        25   # BR(H -> h       h      )
     1.10468521E-18    2          36        36   # BR(H -> A       A      )
     1.74753988E-09    2          23        36   # BR(H -> Z       A      )
     4.78328273E-10    2          24       -37   # BR(H -> W+      H-     )
     4.78328273E-10    2         -24        37   # BR(H -> W-      H+     )
     1.45705707E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20847831E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39358573E-04    2           5        -5   # BR(A -> b       bb     )
     3.53772981E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25058043E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87195226E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308420E-05    2           4        -4   # BR(A -> c       cb     )
     9.97510664E-01    2           6        -6   # BR(A -> t       tb     )
     1.70716769E-03    2          21        21   # BR(A -> g       g      )
     5.53873928E-06    2          22        22   # BR(A -> gam     gam    )
     2.07429890E-06    2          23        22   # BR(A -> Z       gam    )
     3.45718423E-04    2          23        25   # BR(A -> Z       h      )
     1.43557720E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05737474E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97150514E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65574669E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29229911E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43829341E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.82606908E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02833956E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99590815E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.61806511E-04    2          24        25   # BR(H+ -> W+      h      )
     1.14453727E-11    2          24        36   # BR(H+ -> W+      A      )
