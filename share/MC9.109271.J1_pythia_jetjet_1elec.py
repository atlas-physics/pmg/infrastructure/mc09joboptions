################################################################
#
# Pythia Dijet J1 with 1 electron EF
#
# Responsible person(s)
#   Nov 12, 2008 : Junichi Tanaka (Junichi.Tanaka@cern.ch)
#
################################################################
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )

# dijet production with pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )
Pythia.PythiaCommand += [
			      "pysubs msel 0",
			      "pysubs ckin 3 17.",
			      "pysubs ckin 4 35.",
			      "pysubs msub 11 1",
			      "pysubs msub 12 1",
			      "pysubs msub 13 1",
			      "pysubs msub 68 1",
			      "pysubs msub 28 1",
			      "pysubs msub 53 1"]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
topAlg += ElectronFilter()

ElectronFilter = topAlg.ElectronFilter
ElectronFilter.Ptcut = 8000.
ElectronFilter.Etacut = 3.0

try:
    StreamEVGEN.RequireAlgs +=  [ "ElectronFilter" ]
except Exception, e:
    pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.000981
# 14.2.24.3 eff=5000/4567516=0.00109
