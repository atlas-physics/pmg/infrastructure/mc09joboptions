#==============================================================
#
# Job options file
# Martin Flechl, July 6, 2009
# (Martin.Flechl@cern.ch)
#
#==============================================================

import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#load relevant libraries
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

from Sherpa_i.Sherpa_iConf import ReadSherpa_i
sherpa = ReadSherpa_i()
sherpa.Files = [ "sherpa.evts" ]

topAlg += sherpa

from MC09JobOptions.SherpaEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'Sherpa10102.107649.Sherpattbar_tauhadplusjets'
evgenConfig.efficiency = 0.95

