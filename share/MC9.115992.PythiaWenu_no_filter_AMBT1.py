###############################################################
#
# Job options file for inclusive Wenu
#
# author: C. Gwenlan (June'10)
# AMBT1 tune [ATLAS-CONF-2010-031]
# equivalent MC9 JO: MC9.106043.PythiaWenu_no_filter.py
#===============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC09JobOptions/MC9_PythiaAMBT1_Common.py" )

Pythia.PythiaCommand += [ "pysubs msel 0",         # Users decay choice.
                         "pydat1 parj 90 20000",   # Turn off FSR.
                         "pydat3 mdcy 15 1 0",     # Turn off tau decays.
                         "pysubs msub 2 1",        # Create W bosons.
                         "pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 206 1 1",    # Switch for W->enu.
                         "pydat3 mdme 207 1 0",    # Switch for W->munu.
                         "pydat3 mdme 208 1 0"]    # Switch for W->taunu.

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# efficiency = 0.9 - no filtering
from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.90

#==============================================================
#
# End of job options file
#
###############################################################
