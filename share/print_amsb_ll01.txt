 ********************************************
 *                                          *
 *  ISAJET     V7.78   27-MAR-2008 12:16:18 *
 *                                          *
 ********************************************



 Anomaly-mediated SUSY breaking model:

 WARNING IN SSXINT: BAD CONVERGENCE FOR  512 INTERVALS.
 WARNING IN SSXINT: BAD CONVERGENCE FOR  473 INTERVALS.
 WARNING IN SSXINT: BAD CONVERGENCE FOR  507 INTERVALS.
 WARNING IN SSXINT: BAD CONVERGENCE FOR  512 INTERVALS.
 WARNING IN SSXINT: BAD CONVERGENCE FOR  473 INTERVALS.
 WARNING IN SSXINT: BAD CONVERGENCE FOR  507 INTERVALS.
 M_0,  M_(3/2),  tan(beta),  sgn(mu),  M_t =
  1500.000 32000.000     5.000     1.0   172.500

 ISASUGRA unification:
 M_GUT      = 0.127E+17   g_GUT          =0.702      alpha_GUT =0.039
 FT_GUT     = 0.529       FB_GUT         = 0.026     FL_GUT = 0.034

 1/alpha_em =  127.85     sin**2(thetaw) =0.2309     a_s^DRB   =  0.119
 M_1        =  294.72     M_2            =   89.35   M_3       = -695.15
 mu(Q)      =  649.50     B(Q)           =  834.67   Q         = 1126.84
 M_Hd^2     = 0.219E+07   M_Hu^2         =-0.291E+06 TANBQ     =    4.810

 ISAJET masses (with signs):
 M(GL)  =   801.32
 M(UL)  =  1620.68   M(UR)  =  1628.73   M(DL)  =  1622.54   M(DR) =  1632.26
 M(B1)  =  1341.76   M(B2)  =  1624.37   M(T1)  =   991.24   M(T2) =  1354.36
 M(SN)  =  1489.70   M(EL)  =  1491.42   M(ER)  =  1492.21
 M(NTAU)=  1488.03   M(TAU1)=  1487.22   M(TAU2)=  1491.03
 M(Z1)  =   -90.04   M(Z2)  =  -291.39   M(Z3)  =   658.08   M(Z4) =  -666.20
 M(W1)  =   -90.24   M(W2)  =  -665.80
 M(HL)  =   108.45   M(HH)  =  1662.69   M(HA)  =  1651.38   M(H+) =  1664.15

 theta_t=  -1.5137   theta_b=   0.0069   theta_l=   0.8957   alpha_h=   0.1988

 NEUTRALINO MASSES (SIGNED) =   -90.036  -291.385   658.077  -666.196
 EIGENVECTOR 1       =   0.04223  -0.12329  -0.99139   0.01294
 EIGENVECTOR 2       =  -0.04931   0.08334  -0.02546  -0.99497
 EIGENVECTOR 3       =   0.70836   0.70310  -0.05694   0.02524
 EIGENVECTOR 4       =  -0.70286   0.69535  -0.11516   0.09603

 CHARGINO MASSES (SIGNED)  =   -90.236  -665.803
 GAMMAL, GAMMAR             =   1.74108   1.62920


 ISAJET equivalent input:
 MSSMA:   801.32  649.50 1651.38    5.00
 MSSMB:  1587.92 1598.45 1595.49 1486.29 1490.14
 MSSMC:  1313.36 1597.73  966.81 1484.67 1486.98  579.64 1331.65  339.82
 MSSMD: SAME AS MSSMB (DEFAULT)
 MSSME:   294.72   89.35


 Output from ISATOOLS:
 Delta a_mu       =  0.00000E+00    BF(b->s gamma)    =  0.00000E+00
 Omega h^2        =  0.00000E+00    <s.v>(v=0)[cm^3/s]=  0.00000E+00
 BF(Bs -> mu mu)  =  0.00000E+00    BF(B -> tau tau)  =  0.00000E+00
 LSP-nucleon spin independent (SI) and dependent (SD) sigmas:
 sigma(p,SI)[pb]  =  0.00000E+00    sigma(n,SI)[pb]   =  0.00000E+00
 sigma(p,SD)[pb]  =  0.00000E+00    sigma(n,SD)[pb]   =  0.00000E+00


 ISASUSY decay modes:
 Parent --> daughters                  Width          Branching ratio

 TP     -->  UP     DB     BT                0.38227E+00    0.33333E+00
 TP     -->  CH     SB     BT                0.38227E+00    0.33333E+00
 TP     -->  E+     NUE    BT                0.12742E+00    0.11111E+00
 TP     -->  MU+    NUM    BT                0.12742E+00    0.11111E+00
 TP     -->  TAU+   NUT    BT                0.12742E+00    0.11111E+00
  
 GLSS   -->  W1SS+  DN     UB                0.22974E-03    0.84638E-01
 GLSS   -->  W1SS-  UP     DB                0.22974E-03    0.84638E-01
 GLSS   -->  W1SS+  ST     CB                0.22974E-03    0.84638E-01
 GLSS   -->  W1SS-  CH     SB                0.22974E-03    0.84638E-01
 GLSS   -->  W1SS+  BT     TB                0.39675E-03    0.14616E+00
 GLSS   -->  W1SS-  TP     BB                0.39675E-03    0.14616E+00
 GLSS   -->  W2SS+  DN     UB                0.60048E-08    0.22122E-05
 GLSS   -->  W2SS-  UP     DB                0.60048E-08    0.22122E-05
 GLSS   -->  W2SS+  ST     CB                0.60048E-08    0.22122E-05
 GLSS   -->  W2SS-  CH     SB                0.60048E-08    0.22122E-05
 GLSS   -->  Z1SS   GL                       0.19767E-06    0.72823E-04
 GLSS   -->  Z1SS   UP     UB                0.11459E-03    0.42214E-01
 GLSS   -->  Z1SS   DN     DB                0.11508E-03    0.42396E-01
 GLSS   -->  Z1SS   ST     SB                0.11508E-03    0.42396E-01
 GLSS   -->  Z1SS   CH     CB                0.11459E-03    0.42214E-01
 GLSS   -->  Z1SS   BT     BB                0.28005E-03    0.10317E+00
 GLSS   -->  Z1SS   TP     TB                0.12776E-03    0.47067E-01
 GLSS   -->  Z2SS   GL                       0.33720E-05    0.12423E-02
 GLSS   -->  Z2SS   UP     UB                0.21967E-04    0.80927E-02
 GLSS   -->  Z2SS   DN     DB                0.59748E-05    0.22012E-02
 GLSS   -->  Z2SS   ST     SB                0.59748E-05    0.22012E-02
 GLSS   -->  Z2SS   CH     CB                0.21967E-04    0.80926E-02
 GLSS   -->  Z2SS   BT     BB                0.76163E-05    0.28059E-02
 GLSS   -->  Z2SS   TP     TB                0.53210E-04    0.19603E-01
 GLSS   -->  Z3SS   GL                       0.80402E-05    0.29621E-02
 GLSS   -->  Z3SS   UP     UB                0.16026E-08    0.59043E-06
 GLSS   -->  Z3SS   DN     DB                0.20099E-08    0.74045E-06
 GLSS   -->  Z3SS   ST     SB                0.20099E-08    0.74045E-06
 GLSS   -->  Z3SS   CH     CB                0.16026E-08    0.59043E-06
 GLSS   -->  Z3SS   BT     BB                0.22953E-07    0.84561E-05
 GLSS   -->  Z4SS   GL                       0.63823E-05    0.23513E-02
 GLSS   -->  Z4SS   UP     UB                0.19347E-08    0.71274E-06
 GLSS   -->  Z4SS   DN     DB                0.25234E-08    0.92964E-06
 GLSS   -->  Z4SS   ST     SB                0.25234E-08    0.92964E-06
 GLSS   -->  Z4SS   CH     CB                0.19346E-08    0.71274E-06
 GLSS   -->  Z4SS   BT     BB                0.21469E-07    0.79095E-05
  
 UPL    -->  Z1SS   UP                       0.66318E+01    0.88853E-01
 UPL    -->  Z2SS   UP                       0.27569E+00    0.36937E-02
 UPL    -->  Z3SS   UP                       0.13021E-01    0.17446E-03
 UPL    -->  Z4SS   UP                       0.44851E-01    0.60091E-03
 UPL    -->  GLSS   UP                       0.54127E+02    0.72520E+00
 UPL    -->  W1SS+  DN                       0.13513E+02    0.18105E+00
 UPL    -->  W2SS+  DN                       0.32124E-01    0.43040E-03
  
 DNL    -->  Z1SS   DN                       0.67033E+01    0.89759E-01
 DNL    -->  Z2SS   DN                       0.15732E+00    0.21066E-02
 DNL    -->  Z3SS   DN                       0.18069E-01    0.24194E-03
 DNL    -->  Z4SS   DN                       0.83204E-01    0.11141E-02
 DNL    -->  GLSS   DN                       0.54263E+02    0.72659E+00
 DNL    -->  W1SS-  UP                       0.13185E+02    0.17655E+00
 DNL    -->  W2SS-  UP                       0.27130E+00    0.36327E-02
  
 STL    -->  Z1SS   ST                       0.67033E+01    0.89759E-01
 STL    -->  Z2SS   ST                       0.15732E+00    0.21066E-02
 STL    -->  Z3SS   ST                       0.18069E-01    0.24194E-03
 STL    -->  Z4SS   ST                       0.83204E-01    0.11141E-02
 STL    -->  GLSS   ST                       0.54263E+02    0.72659E+00
 STL    -->  W1SS-  CH                       0.13185E+02    0.17655E+00
 STL    -->  W2SS-  CH                       0.27130E+00    0.36327E-02
  
 CHL    -->  Z1SS   CH                       0.66318E+01    0.88853E-01
 CHL    -->  Z2SS   CH                       0.27569E+00    0.36937E-02
 CHL    -->  Z3SS   CH                       0.13021E-01    0.17446E-03
 CHL    -->  Z4SS   CH                       0.44851E-01    0.60091E-03
 CHL    -->  GLSS   CH                       0.54127E+02    0.72520E+00
 CHL    -->  W1SS+  ST                       0.13513E+02    0.18105E+00
 CHL    -->  W2SS+  ST                       0.32124E-01    0.43040E-03
  
 BT1    -->  Z1SS   BT                       0.55303E+01    0.90714E-01
 BT1    -->  Z2SS   BT                       0.12735E+00    0.20889E-02
 BT1    -->  Z3SS   BT                       0.47375E-01    0.77709E-03
 BT1    -->  Z4SS   BT                       0.90401E-01    0.14829E-02
 BT1    -->  GLSS   BT                       0.33096E+02    0.54288E+00
 BT1    -->  W1SS-  TP                       0.10550E+02    0.17305E+00
 BT1    -->  W2SS-  TP                       0.11081E+02    0.18176E+00
 BT1    -->  W-     TP1                      0.44222E+00    0.72538E-02
  
 TP1    -->  GLSS   TP                       0.30621E+01    0.24027E+00
 TP1    -->  Z1SS   TP                       0.19363E-02    0.15194E-03
 TP1    -->  Z2SS   TP                       0.16675E+01    0.13085E+00
 TP1    -->  Z3SS   TP                       0.17473E+01    0.13711E+00
 TP1    -->  Z4SS   TP                       0.19124E+01    0.15006E+00
 TP1    -->  W1SS+  BT                       0.31990E-02    0.25102E-03
 TP1    -->  W2SS+  BT                       0.43496E+01    0.34131E+00
  
 UPR    -->  Z1SS   UP                       0.61318E-03    0.10543E-04
 UPR    -->  Z2SS   UP                       0.34153E+01    0.58725E-01
 UPR    -->  Z3SS   UP                       0.16422E-02    0.28237E-04
 UPR    -->  Z4SS   UP                       0.23541E-01    0.40477E-03
 UPR    -->  GLSS   UP                       0.54717E+02    0.94083E+00
  
 DNR    -->  Z1SS   DN                       0.15363E-03    0.27514E-05
 DNR    -->  Z2SS   DN                       0.85593E+00    0.15329E-01
 DNR    -->  Z3SS   DN                       0.41214E-03    0.73810E-05
 DNR    -->  Z4SS   DN                       0.59081E-02    0.10581E-03
 DNR    -->  GLSS   DN                       0.54975E+02    0.98456E+00
  
 STR    -->  Z1SS   ST                       0.15363E-03    0.27514E-05
 STR    -->  Z2SS   ST                       0.85593E+00    0.15329E-01
 STR    -->  Z3SS   ST                       0.41214E-03    0.73810E-05
 STR    -->  Z4SS   ST                       0.59081E-02    0.10581E-03
 STR    -->  GLSS   ST                       0.54975E+02    0.98456E+00
  
 CHR    -->  Z1SS   CH                       0.61318E-03    0.10543E-04
 CHR    -->  Z2SS   CH                       0.34153E+01    0.58725E-01
 CHR    -->  Z3SS   CH                       0.16422E-02    0.28237E-04
 CHR    -->  Z4SS   CH                       0.23540E-01    0.40477E-03
 CHR    -->  GLSS   CH                       0.54717E+02    0.94083E+00
  
 BT2    -->  Z1SS   BT                       0.10330E-02    0.18620E-04
 BT2    -->  Z2SS   BT                       0.85155E+00    0.15349E-01
 BT2    -->  Z3SS   BT                       0.53026E-01    0.95581E-03
 BT2    -->  Z4SS   BT                       0.57622E-01    0.10387E-02
 BT2    -->  GLSS   BT                       0.54399E+02    0.98056E+00
 BT2    -->  W1SS-  TP                       0.16062E-02    0.28952E-04
 BT2    -->  W2SS-  TP                       0.10309E+00    0.18582E-02
 BT2    -->  W-     TP1                      0.10530E-03    0.18980E-05
 BT2    -->  W-     TP2                      0.32876E-02    0.59261E-04
 BT2    -->  Z0     BT1                      0.18436E-02    0.33231E-04
 BT2    -->  HL0    BT1                      0.53704E-02    0.96804E-04
  
 TP2    -->  GLSS   TP                       0.30608E+02    0.51400E+00
 TP2    -->  W1SS+  BT                       0.11328E+02    0.19022E+00
 TP2    -->  W2SS+  BT                       0.71830E-01    0.12062E-02
 TP2    -->  Z0     TP1                      0.24120E+00    0.40504E-02
 TP2    -->  HL0    TP1                      0.63145E+00    0.10604E-01
 TP2    -->  Z1SS   TP                       0.53982E+01    0.90651E-01
 TP2    -->  Z2SS   TP                       0.30907E+00    0.51901E-02
 TP2    -->  Z3SS   TP                       0.56838E+01    0.95446E-01
 TP2    -->  Z4SS   TP                       0.52779E+01    0.88630E-01
  
 EL-    -->  Z1SS   E-                       0.60377E+01    0.29742E+00
 EL-    -->  Z2SS   E-                       0.19023E+01    0.93707E-01
 EL-    -->  Z3SS   E-                       0.75508E-02    0.37195E-03
 EL-    -->  Z4SS   E-                       0.15655E-01    0.77116E-03
 EL-    -->  W1SS-  NUE                      0.12106E+02    0.59634E+00
 EL-    -->  W2SS-  NUE                      0.23118E+00    0.11388E-01
  
 MUL-   -->  Z1SS   MU-                      0.60377E+01    0.29742E+00
 MUL-   -->  Z2SS   MU-                      0.19023E+01    0.93707E-01
 MUL-   -->  Z3SS   MU-                      0.75508E-02    0.37195E-03
 MUL-   -->  Z4SS   MU-                      0.15655E-01    0.77116E-03
 MUL-   -->  W1SS-  NUM                      0.12106E+02    0.59634E+00
 MUL-   -->  W2SS-  NUM                      0.23118E+00    0.11388E-01
  
 TAU1-  -->  Z1SS   TAU-                     0.24354E+01    0.19752E+00
 TAU1-  -->  Z2SS   TAU-                     0.49896E+01    0.40469E+00
 TAU1-  -->  Z3SS   TAU-                     0.73347E-02    0.59489E-03
 TAU1-  -->  Z4SS   TAU-                     0.65239E-02    0.52913E-03
 TAU1-  -->  W1SS-  NUT                      0.48735E+01    0.39528E+00
 TAU1-  -->  W2SS-  NUT                      0.17139E-01    0.13901E-02
  
 NUEL   -->  Z1SS   NUE                      0.62062E+01    0.30496E+00
 NUEL   -->  Z2SS   NUE                      0.15768E+01    0.77480E-01
 NUEL   -->  Z3SS   NUE                      0.20367E-01    0.10008E-02
 NUEL   -->  Z4SS   NUE                      0.11319E+00    0.55619E-02
 NUEL   -->  W1SS+  E-                       0.12407E+02    0.60965E+00
 NUEL   -->  W2SS+  E-                       0.27367E-01    0.13448E-02
  
 NUML   -->  Z1SS   NUM                      0.62062E+01    0.30496E+00
 NUML   -->  Z2SS   NUM                      0.15768E+01    0.77480E-01
 NUML   -->  Z3SS   NUM                      0.20367E-01    0.10008E-02
 NUML   -->  Z4SS   NUM                      0.11319E+00    0.55619E-02
 NUML   -->  W1SS+  MU-                      0.12407E+02    0.60965E+00
 NUML   -->  W2SS+  MU-                      0.27367E-01    0.13448E-02
  
 NUTL   -->  Z1SS   NUT                      0.61992E+01    0.30424E+00
 NUTL   -->  Z2SS   NUT                      0.15747E+01    0.77283E-01
 NUTL   -->  Z3SS   NUT                      0.20323E-01    0.99737E-03
 NUTL   -->  Z4SS   NUT                      0.11294E+00    0.55426E-02
 NUTL   -->  W1SS+  TAU-                     0.12395E+02    0.60830E+00
 NUTL   -->  W2SS+  TAU-                     0.74177E-01    0.36403E-02
 NUTL   -->  TAU1-  NUE    E+                0.44652E-13    0.21914E-14
 NUTL   -->  TAU1-  NUM    MU+               0.44652E-13    0.21914E-14
 NUTL   -->  TAU1-  UP     DB                0.13396E-12    0.65741E-14
  
 ER-    -->  Z1SS   E-                       0.12625E-02    0.18034E-03
 ER-    -->  Z2SS   E-                       0.69516E+01    0.99296E+00
 ER-    -->  Z3SS   E-                       0.31372E-02    0.44812E-03
 ER-    -->  Z4SS   E-                       0.44867E-01    0.64088E-02
  
 MUR-   -->  Z1SS   MU-                      0.12625E-02    0.18034E-03
 MUR-   -->  Z2SS   MU-                      0.69516E+01    0.99296E+00
 MUR-   -->  Z3SS   MU-                      0.31372E-02    0.44812E-03
 MUR-   -->  Z4SS   MU-                      0.44867E-01    0.64088E-02
  
 TAU2-  -->  Z1SS   TAU-                     0.35979E+01    0.23869E+00
 TAU2-  -->  Z2SS   TAU-                     0.38435E+01    0.25499E+00
 TAU2-  -->  Z3SS   TAU-                     0.51606E-01    0.34236E-02
 TAU2-  -->  Z4SS   TAU-                     0.10075E+00    0.66841E-02
 TAU2-  -->  W1SS-  NUT                      0.72188E+01    0.47891E+00
 TAU2-  -->  W2SS-  NUT                      0.26080E+00    0.17302E-01
  
 Z2SS   -->  Z1SS   GM                       0.75828E-10    0.33340E-08
 Z2SS   -->  W1SS+  W-                       0.76808E-02    0.33771E+00
 Z2SS   -->  W1SS-  W+                       0.76808E-02    0.33771E+00
 Z2SS   -->  Z1SS   Z0                       0.46637E-03    0.20505E-01
 Z2SS   -->  Z1SS   UP     UB                0.12846E-07    0.56480E-06
 Z2SS   -->  Z1SS   DN     DB                0.73496E-08    0.32315E-06
 Z2SS   -->  Z1SS   ST     SB                0.73496E-08    0.32315E-06
 Z2SS   -->  Z1SS   CH     CB                0.12846E-07    0.56480E-06
 Z2SS   -->  Z1SS   BT     BB                0.16196E-07    0.71209E-06
 Z2SS   -->  Z1SS   E-     E+                0.45116E-07    0.19837E-05
 Z2SS   -->  Z1SS   MU-    MU+               0.45116E-07    0.19837E-05
 Z2SS   -->  Z1SS   TAU-   TAU+              0.45427E-07    0.19973E-05
 Z2SS   -->  Z1SS   NUE    ANUE              0.38689E-07    0.17011E-05
 Z2SS   -->  Z1SS   NUM    ANUM              0.38689E-07    0.17011E-05
 Z2SS   -->  Z1SS   NUT    ANUT              0.38866E-07    0.17088E-05
 Z2SS   -->  Z1SS   HL0                      0.69156E-02    0.30406E+00
  
 Z3SS   -->  Z1SS   GM                       0.18498E-05    0.38920E-06
 Z3SS   -->  Z2SS   GM                       0.24802E-08    0.52184E-09
 Z3SS   -->  W1SS+  W-                       0.14253E+01    0.29989E+00
 Z3SS   -->  W1SS-  W+                       0.14253E+01    0.29989E+00
 Z3SS   -->  Z1SS   Z0                       0.11863E+01    0.24961E+00
 Z3SS   -->  Z2SS   Z0                       0.39343E+00    0.82782E-01
 Z3SS   -->  Z1SS   UP     UB                0.53246E-07    0.11203E-07
 Z3SS   -->  Z1SS   DN     DB                0.74076E-07    0.15586E-07
 Z3SS   -->  Z1SS   ST     SB                0.74076E-07    0.15586E-07
 Z3SS   -->  Z1SS   CH     CB                0.53246E-07    0.11203E-07
 Z3SS   -->  Z1SS   BT     BB                0.13166E-05    0.27703E-06
 Z3SS   -->  Z1SS   E-     E+                0.17136E-07    0.36056E-08
 Z3SS   -->  Z1SS   MU-    MU+               0.17136E-07    0.36056E-08
 Z3SS   -->  Z1SS   TAU-   TAU+              0.16775E-06    0.35296E-07
 Z3SS   -->  Z1SS   NUE    ANUE              0.47917E-07    0.10082E-07
 Z3SS   -->  Z1SS   NUM    ANUM              0.47917E-07    0.10082E-07
 Z3SS   -->  Z1SS   NUT    ANUT              0.48154E-07    0.10132E-07
 Z3SS   -->  Z2SS   UP     UB                0.12585E-08    0.26480E-09
 Z3SS   -->  Z2SS   DN     DB                0.44107E-09    0.92805E-10
 Z3SS   -->  Z2SS   ST     SB                0.44107E-09    0.92805E-10
 Z3SS   -->  Z2SS   CH     CB                0.12585E-08    0.26480E-09
 Z3SS   -->  Z2SS   BT     BB                0.36700E-07    0.77219E-08
 Z3SS   -->  Z2SS   E-     E+                0.31411E-08    0.66090E-09
 Z3SS   -->  Z2SS   MU-    MU+               0.31411E-08    0.66090E-09
 Z3SS   -->  Z2SS   TAU-   TAU+              0.54105E-07    0.11384E-07
 Z3SS   -->  Z2SS   NUE    ANUE              0.28204E-08    0.59344E-09
 Z3SS   -->  Z2SS   NUM    ANUM              0.28204E-08    0.59344E-09
 Z3SS   -->  Z2SS   NUT    ANUT              0.28348E-08    0.59646E-09
 Z3SS   -->  Z1SS   HL0                      0.29648E+00    0.62383E-01
 Z3SS   -->  Z2SS   HL0                      0.25920E-01    0.54538E-02
  
 Z4SS   -->  Z1SS   GM                       0.77240E-07    0.16002E-07
 Z4SS   -->  Z2SS   GM                       0.33075E-06    0.68520E-07
 Z4SS   -->  Z3SS   GM                       0.37246E-09    0.77162E-10
 Z4SS   -->  W1SS+  W-                       0.14981E+01    0.31035E+00
 Z4SS   -->  W1SS-  W+                       0.14981E+01    0.31035E+00
 Z4SS   -->  Z1SS   Z0                       0.29393E+00    0.60893E-01
 Z4SS   -->  Z2SS   Z0                       0.29921E-01    0.61986E-02
 Z4SS   -->  Z1SS   UP     UB                0.30632E-06    0.63460E-07
 Z4SS   -->  Z1SS   DN     DB                0.56970E-06    0.11802E-06
 Z4SS   -->  Z1SS   ST     SB                0.56970E-06    0.11802E-06
 Z4SS   -->  Z1SS   CH     CB                0.30632E-06    0.63460E-07
 Z4SS   -->  Z1SS   BT     BB                0.24711E-05    0.51194E-06
 Z4SS   -->  Z1SS   E-     E+                0.59311E-07    0.12287E-07
 Z4SS   -->  Z1SS   MU-    MU+               0.59311E-07    0.12287E-07
 Z4SS   -->  Z1SS   TAU-   TAU+              0.21021E-06    0.43548E-07
 Z4SS   -->  Z1SS   NUE    ANUE              0.44432E-06    0.92048E-07
 Z4SS   -->  Z1SS   NUM    ANUM              0.44432E-06    0.92048E-07
 Z4SS   -->  Z1SS   NUT    ANUT              0.44649E-06    0.92498E-07
 Z4SS   -->  Z2SS   UP     UB                0.34858E-07    0.72213E-08
 Z4SS   -->  Z2SS   DN     DB                0.68945E-08    0.14283E-08
 Z4SS   -->  Z2SS   ST     SB                0.68945E-08    0.14283E-08
 Z4SS   -->  Z2SS   CH     CB                0.34858E-07    0.72213E-08
 Z4SS   -->  Z2SS   BT     BB                0.48514E-07    0.10051E-07
 Z4SS   -->  Z2SS   E-     E+                0.82062E-07    0.17001E-07
 Z4SS   -->  Z2SS   MU-    MU+               0.82062E-07    0.17001E-07
 Z4SS   -->  Z2SS   TAU-   TAU+              0.13696E-06    0.28373E-07
 Z4SS   -->  Z2SS   NUE    ANUE              0.43406E-07    0.89923E-08
 Z4SS   -->  Z2SS   NUM    ANUM              0.43406E-07    0.89923E-08
 Z4SS   -->  Z2SS   NUT    ANUT              0.43625E-07    0.90375E-08
 Z4SS   -->  Z3SS   UP     UB                0.43057E-08    0.89200E-09
 Z4SS   -->  Z3SS   DN     DB                0.55528E-08    0.11504E-08
 Z4SS   -->  Z3SS   ST     SB                0.55528E-08    0.11504E-08
 Z4SS   -->  Z3SS   CH     CB                0.43057E-08    0.89200E-09
 Z4SS   -->  Z3SS   E-     E+                0.12596E-08    0.26095E-09
 Z4SS   -->  Z3SS   MU-    MU+               0.12596E-08    0.26095E-09
 Z4SS   -->  Z3SS   TAU-   TAU+              0.71833E-09    0.14881E-09
 Z4SS   -->  Z3SS   NUE    ANUE              0.25060E-08    0.51915E-09
 Z4SS   -->  Z3SS   NUM    ANUM              0.25060E-08    0.51915E-09
 Z4SS   -->  Z3SS   NUT    ANUT              0.25060E-08    0.51915E-09
 Z4SS   -->  Z1SS   HL0                      0.11191E+01    0.23183E+00
 Z4SS   -->  Z2SS   HL0                      0.38796E+00    0.80373E-01
  
 W1SS+  -->  Z1SS   PI+                      0.82090E-14    0.95814E+00
 W1SS+  -->  Z1SS   E+     NUE               0.17931E-15    0.20929E-01
 W1SS+  -->  Z1SS   MU+    NUM               0.17931E-15    0.20929E-01
  
 W2SS+  -->  Z1SS   UP     DB                0.74161E-06    0.15828E-06
 W2SS+  -->  Z1SS   CH     SB                0.74161E-06    0.15828E-06
 W2SS+  -->  Z1SS   E+     NUE               0.35110E-06    0.74935E-07
 W2SS+  -->  Z1SS   MU+    NUM               0.35110E-06    0.74935E-07
 W2SS+  -->  Z1SS   TAU+   NUT               0.51401E-06    0.10970E-06
 W2SS+  -->  Z1SS   W+                       0.13566E+01    0.28953E+00
 W2SS+  -->  Z2SS   UP     DB                0.91557E-08    0.19541E-08
 W2SS+  -->  Z2SS   CH     SB                0.91557E-08    0.19541E-08
 W2SS+  -->  Z2SS   E+     NUE               0.50586E-07    0.10796E-07
 W2SS+  -->  Z2SS   MU+    NUM               0.50586E-07    0.10796E-07
 W2SS+  -->  Z2SS   TAU+   NUT               0.10335E-06    0.22058E-07
 W2SS+  -->  Z2SS   W+                       0.50182E+00    0.10710E+00
 W2SS+  -->  Z3SS   UP     DB                0.11709E-07    0.24991E-08
 W2SS+  -->  Z3SS   CH     SB                0.11709E-07    0.24991E-08
 W2SS+  -->  Z3SS   E+     NUE               0.39031E-08    0.83302E-09
 W2SS+  -->  Z3SS   MU+    NUM               0.39031E-08    0.83302E-09
 W2SS+  -->  Z3SS   TAU+   NUT               0.39032E-08    0.83304E-09
 W2SS+  -->  W1SS+  Z0                       0.14245E+01    0.30403E+00
 W2SS+  -->  W1SS+  DN     DB                0.18354E-06    0.39172E-07
 W2SS+  -->  W1SS+  ST     SB                0.18354E-06    0.39172E-07
 W2SS+  -->  W1SS+  UP     UB                0.15001E-05    0.32016E-06
 W2SS+  -->  W1SS+  CH     CB                0.15001E-05    0.32016E-06
 W2SS+  -->  W1SS+  NUE    ANUE              0.72162E-06    0.15401E-06
 W2SS+  -->  W1SS+  NUM    ANUM              0.72162E-06    0.15401E-06
 W2SS+  -->  W1SS+  NUT    ANUT              0.73060E-06    0.15593E-06
 W2SS+  -->  W1SS+  E-     E+                0.88302E-07    0.18846E-07
 W2SS+  -->  W1SS+  MU-    MU+               0.88302E-07    0.18846E-07
 W2SS+  -->  W1SS+  TAU-   TAU+              0.88736E-07    0.18939E-07
 W2SS+  -->  W1SS+  HL0                      0.14025E+01    0.29933E+00
  
 HL0    -->  E-     E+                       0.18698E-10    0.50009E-08
 HL0    -->  MU-    MU+                      0.78948E-06    0.21115E-03
 HL0    -->  TAU-   TAU+                     0.22575E-03    0.60379E-01
 HL0    -->  DN     DB                       0.23489E-07    0.62823E-05
 HL0    -->  ST     SB                       0.94909E-05    0.25384E-02
 HL0    -->  BT     BB                       0.31372E-02    0.83906E+00
 HL0    -->  UP     UB                       0.74035E-08    0.19801E-05
 HL0    -->  CH     CB                       0.14064E-03    0.37614E-01
 HL0    -->  GM     GM                       0.62093E-05    0.16607E-02
 HL0    -->  GL     GL                       0.13457E-03    0.35990E-01
 HL0    -->  W+     E-     ANUE              0.44848E-05    0.11995E-02
 HL0    -->  W+     MU-    ANUM              0.44848E-05    0.11995E-02
 HL0    -->  W+     TAU-   ANUT              0.44848E-05    0.11995E-02
 HL0    -->  W+     UB     DN                0.13455E-04    0.35984E-02
 HL0    -->  W+     CB     ST                0.13455E-04    0.35984E-02
 HL0    -->  W-     E+     NUE               0.44848E-05    0.11995E-02
 HL0    -->  W-     MU+    NUM               0.44848E-05    0.11995E-02
 HL0    -->  W-     TAU+   NUT               0.44848E-05    0.11995E-02
 HL0    -->  W-     UP     DB                0.13455E-04    0.35984E-02
 HL0    -->  W-     CH     SB                0.13455E-04    0.35984E-02
 HL0    -->  Z0     NUE    ANUE              0.24337E-06    0.65090E-04
 HL0    -->  Z0     NUM    ANUM              0.24337E-06    0.65090E-04
 HL0    -->  Z0     NUT    ANUT              0.24337E-06    0.65090E-04
 HL0    -->  Z0     E-     E+                0.12249E-06    0.32759E-04
 HL0    -->  Z0     MU-    MU+               0.12249E-06    0.32759E-04
 HL0    -->  Z0     TAU-   TAU+              0.12249E-06    0.32759E-04
 HL0    -->  Z0     UP     UB                0.41963E-06    0.11223E-03
 HL0    -->  Z0     CH     CB                0.41963E-06    0.11223E-03
 HL0    -->  Z0     DN     DB                0.54059E-06    0.14458E-03
 HL0    -->  Z0     ST     SB                0.54059E-06    0.14458E-03
 HL0    -->  Z0     BT     BB                0.54059E-06    0.14458E-03
  
 HH0    -->  E-     E+                       0.70598E-08    0.35117E-09
 HH0    -->  MU-    MU+                      0.29808E-03    0.14827E-04
 HH0    -->  TAU-   TAU+                     0.85373E-01    0.42467E-02
 HH0    -->  DN     DB                       0.86141E-05    0.42848E-06
 HH0    -->  ST     SB                       0.34805E-02    0.17313E-03
 HH0    -->  BT     BB                       0.45714E+00    0.22739E-01
 HH0    -->  UP     UB                       0.44768E-08    0.22269E-09
 HH0    -->  CH     CB                       0.59511E-04    0.29602E-05
 HH0    -->  TP     TB                       0.29880E+01    0.14863E+00
 HH0    -->  GM     GM                       0.14915E-04    0.74191E-06
 HH0    -->  GL     GL                       0.12794E-02    0.63643E-04
 HH0    -->  W+     W-                       0.31113E-02    0.15476E-03
 HH0    -->  Z0     Z0                       0.15906E-02    0.79120E-04
 HH0    -->  Z1SS   Z1SS                     0.17360E+00    0.86351E-02
 HH0    -->  Z1SS   Z2SS                     0.10913E+00    0.54283E-02
 HH0    -->  Z1SS   Z3SS                     0.34885E+01    0.17353E+00
 HH0    -->  Z1SS   Z4SS                     0.12689E+01    0.63117E-01
 HH0    -->  Z2SS   Z2SS                     0.16186E-01    0.80515E-03
 HH0    -->  Z2SS   Z3SS                     0.97315E+00    0.48407E-01
 HH0    -->  Z2SS   Z4SS                     0.27346E+00    0.13602E-01
 HH0    -->  Z3SS   Z3SS                     0.11010E-01    0.54767E-03
 HH0    -->  Z3SS   Z4SS                     0.13361E+00    0.66462E-02
 HH0    -->  Z4SS   Z4SS                     0.24899E-01    0.12385E-02
 HH0    -->  W1SS+  W1SS-                    0.32815E+00    0.16323E-01
 HH0    -->  W2SS+  W2SS-                    0.15870E-02    0.78939E-04
 HH0    -->  W1SS+  W2SS-                    0.48739E+01    0.24244E+00
 HH0    -->  W1SS-  W2SS+                    0.48739E+01    0.24244E+00
 HH0    -->  HL0    HL0                      0.13174E-01    0.65528E-03
  
 HA0    -->  E-     E+                       0.70159E-08    0.35197E-09
 HA0    -->  MU-    MU+                      0.29622E-03    0.14861E-04
 HA0    -->  TAU-   TAU+                     0.84843E-01    0.42564E-02
 HA0    -->  DN     DB                       0.85609E-05    0.42948E-06
 HA0    -->  ST     SB                       0.34590E-02    0.17353E-03
 HA0    -->  BT     BB                       0.45432E+00    0.22792E-01
 HA0    -->  UP     UB                       0.43827E-08    0.21987E-09
 HA0    -->  CH     CB                       0.58305E-04    0.29250E-05
 HA0    -->  TP     TB                       0.30313E+01    0.15207E+00
 HA0    -->  GM     GM                       0.10498E-04    0.52667E-06
 HA0    -->  GL     GL                       0.14418E-02    0.72333E-04
 HA0    -->  Z1SS   Z1SS                     0.22993E+00    0.11535E-01
 HA0    -->  Z1SS   Z2SS                     0.16637E+00    0.83463E-02
 HA0    -->  Z1SS   Z3SS                     0.13513E+01    0.67792E-01
 HA0    -->  Z1SS   Z4SS                     0.32289E+01    0.16199E+00
 HA0    -->  Z2SS   Z2SS                     0.29538E-01    0.14819E-02
 HA0    -->  Z2SS   Z3SS                     0.29223E+00    0.14661E-01
 HA0    -->  Z2SS   Z4SS                     0.88814E+00    0.44556E-01
 HA0    -->  Z3SS   Z3SS                     0.12760E-01    0.64015E-03
 HA0    -->  Z3SS   Z4SS                     0.33572E-01    0.16843E-02
 HA0    -->  Z4SS   Z4SS                     0.15564E+00    0.78080E-02
 HA0    -->  W1SS+  W1SS-                    0.43379E+00    0.21762E-01
 HA0    -->  W2SS+  W2SS-                    0.65997E-01    0.33109E-02
 HA0    -->  W1SS+  W2SS-                    0.47331E+01    0.23745E+00
 HA0    -->  W1SS-  W2SS+                    0.47331E+01    0.23745E+00
 HA0    -->  HL0    Z0                       0.30632E-02    0.15368E-03
  
 H+     -->  NUE    E+                       0.70701E-08    0.35688E-09
 H+     -->  NUM    MU+                      0.29851E-03    0.15068E-04
 H+     -->  NUT    TAU+                     0.85498E-01    0.43158E-02
 H+     -->  UP     DB                       0.79652E-05    0.40207E-06
 H+     -->  CH     SB                       0.32709E-02    0.16511E-03
 H+     -->  TP     BB                       0.32023E+01    0.16165E+00
 H+     -->  W1SS+  Z1SS                     0.17059E-03    0.86110E-05
 H+     -->  W1SS+  Z2SS                     0.28948E+00    0.14612E-01
 H+     -->  W1SS+  Z3SS                     0.49184E+01    0.24827E+00
 H+     -->  W1SS+  Z4SS                     0.48032E+01    0.24246E+00
 H+     -->  W2SS+  Z1SS                     0.49879E+01    0.25178E+00
 H+     -->  W2SS+  Z2SS                     0.14348E+01    0.72426E-01
 H+     -->  W2SS+  Z3SS                     0.52089E-01    0.26293E-02
 H+     -->  W2SS+  Z4SS                     0.30122E-01    0.15205E-02
 H+     -->  HL0    W+                       0.31013E-02    0.15655E-03
  
