###############################################################
#
# Job options file
#
#==============================================================
#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Main generator : Herwig
include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_14TeV.py" ) 
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TTbarLeptonFilter
topAlg += TTbarLeptonFilter()

TTbarLeptonFilter = topAlg.TTbarLeptonFilter
TTbarLeptonFilter.Ptcut = 1.

try:
     StreamEVGEN.RequireAlgs = [ "TTbarLeptonFilter" ]
except Exception, e:
     pass

from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo31.205200.ttbar'
# 14.2.25.6
# 5000/8917=0.561
evgenConfig.efficiency = 0.50
#==============================================================
#
# End of job options file
#
###############################################################
