#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.34978364E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.49783641E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     4.80000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.95000000E+02   # M_q1L               
        42     4.95000000E+02   # M_q2L               
        43     2.57000000E+02   # M_q3L               
        44     4.95000000E+02   # M_uR                
        45     4.95000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.95000000E+02   # M_dR                
        48     4.95000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04943559E+01   # W+
        25     9.74811301E+01   # h
        35     1.00069599E+03   # H
        36     1.00000000E+03   # A
        37     1.00418190E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00096659E+02   # ~d_L
   2000001     5.00562576E+02   # ~d_R
   1000002     5.00424616E+02   # ~u_L
   2000002     5.00801757E+02   # ~u_R
   1000003     5.00096659E+02   # ~s_L
   2000003     5.00562576E+02   # ~s_R
   1000004     5.00424616E+02   # ~c_L
   2000004     5.00801757E+02   # ~c_R
   1000005     2.80167332E+02   # ~b_1
   2000005     1.00706363E+03   # ~b_2
   1000006     2.99805459E+02   # ~t_1
   2000006     6.40686527E+02   # ~t_2
   1000011     5.00020025E+03   # ~e_L
   2000011     5.00017997E+03   # ~e_R
   1000012     4.99961976E+03   # ~nu_eL
   1000013     5.00020025E+03   # ~mu_L
   2000013     5.00017997E+03   # ~mu_R
   1000014     4.99961976E+03   # ~nu_muL
   1000015     4.99932789E+03   # ~tau_1
   2000015     5.00105280E+03   # ~tau_2
   1000016     4.99961976E+03   # ~nu_tauL
   1000021     5.0006648E+02   # ~g
   1000022     5.99325738E+01   # ~chi_10
   1000023     1.19666945E+02   # ~chi_20
   1000025    -9.97290679E+02   # ~chi_30
   1000035     1.00147280E+03   # ~chi_40
   1000024     1.19612942E+02   # ~chi_1+
   1000037     1.00173519E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98497378E-01   # N_11
  1  2    -2.66772129E-02   # N_12
  1  3     4.63098459E-02   # N_13
  1  4    -1.21124451E-02   # N_14
  2  1     3.06103876E-02   # N_21
  2  2     9.96035378E-01   # N_22
  2  3    -7.96206303E-02   # N_23
  2  4     2.52405474E-02   # N_24
  3  1     2.30750895E-02   # N_31
  3  2    -3.91459912E-02   # N_32
  3  3    -7.05151849E-01   # N_33
  3  4    -7.07598757E-01   # N_34
  4  1     3.91603276E-02   # N_41
  4  2    -7.52957072E-02   # N_42
  4  3    -7.03048237E-01   # N_43
  4  4     7.06059631E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93448927E-01   # U_11
  1  2     1.14276982E-01   # U_12
  2  1     1.14276982E-01   # U_21
  2  2     9.93448927E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99339070E-01   # V_11
  1  2     3.63513898E-02   # V_12
  2  1     3.63513898E-02   # V_21
  2  2     9.99339070E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95309308E-01   # cos(theta_t)
  1  2     9.67438960E-02   # sin(theta_t)
  2  1    -9.67438960E-02   # -sin(theta_t)
  2  2     9.95309308E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99915279E-01   # cos(theta_b)
  1  2     1.30167132E-02   # sin(theta_b)
  2  1    -1.30167132E-02   # -sin(theta_b)
  2  2     9.99915279E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.02938006E-01   # cos(theta_tau)
  1  2     7.11251123E-01   # sin(theta_tau)
  2  1    -7.11251123E-01   # -sin(theta_tau)
  2  2     7.02938006E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.04156293E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.49783641E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.88898079E+00   # tanbeta(Q)          
         3     2.46947593E+02   # vev(Q)              
         4     1.00621654E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.49783641E+02  # The gauge couplings
     1     3.58285657E-01   # gprime(Q) DRbar
     2     6.43455583E-01   # g(Q) DRbar
     3     1.10862904E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.49783641E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  3.49783641E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  3.49783641E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.49783641E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.04609768E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.49783641E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.08849042E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.49783641E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.04143828E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.49783641E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     4.80000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.39389152E+04   # M^2_Hd              
        22    -9.53302755E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.95000000E+02   # M_q1L               
        42     4.95000000E+02   # M_q2L               
        43     2.57000000E+02   # M_q3L               
        44     4.95000000E+02   # M_uR                
        45     4.95000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.95000000E+02   # M_dR                
        48     4.95000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43297784E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.63957498E+00   # gluino decays
#          BR         NDA      ID1       ID2
     3.45912007E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.45912007E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.54087993E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.54087993E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.92126750E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09545689E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     7.03416836E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.18703747E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.24357502E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     5.07800294E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.19507535E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.98234459E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.61638426E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.16170375E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.05213485E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.07492553E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.89832816E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.41016718E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.10379108E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.31944943E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.24844679E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.95193266E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.07006831E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.89993768E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.82115015E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.02165576E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.41662606E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.23275518E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.40839723E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     5.45706326E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     8.95474690E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31375136E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59670001E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.16565933E-07    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.07119775E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.97638577E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.70704527E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.49071830E-03    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     5.49215440E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.60170994E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.25049427E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.54228721E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.70475283E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.76124540E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.69996577E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.47077098E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.91563456E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     5.45706326E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.95474690E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31375136E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59670001E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.16565933E-07    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.07119775E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.97638577E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.70704527E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.49071830E-03    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     5.49215440E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.60170994E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.25049427E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.54228721E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.70475283E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.76124540E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.69996577E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.47077098E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.91563456E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80440863E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.47646846E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10258647E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.92975572E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.97863860E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96702977E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.28285204E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55275605E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97158897E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.36343767E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91159978E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.41359932E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80440863E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.47646846E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10258647E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.92975572E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.97863860E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96702977E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.28285204E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55275605E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97158897E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.36343767E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91159978E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.41359932E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.69259739E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.36414830E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.26335718E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09983149E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.55971302E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.35289614E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.04034312E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73452844E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.26056369E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.22139859E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.34874040E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.42209572E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26207032E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.48259037E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80610234E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02677331E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89624632E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.54091717E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.62815708E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03579142E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.36646377E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80610234E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02677331E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89624632E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.54091717E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.62815708E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03579142E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.36646377E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82941977E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02326764E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88635777E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.51517049E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.61918386E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01566313E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.10044498E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.16010163E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39136738E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.39136738E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.07403456E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.07403456E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.06919613E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.93631158E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.34503598E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.28475262E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.34503598E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.28475262E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     6.34063299E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.47003203E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.58230737E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.61663405E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48650646E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.71898058E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.51657032E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.48486571E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.11661062E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     6.79039359E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.02509184E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     6.79039359E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.02509184E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.21659727E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.80112773E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.80112773E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.99385439E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.60043797E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.60043797E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.60043797E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.18209155E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.18209155E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.18209155E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.18209155E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.68297580E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.68297580E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.68297580E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.68297580E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.83786030E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.40518881E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.36873128E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.66424226E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.66424226E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.77027148E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.16674671E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.11242612E-04    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.11242612E-04    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.67757514E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.67757514E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.70356677E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.70356677E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     6.66660380E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     6.66660380E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.11242612E-04    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.11242612E-04    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.67757514E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.67757514E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.70356677E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.70356677E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     6.66660380E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     6.66660380E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.72261779E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.72261779E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.31345389E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.31345389E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     2.04215060E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.04215060E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.89368410E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.78460577E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.17381681E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.61240727E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.61240727E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.36283495E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.24381467E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.21302321E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.21302321E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     7.67098962E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     7.67098962E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     6.12543922E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     6.12543922E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.91000365E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.91000365E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.21302321E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.21302321E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     7.67098962E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     7.67098962E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     6.12543922E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     6.12543922E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.91000365E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.91000365E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     2.96307064E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.96307064E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.06323506E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.06323506E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.72739493E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.72739493E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.69247371E-03   # h decays
#          BR         NDA      ID1       ID2
     8.28208008E-01    2           5        -5   # BR(h -> b       bb     )
     7.66593421E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.71526886E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.04526392E-04    2           3        -3   # BR(h -> s       sb     )
     2.57085144E-02    2           4        -4   # BR(h -> c       cb     )
     6.03913751E-02    2          21        21   # BR(h -> g       g      )
     1.32069721E-03    2          22        22   # BR(h -> gam     gam    )
     1.68306665E-05    2          22        23   # BR(h -> Z       gam    )
     6.07411629E-03    2          24       -24   # BR(h -> W+      W-     )
     7.45063034E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.20142929E+01   # H decays
#          BR         NDA      ID1       ID2
     1.37275995E-02    2           5        -5   # BR(H -> b       bb     )
     2.24977947E-03    2         -15        15   # BR(H -> tau+    tau-   )
     7.95302548E-06    2         -13        13   # BR(H -> mu+     mu-    )
     1.13820628E-05    2           3        -3   # BR(H -> s       sb     )
     9.13783592E-07    2           4        -4   # BR(H -> c       cb     )
     8.17686170E-02    2           6        -6   # BR(H -> t       tb     )
     2.39128990E-04    2          21        21   # BR(H -> g       g      )
     1.27948467E-06    2          22        22   # BR(H -> gam     gam    )
     1.27216479E-07    2          23        22   # BR(H -> Z       gam    )
     8.26036493E-05    2          24       -24   # BR(H -> W+      W-     )
     4.08502330E-05    2          23        23   # BR(H -> Z       Z      )
     4.03347563E-04    2          25        25   # BR(H -> h       h      )
     6.92471182E-25    2          36        36   # BR(H -> A       A      )
     8.00385716E-15    2          23        36   # BR(H -> Z       A      )
     3.86581965E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.16837477E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.80296039E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.25602067E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.70692250E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.13516331E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.13516331E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     2.22893599E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.13136023E+01   # A decays
#          BR         NDA      ID1       ID2
     1.42027851E-02    2           5        -5   # BR(A -> b       bb     )
     2.32444739E-03    2         -15        15   # BR(A -> tau+    tau-   )
     8.21687517E-06    2         -13        13   # BR(A -> mu+     mu-    )
     1.17933526E-05    2           3        -3   # BR(A -> s       sb     )
     9.29874466E-07    2           4        -4   # BR(A -> c       cb     )
     9.06630849E-02    2           6        -6   # BR(A -> t       tb     )
     1.75998830E-04    2          21        21   # BR(A -> g       g      )
     2.59932072E-07    2          22        22   # BR(A -> gam     gam    )
     1.94617823E-07    2          23        22   # BR(A -> Z       gam    )
     8.38227373E-05    2          23        25   # BR(A -> Z       h      )
     5.49804623E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.81971669E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.56601492E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.69551061E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.41243462E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.41243462E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     2.46239883E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.81062739E-05    2           4        -5   # BR(H+ -> c       bb     )
     2.02036852E-03    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.14196207E-06    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.15871168E-07    2           2        -5   # BR(H+ -> u       bb     )
     4.87582799E-07    2           2        -3   # BR(H+ -> u       sb     )
     1.08005716E-05    2           4        -3   # BR(H+ -> c       sb     )
     8.74375266E-02    2           6        -5   # BR(H+ -> t       bb     )
     7.39191103E-05    2          24        25   # BR(H+ -> W+      h      )
     6.80491949E-11    2          24        36   # BR(H+ -> W+      A      )
     2.43220535E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.31205645E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.71599063E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.80837110E-01    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
