#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12359619E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.23596192E+03   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     3.38000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.72000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07401520E+01   # W+
        25     1.16909477E+02   # h
        35     1.00085799E+03   # H
        36     1.00000000E+03   # A
        37     1.00373540E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04658754E+03   # ~d_L
   2000001     5.04633557E+03   # ~d_R
   1000002     5.04602395E+03   # ~u_L
   2000002     5.04615669E+03   # ~u_R
   1000003     5.04658754E+03   # ~s_L
   2000003     5.04633557E+03   # ~s_R
   1000004     5.04602395E+03   # ~c_L
   2000004     5.04615669E+03   # ~c_R
   1000005     3.10484296E+02   # ~b_1
   2000005     5.04633678E+03   # ~b_2
   1000006     6.01932718E+02   # ~t_1
   2000006     5.06834517E+03   # ~t_2
   1000011     5.00019057E+03   # ~e_L
   2000011     5.00017721E+03   # ~e_R
   1000012     4.99963220E+03   # ~nu_eL
   1000013     5.00019057E+03   # ~mu_L
   2000013     5.00017721E+03   # ~mu_R
   1000014     4.99963220E+03   # ~nu_muL
   1000015     4.99934567E+03   # ~tau_1
   2000015     5.00102258E+03   # ~tau_2
   1000016     4.99963220E+03   # ~nu_tauL
   1000021     5.00369814E+02   # ~g
   1000022     5.91062116E+01   # ~chi_10
   1000023     6.87521966E+02   # ~chi_20
   1000025    -1.00162429E+03   # ~chi_30
   1000035     1.01499611E+03   # ~chi_40
   1000024     6.87482554E+02   # ~chi_1+
   1000037     1.01466727E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98966765E-01   # N_11
  1  2    -2.47289158E-03   # N_12
  1  3     4.38725141E-02   # N_13
  1  4    -1.15969902E-02   # N_14
  2  1     1.09645269E-02   # N_21
  2  2     9.78764138E-01   # N_22
  2  3    -1.61115775E-01   # N_23
  2  4     1.26262620E-01   # N_24
  3  1     2.27217885E-02   # N_31
  3  2    -2.51625747E-02   # N_32
  3  3    -7.05824152E-01   # N_33
  3  4    -7.07575318E-01   # N_34
  4  1     3.78008243E-02   # N_41
  4  2    -2.03424415E-01   # N_42
  4  3    -6.88425142E-01   # N_43
  4  4     6.95169353E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73576822E-01   # U_11
  1  2     2.28359741E-01   # U_12
  2  1     2.28359741E-01   # U_21
  2  2     9.73576822E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83820793E-01   # V_11
  1  2     1.79155371E-01   # V_12
  2  1     1.79155371E-01   # V_21
  2  2     9.83820793E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998895E-01   # cos(theta_t)
  1  2     1.48660646E-03   # sin(theta_t)
  2  1    -1.48660646E-03   # -sin(theta_t)
  2  2     9.99998895E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999884E-01   # cos(theta_b)
  1  2     4.81663769E-04   # sin(theta_b)
  2  1    -4.81663769E-04   # -sin(theta_b)
  2  2     9.99999884E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04285435E-01   # cos(theta_tau)
  1  2     7.09916915E-01   # sin(theta_tau)
  2  1    -7.09916915E-01   # -sin(theta_tau)
  2  2     7.04285435E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10153853E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.23596192E+03  # DRbar Higgs Parameters
         1     9.92028743E+02   # mu(Q)               
         2     4.79650923E+00   # tanbeta(Q)          
         3     2.43592526E+02   # vev(Q)              
         4     1.00533264E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.23596192E+03  # The gauge couplings
     1     3.60997539E-01   # gprime(Q) DRbar
     2     6.40781014E-01   # g(Q) DRbar
     3     1.05802568E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.23596192E+03  # The trilinear couplings
  1  1     1.08105058E+03   # A_u(Q) DRbar
  2  2     1.08105058E+03   # A_c(Q) DRbar
  3  3     1.38170787E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.23596192E+03  # The trilinear couplings
  1  1     7.69977353E+02   # A_d(Q) DRbar
  2  2     7.69977353E+02   # A_s(Q) DRbar
  3  3     8.70789508E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  1.23596192E+03  # The trilinear couplings
  1  1     4.37504374E+02   # A_e(Q) DRbar
  2  2     4.37504374E+02   # A_mu(Q) DRbar
  3  3     4.37863430E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.23596192E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75948435E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.23596192E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.10890597E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.23596192E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97246474E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.23596192E+03  # The soft SUSY breaking masses at the scale Q
         1     1.47877404E+02   # M_1                 
         2     8.80348069E+02   # M_2                 
         3     1.56359161E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.46634108E+06   # M^2_Hd              
        22     2.33016928E+07   # M^2_Hu              
        31     5.14497587E+03   # M_eL                
        32     5.14497587E+03   # M_muL               
        33     5.14837978E+03   # M_tauL              
        34     4.68548844E+03   # M_eR                
        35     4.68548844E+03   # M_muR               
        36     4.69299937E+03   # M_tauR              
        41     5.02405837E+03   # M_q1L               
        42     5.02405837E+03   # M_q2L               
        43     2.98166906E+03   # M_q3L               
        44     5.29433652E+03   # M_uR                
        45     5.29433652E+03   # M_cR                
        46     6.73485560E+03   # M_tR                
        47     4.98446173E+03   # M_dR                
        48     4.98446173E+03   # M_sR                
        49     4.98921579E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41025730E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.08429527E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     4.21239911E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.64131052E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98358689E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.67302520E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.41598794E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.27061301E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.89369320E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.64680557E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.51484630E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.31737206E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.81361216E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     3.33156831E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.06394050E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     4.03111743E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     8.43437711E-03    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.03094095E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.87153693E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.27215560E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     3.21350473E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.75679114E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.50581000E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.51834163E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.27313840E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.90542354E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.24344251E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.88401588E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.70142130E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.82244100E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     2.80215929E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     5.30116590E-05    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     3.33607911E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.52850812E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.76325517E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.81997222E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.95048388E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.16199053E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.90470473E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.96161771E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.17237253E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.43960315E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.28887675E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.42166985E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.99430253E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65052947E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.55901949E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.65717212E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.76335011E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.91868255E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.90946233E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.28897140E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.19036880E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.75616482E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.15476139E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.44037026E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.20443091E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.77991290E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.02492970E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.23523647E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.16983846E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91203129E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.76325517E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.81997222E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.95048388E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.16199053E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.90470473E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.96161771E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.17237253E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.43960315E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.28887675E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.42166985E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.99430253E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65052947E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.55901949E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.65717212E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.76335011E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.91868255E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.90946233E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.28897140E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.19036880E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.75616482E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.15476139E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.44037026E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.20443091E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.77991290E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.02492970E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.23523647E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.16983846E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91203129E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.53284296E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81180001E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91907748E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.40142142E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.53258549E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70423421E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.99742310E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59157147E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98094197E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.15768861E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.75886321E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31414776E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.53284296E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81180001E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91907748E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.40142142E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.53258549E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70423421E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.99742310E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59157147E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98094197E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.15768861E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.75886321E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31414776E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58480287E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54846445E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13812735E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.42220376E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49686741E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18012269E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.40947948E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60761513E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47427666E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01450855E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.81544544E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64264182E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93086879E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.77927363E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.53438262E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98230915E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84529537E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.14955946E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45070882E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82283267E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84420611E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.53438262E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98230915E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84529537E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.14955946E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45070882E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82283267E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84420611E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55704750E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94780462E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83546041E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13521622E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44569435E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80456591E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16488563E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.41718053E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.90298370E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.28492897E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.47726638E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.45107010E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.02847142E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.36126390E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.83958746E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.47253484E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.86868920E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.17807806E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.02929935E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.40018377E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.05083929E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98204571E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98204571E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.60255110E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.07101388E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.08894940E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.11360904E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.11360904E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04230050E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.52004576E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.08452184E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.08452184E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.41284645E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.41284645E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.58225061E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.04030885E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.00492858E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.20911915E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.20911915E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.02765540E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08443861E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.83633121E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.83633121E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.98907480E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.98907480E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.41546230E-03   # h decays
#          BR         NDA      ID1       ID2
     7.27889889E-01    2           5        -5   # BR(h -> b       bb     )
     7.40186507E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.62014688E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.62298612E-04    2           3        -3   # BR(h -> s       sb     )
     2.34086768E-02    2           4        -4   # BR(h -> c       cb     )
     6.97293416E-02    2          21        21   # BR(h -> g       g      )
     2.07535183E-03    2          22        22   # BR(h -> gam     gam    )
     8.21569796E-04    2          22        23   # BR(h -> Z       gam    )
     9.09963327E-02    2          24       -24   # BR(h -> W+      W-     )
     1.02358742E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.32568640E+00   # H decays
#          BR         NDA      ID1       ID2
     1.33753857E-01    2           5        -5   # BR(H -> b       bb     )
     2.04814776E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.24025240E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.03611461E-04    2           3        -3   # BR(H -> s       sb     )
     9.17325403E-06    2           4        -4   # BR(H -> c       cb     )
     8.20893077E-01    2           6        -6   # BR(H -> t       tb     )
     1.15951686E-03    2          21        21   # BR(H -> g       g      )
     3.81397212E-06    2          22        22   # BR(H -> gam     gam    )
     1.26883543E-06    2          23        22   # BR(H -> Z       gam    )
     2.89529249E-03    2          24       -24   # BR(H -> W+      W-     )
     1.43216267E-03    2          23        23   # BR(H -> Z       Z      )
     1.02700508E-02    2          25        25   # BR(H -> h       h      )
     3.58365952E-22    2          36        36   # BR(H -> A       A      )
     2.16808861E-13    2          23        36   # BR(H -> Z       A      )
     1.64055186E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.98573738E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.29800620E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40933059E+00   # A decays
#          BR         NDA      ID1       ID2
     1.29421294E-01    2           5        -5   # BR(A -> b       bb     )
     1.97922009E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.99650357E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00424502E-04    2           3        -3   # BR(A -> s       sb     )
     8.54615723E-06    2           4        -4   # BR(A -> c       cb     )
     8.33253312E-01    2           6        -6   # BR(A -> t       tb     )
     1.60955761E-03    2          21        21   # BR(A -> g       g      )
     5.34001075E-06    2          22        22   # BR(A -> gam     gam    )
     1.89441288E-06    2          23        22   # BR(A -> Z       gam    )
     2.70966357E-03    2          23        25   # BR(A -> Z       h      )
     2.01009741E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10177044E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.35844173E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.07188248E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.02947916E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.17416803E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.32590670E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.89813726E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.09115619E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.54733829E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.81647370E-03    2          24        25   # BR(H+ -> W+      h      )
     4.04132708E-10    2          24        36   # BR(H+ -> W+      A      )
     1.63233596E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.43727651E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
