#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019043E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190428E+03   # EWSB                
         1     4.07200000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.29280000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889119E+01   # W+
        25     8.64412271E+01   # h
        35     1.00952665E+03   # H
        36     1.00000000E+03   # A
        37     1.00338027E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04407477E+03   # ~d_L
   2000001     5.04409055E+03   # ~d_R
   1000002     5.04411027E+03   # ~u_L
   2000002     5.04410235E+03   # ~u_R
   1000003     5.04407477E+03   # ~s_L
   2000003     5.04409055E+03   # ~s_R
   1000004     5.04411027E+03   # ~c_L
   2000004     5.04410235E+03   # ~c_R
   1000005     5.04385916E+03   # ~b_1
   2000005     5.04430726E+03   # ~b_2
   1000006     4.90889333E+03   # ~t_1
   2000006     5.04280760E+03   # ~t_2
   1000011     4.99998824E+03   # ~e_L
   2000011     4.99998831E+03   # ~e_R
   1000012     5.00002345E+03   # ~nu_eL
   1000013     4.99998824E+03   # ~mu_L
   2000013     4.99998831E+03   # ~mu_R
   1000014     5.00002345E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015303E+03   # ~tau_2
   1000016     5.00002345E+03   # ~nu_tauL
   1000021     5.50087757E+02   # ~g
   1000022     3.99990341E+02   # ~chi_10
   1000023    -1.01759096E+03   # ~chi_20
   1000025     1.01932386E+03   # ~chi_30
   1000035     5.02219749E+03   # ~chi_40
   1000024     1.01609180E+03   # ~chi_1+
   1000037     5.02219733E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97386145E-01   # N_11
  1  2    -1.21040772E-03   # N_12
  1  3     5.04280524E-02   # N_13
  1  4    -5.17341665E-02   # N_14
  2  1     9.23019728E-04   # N_21
  2  2    -3.76840146E-04   # N_22
  2  3     7.07130885E-01   # N_23
  2  4     7.07081973E-01   # N_24
  3  1     7.22495023E-02   # N_31
  3  2     1.93019486E-02   # N_32
  3  3    -7.05144366E-01   # N_33
  3  4     7.05109117E-01   # N_34
  4  1     1.86999450E-04   # N_41
  4  2    -9.99812896E-01   # N_42
  4  3    -1.39407825E-02   # N_43
  4  4     1.34086513E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97132181E-02   # U_11
  1  2     9.99805676E-01   # U_12
  2  1     9.99805676E-01   # U_21
  2  2     1.97132181E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89605653E-02   # V_11
  1  2     9.99820232E-01   # V_12
  2  1     9.99820232E-01   # V_21
  2  2     1.89605653E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.04115137E-01   # cos(theta_t)
  1  2     9.94565251E-01   # sin(theta_t)
  2  1    -9.94565251E-01   # -sin(theta_t)
  2  2     1.04115137E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19455427E-01   # cos(theta_b)
  1  2     6.94538616E-01   # sin(theta_b)
  2  1    -6.94538616E-01   # -sin(theta_b)
  2  2     7.19455427E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173106E-01   # cos(theta_tau)
  1  2     7.07040450E-01   # sin(theta_tau)
  2  1    -7.07040450E-01   # -sin(theta_tau)
  2  2     7.07173106E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22304670E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190428E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.42983444E-01   # tanbeta(Q)          
         3     2.44101571E+02   # vev(Q)              
         4     1.10490598E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190428E+03  # The gauge couplings
     1     3.65890442E-01   # gprime(Q) DRbar
     2     6.34854500E-01   # g(Q) DRbar
     3     1.01976028E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190428E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190428E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190428E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17511137E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87486918E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38877868E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190428E+03  # The soft SUSY breaking masses at the scale Q
         1     4.07200000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     3.29280000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03321574E+05   # M^2_Hd              
        22     6.86089149E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39082365E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.11818882E-09   # gluino decays
#          BR         NDA      ID1       ID2
     5.93989219E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.63079349E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.26373063E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.63079349E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.26373063E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.52390819E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.88158756E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.97473869E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.06458817E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.05127841E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.10648472E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.58017484E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.66768072E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.85738045E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.37747490E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.38018503E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.77409655E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.40951650E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.19406408E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.32706780E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.70066591E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.05243769E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.38181071E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.17867294E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.76716404E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11109100E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.64270090E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.31185508E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     9.10222246E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.01085995E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.47913583E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.97323781E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.95627029E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.08660100E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.55633394E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.39669052E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.70370976E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.37588318E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.08915072E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.15377405E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.95569092E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.14411738E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.89111565E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.28901937E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97688048E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.48291850E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.28123595E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.63838927E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.61611316E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.29482488E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67026003E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.37585301E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.14261665E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.66157137E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.59013338E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.13728681E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.20617154E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.27392680E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97689603E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.39677586E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.41110108E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.76322954E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.14273376E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.12502245E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91547465E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.37588318E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.08915072E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.15377405E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.95569092E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.14411738E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.89111565E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.28901937E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97688048E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.48291850E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.28123595E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.63838927E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.61611316E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.29482488E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67026003E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.37585301E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.14261665E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.66157137E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.59013338E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.13728681E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.20617154E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.27392680E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97689603E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.39677586E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.41110108E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.76322954E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.14273376E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.12502245E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91547465E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.59434894E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.87457730E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.72199331E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.03707119E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.17149094E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.62843347E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95140363E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.93255518E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85884347E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.59434894E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.87457730E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.72199331E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.03707119E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.17149094E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.62843347E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95140363E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.93255518E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85884347E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.64891984E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91576555E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.57508215E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.93384617E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.93209104E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.64423565E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.92438489E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.14432536E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.04184055E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.23767733E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.58921640E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96592794E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.30882690E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.39447450E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.01042220E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.58921640E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96592794E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.30882690E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.39447450E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.01042220E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.60683834E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93934655E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.30266874E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.39075512E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.67228700E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.99477671E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51263635E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.15485039E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07984048E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07984048E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07303645E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08637549E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08637549E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05791345E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02849220E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78538541E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.35175318E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71604828E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71122157E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85265140E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.32031473E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.32039213E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.86647707E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31602276E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.28437473E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.03931122E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99730270E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.69730077E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.08158289E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.65005849E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99834994E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51261455E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.61651908E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78509104E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.74014282E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71569101E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71569101E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.77406569E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.65452062E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.85143603E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.11234210E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28885640E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.48319904E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84672969E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.33202040E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.26594404E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32411949E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32411949E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04299591E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04299591E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.84875693E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.84875693E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04299591E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04299591E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.84875693E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.84875693E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27177693E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27177693E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12432276E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12432276E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04014715E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04014715E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04014715E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04014715E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04014715E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04014715E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.23313097E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45664795E-01    2           5        -5   # BR(h -> b       bb     )
     8.11709660E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87662545E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56511340E-04    2           3        -3   # BR(h -> s       sb     )
     2.77578645E-02    2           4        -4   # BR(h -> c       cb     )
     4.19254172E-02    2          21        21   # BR(h -> g       g      )
     1.09988012E-03    2          22        22   # BR(h -> gam     gam    )
     1.15246970E-03    2          24       -24   # BR(h -> W+      W-     )
     2.84433267E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.86033501E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47276218E-04    2           5        -5   # BR(H -> b       bb     )
     3.76676295E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33155948E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.84902749E-07    2           3        -3   # BR(H -> s       sb     )
     1.11066290E-05    2           4        -4   # BR(H -> c       cb     )
     9.96306298E-01    2           6        -6   # BR(H -> t       tb     )
     1.42481206E-03    2          21        21   # BR(H -> g       g      )
     4.73375954E-06    2          22        22   # BR(H -> gam     gam    )
     1.68518738E-06    2          23        22   # BR(H -> Z       gam    )
     3.83146333E-04    2          24       -24   # BR(H -> W+      W-     )
     1.89531763E-04    2          23        23   # BR(H -> Z       Z      )
     1.39327636E-03    2          25        25   # BR(H -> h       h      )
     1.09485079E-18    2          36        36   # BR(H -> A       A      )
     1.73332790E-09    2          23        36   # BR(H -> Z       A      )
     4.72354336E-10    2          24       -37   # BR(H -> W+      H-     )
     4.72354336E-10    2         -24        37   # BR(H -> W-      H+     )
     1.44951577E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20712191E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39503957E-04    2           5        -5   # BR(A -> b       bb     )
     3.53956838E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25123036E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87290438E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308557E-05    2           4        -4   # BR(A -> c       cb     )
     9.97512002E-01    2           6        -6   # BR(A -> t       tb     )
     1.70717211E-03    2          21        21   # BR(A -> g       g      )
     5.53875051E-06    2          22        22   # BR(A -> gam     gam    )
     2.07430170E-06    2          23        22   # BR(A -> Z       gam    )
     3.44178178E-04    2          23        25   # BR(A -> Z       h      )
     1.43591531E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05605568E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97394073E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65764692E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29297084E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43985202E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83065592E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02835048E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99592408E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.60194099E-04    2          24        25   # BR(H+ -> W+      h      )
     1.14431138E-11    2          24        36   # BR(H+ -> W+      A      )
