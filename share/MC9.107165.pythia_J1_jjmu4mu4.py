###############################################################
#
# Job options file
#
# Dimuons from jets
#==============================================================

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.PythiaCommand +=["pysubs msel 1",
                        "pysubs ckin 3 17.", 
                        "pysubs ckin 4 35."]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.minevents = 50

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
# bugfix setting by Borut
try:
   svcMgr.EventSelector.EventsPerRun=2000000000
except:
   pass


from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.Ptcut = 4000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 2

try:
    StreamEVGEN.RequireAlgs += [ "MultiMuonFilter" ]
except Exception, e:
    pass

#==============================================================
#
# End of job options file
#
###############################################################


