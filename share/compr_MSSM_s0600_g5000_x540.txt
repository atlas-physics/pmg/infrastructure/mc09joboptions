#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019026E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190262E+03   # EWSB                
         1     5.64800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05862343E+01   # W+
        25     7.05172018E+01   # h
        35     1.00936868E+03   # H
        36     1.00000000E+03   # A
        37     1.00328682E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.99864591E+02   # ~d_L
   2000001     6.00002361E+02   # ~d_R
   1000002     6.00174257E+02   # ~u_L
   2000002     6.00104860E+02   # ~u_R
   1000003     5.99864591E+02   # ~s_L
   2000003     6.00002361E+02   # ~s_R
   1000004     6.00174257E+02   # ~c_L
   2000004     6.00104860E+02   # ~c_R
   1000005     5.04726393E+03   # ~b_1
   2000005     5.04771147E+03   # ~b_2
   1000006     4.90968663E+03   # ~t_1
   2000006     5.04698307E+03   # ~t_2
   1000011     4.99998819E+03   # ~e_L
   2000011     4.99998831E+03   # ~e_R
   1000012     5.00002350E+03   # ~nu_eL
   1000013     4.99998819E+03   # ~mu_L
   2000013     4.99998831E+03   # ~mu_R
   1000014     5.00002350E+03   # ~nu_muL
   1000015     4.99982373E+03   # ~tau_1
   2000015     5.00015337E+03   # ~tau_2
   1000016     5.00002350E+03   # ~nu_tauL
   1000021     5.15145022E+03   # ~g
   1000022     5.39992615E+02   # ~chi_10
   1000023    -1.01780286E+03   # ~chi_20
   1000025     1.02047803E+03   # ~chi_30
   1000035     4.96012236E+03   # ~chi_40
   1000024     1.01626226E+03   # ~chi_1+
   1000037     4.96012219E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.95602077E-01   # N_11
  1  2    -1.65100022E-03   # N_12
  1  3     6.56399818E-02   # N_13
  1  4    -6.68219369E-02   # N_14
  2  1     8.35732189E-04   # N_21
  2  2    -3.79652454E-04   # N_22
  2  3     7.07129769E-01   # N_23
  2  4     7.07083197E-01   # N_24
  3  1     9.36790560E-02   # N_31
  3  2     1.96916351E-02   # N_32
  3  3    -7.03886423E-01   # N_33
  3  4     7.03832635E-01   # N_34
  4  1     2.00676460E-04   # N_41
  4  2    -9.99804666E-01   # N_42
  4  3    -1.42402914E-02   # N_43
  4  4     1.37041695E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01365920E-02   # U_11
  1  2     9.99797238E-01   # U_12
  2  1     9.99797238E-01   # U_21
  2  2     2.01365920E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.93782933E-02   # V_11
  1  2     9.99812223E-01   # V_12
  2  1     9.99812223E-01   # V_21
  2  2     1.93782933E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.07102064E-01   # cos(theta_t)
  1  2     9.94248031E-01   # sin(theta_t)
  2  1    -9.94248031E-01   # -sin(theta_t)
  2  2     1.07102064E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19489264E-01   # cos(theta_b)
  1  2     6.94503563E-01   # sin(theta_b)
  2  1    -6.94503563E-01   # -sin(theta_b)
  2  2     7.19489264E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07238256E-01   # cos(theta_tau)
  1  2     7.06975282E-01   # sin(theta_tau)
  2  1    -7.06975282E-01   # -sin(theta_tau)
  2  2     7.07238256E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21984548E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190262E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43461780E-01   # tanbeta(Q)          
         3     2.44640530E+02   # vev(Q)              
         4     1.10586354E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190262E+03  # The gauge couplings
     1     3.66588859E-01   # gprime(Q) DRbar
     2     6.37166226E-01   # g(Q) DRbar
     3     1.01951609E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190262E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190262E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190262E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190262E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17163702E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190262E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87158281E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190262E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38835006E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190262E+03  # The soft SUSY breaking masses at the scale Q
         1     5.64800000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.04974160E+05   # M^2_Hd              
        22     6.79768394E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.44800000E+02   # M_q1L               
        42     4.44800000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     4.44800000E+02   # M_uR                
        45     4.44800000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     4.44800000E+02   # M_dR                
        48     4.44800000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40108783E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.50580515E+02   # gluino decays
#          BR         NDA      ID1       ID2
     6.24322149E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.24322149E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.24314264E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.24314264E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.24304424E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.24304424E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.24308397E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.24308397E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.24322149E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.24322149E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.24314264E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.24314264E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.24304424E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.24304424E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.24308397E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.24308397E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     1.08123110E-04    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.08123110E-04    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.16235676E-04    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.16235676E-04    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.25794463E-04    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.25794463E-04    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.48954937E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.74663810E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.41607733E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.36727700E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.74198186E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26466352E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.32342145E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.86587878E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.88093513E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.67508742E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.06566513E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.82742252E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.50046161E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.41342407E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.77560995E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.53095983E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.40815370E-04    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.05799921E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.71870899E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.26709438E-04    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     6.29356575E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.76532814E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43250739E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.26041379E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.07026342E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.71391303E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.20902616E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     4.57505321E-03   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     7.43492165E-02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     4.69804275E-03   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     1.85378077E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     4.57505321E-03   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     7.43492165E-02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     4.69804275E-03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     1.85378077E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     6.56505376E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80083981E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89311500E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.52970571E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.75390307E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.29241413E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.55112897E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.61017257E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.91743774E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.57345848E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     8.25556907E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.04056186E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.56505376E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80083981E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89311500E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.52970571E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.75390307E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.29241413E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.55112897E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.61017257E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.91743774E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.57345848E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     8.25556907E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.04056186E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.63823484E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.87653540E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.59551670E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.33423610E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53864966E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.99070229E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.08104680E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.63372630E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.87961906E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.18388908E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.10451959E-02    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57009554E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.11609117E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14383265E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.55991627E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.92232676E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.09414874E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.30769843E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.77721013E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.12469097E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.55511955E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.55991627E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.92232676E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.09414874E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.30769843E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.77721013E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.12469097E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.55511955E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.57751660E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.89577628E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.08854515E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.29884758E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.75639962E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.79632845E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.54946761E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02748634E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.14640242E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     2.32007365E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     2.14640242E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     2.32007365E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.91067048E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.02236515E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.87873698E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.87872921E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.87873698E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.87872921E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.05041589E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.47718390E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.55059225E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.30729720E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.28315509E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.59644777E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.79036122E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.79031793E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.60630462E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.78925195E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.77498712E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05454627E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99857501E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.32945575E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.49511008E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.49511008E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     1.27719908E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     1.27719908E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     9.06561585E-07    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     9.06561585E-07    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     3.19402916E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     3.19402916E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.49511008E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.49511008E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     1.27719908E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     1.27719908E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     9.06561585E-07    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     9.06561585E-07    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     3.19402916E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     3.19402916E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.19840585E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.68684307E-05    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.21754702E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.90539768E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     3.90539768E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.42256593E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.42256593E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     8.21025280E-06    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     8.21025280E-06    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     3.55755460E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.55755460E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     3.90539768E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     3.90539768E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.42256593E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.42256593E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     8.21025280E-06    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     8.21025280E-06    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     3.55755460E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.55755460E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.02236774E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.54349083E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.47652537E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63945501E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.30633668E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.30633668E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.72028063E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.14793509E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.60074050E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.68331381E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.78260820E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.60946367E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.56903820E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.65229916E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.77042551E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.79130757E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.79130757E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.13783336E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.13783336E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.16656507E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.16656507E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.13943013E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.13943013E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.41644989E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.41644989E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.13783336E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.13783336E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.16656507E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.16656507E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.13943013E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.13943013E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.41644989E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.41644989E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.86558322E-03   # h decays
#          BR         NDA      ID1       ID2
     8.61616698E-01    2           5        -5   # BR(h -> b       bb     )
     7.91532487E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80869205E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.69503467E-04    2           3        -3   # BR(h -> s       sb     )
     2.83283091E-02    2           4        -4   # BR(h -> c       cb     )
     2.89968326E-02    2          21        21   # BR(h -> g       g      )
     6.80311065E-04    2          22        22   # BR(h -> gam     gam    )
     2.11538393E-04    2          24       -24   # BR(h -> W+      W-     )
     6.26898528E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85397812E+01   # H decays
#          BR         NDA      ID1       ID2
     2.46706612E-04    2           5        -5   # BR(H -> b       bb     )
     3.77550785E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33465083E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85342885E-07    2           3        -3   # BR(H -> s       sb     )
     1.11071218E-05    2           4        -4   # BR(H -> c       cb     )
     9.96307356E-01    2           6        -6   # BR(H -> t       tb     )
     1.42499119E-03    2          21        21   # BR(H -> g       g      )
     4.74059383E-06    2          22        22   # BR(H -> gam     gam    )
     1.68483763E-06    2          23        22   # BR(H -> Z       gam    )
     3.76707805E-04    2          24       -24   # BR(H -> W+      W-     )
     1.86345705E-04    2          23        23   # BR(H -> Z       Z      )
     1.40228326E-03    2          25        25   # BR(H -> h       h      )
     1.00559038E-18    2          36        36   # BR(H -> A       A      )
     1.59634577E-09    2          23        36   # BR(H -> Z       A      )
     4.48675949E-10    2          24       -37   # BR(H -> W+      H-     )
     4.48675949E-10    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     5.20107747E+01   # A decays
#          BR         NDA      ID1       ID2
     2.38935543E-04    2           5        -5   # BR(A -> b       bb     )
     3.54727793E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25395568E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87690244E-07    2           3        -3   # BR(A -> s       sb     )
     1.02323620E-05    2           4        -4   # BR(A -> c       cb     )
     9.97658865E-01    2           6        -6   # BR(A -> t       tb     )
     1.70743177E-03    2          21        21   # BR(A -> g       g      )
     5.53975456E-06    2          22        22   # BR(A -> gam     gam    )
     2.07323273E-06    2          23        22   # BR(A -> Z       gam    )
     3.41136550E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     5.05046962E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.95987977E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.66506686E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29559377E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.43085186E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.84868621E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02839950E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99595673E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.56855072E-04    2          24        25   # BR(H+ -> W+      h      )
     9.95826655E-12    2          24        36   # BR(H+ -> W+      A      )
