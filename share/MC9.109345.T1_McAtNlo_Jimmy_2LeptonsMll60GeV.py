###############################################################
#
# Job options file
# Carl Gwilliam
# (based on original from Wouter Verkerke)
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
#MessageSvc = Service( "MessageSvc" )
#MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
topAlg += DiLeptonMassFilter()
DiLeptonMassFilter = topAlg.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 5000.
DiLeptonMassFilter.MaxEta = 5.
DiLeptonMassFilter.MinMass = 60000.
DiLeptonMassFilter.MaxMass = 14000000.
DiLeptonMassFilter.AllowElecMu = True
DiLeptonMassFilter.AllowSameCharge = True

try:
     StreamEVGEN.RequireAlgs = [ "DiLeptonMassFilter" ]
except Exception, e:
     pass


from MC09JobOptions.McAtNloEvgenConfig import evgenConfig

# dummy needed
evgenConfig.inputfilebase = 'mcatnlo'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.109345.ttbar_7TeV.TXT.v1'
except NameError:
  pass

# 15.6.9.8+new EF (DiLeptonMassFilter)
# 5000/64754=0.0772+-0.0010
evgenConfig.efficiency = 0.0772*0.9
#==============================================================
#
# End of job options file
#
###############################################################
