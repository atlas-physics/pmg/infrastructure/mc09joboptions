#-------------------------------------------------------------------------
#
# SO(10) point: parameters obtained from a fit to EW and FCNC observables
# - from W. Altmannshofer et al. arXiv:0801.4363
#
# contact :  M-H. Genest
#
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Pythia_Common.py" )

Pythia.SusyInputFile = "susy_SO10_fitFCNC.txt";
Pythia.PythiaCommand += [ "pydat1 parj 90 20000", "pydat3 mdcy 15 1 0", "pysubs msel 39", "pymssm imss 1 11" ]

# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = INFO

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos 
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

from MC09JobOptions.PythiaEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.9

from MC09JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
###############################################################
