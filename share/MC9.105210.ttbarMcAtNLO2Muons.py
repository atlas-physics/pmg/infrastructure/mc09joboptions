###############################################################
#
# Job options file
# based on 105200 jobO made by Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
  # needed (dummy) default 
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
topAlg += MultiMuonFilter()

MultiMuonFilter = topAlg.MultiMuonFilter
MultiMuonFilter.NMuons = 2;
MultiMuonFilter.Etacut = 2.7;
MultiMuonFilter.Ptcut = 5000;

try:
     StreamEVGEN.RequireAlgs += [ "MultiMuonFilter" ]
except Exception, e:
     pass

from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'mcatnlo31.105210.ttbar_v2'
evgenConfig.efficiency = 0.0723*0.95
#==============================================================
#
# End of job options file
#
###############################################################
