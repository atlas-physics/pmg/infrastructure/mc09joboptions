#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018347E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00183475E+03   # EWSB                
         1     1.80000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.18500000E+01   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888871E+01   # W+
        25     8.40592119E+01   # h
        35     1.00920451E+03   # H
        36     1.00000000E+03   # A
        37     1.00335712E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04607430E+03   # ~d_L
   2000001     5.04608943E+03   # ~d_R
   1000002     5.04610832E+03   # ~u_L
   2000002     5.04610073E+03   # ~u_R
   1000003     5.04607430E+03   # ~s_L
   2000003     5.04608943E+03   # ~s_R
   1000004     5.04610832E+03   # ~c_L
   2000004     5.04610073E+03   # ~c_R
   1000005     5.04586073E+03   # ~b_1
   2000005     5.04630407E+03   # ~b_2
   1000006     4.91658618E+03   # ~t_1
   2000006     5.04751860E+03   # ~t_2
   1000011     4.99998874E+03   # ~e_L
   2000011     4.99998880E+03   # ~e_R
   1000012     5.00002245E+03   # ~nu_eL
   1000013     4.99998874E+03   # ~mu_L
   2000013     4.99998880E+03   # ~mu_R
   1000014     5.00002245E+03   # ~nu_muL
   1000015     4.99982415E+03   # ~tau_1
   2000015     5.00015400E+03   # ~tau_2
   1000016     5.00002245E+03   # ~nu_tauL
   1000021     1.49938433E+02   # ~g
   1000022    -9.88731063E-01   # ~chi_10
   1000023    -1.01787739E+03   # ~chi_20
   1000025     1.01833580E+03   # ~chi_30
   1000035     5.02220409E+03   # ~chi_40
   1000024     1.01637362E+03   # ~chi_1+
   1000037     5.02220394E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99035888E-01   # N_11
  1  2    -6.77161794E-04   # N_12
  1  3     3.01556927E-02   # N_13
  1  4    -3.18978193E-02   # N_14
  2  1     1.23102315E-03   # N_21
  2  2    -3.60411537E-04   # N_22
  2  3     7.07135020E-01   # N_23
  2  4     7.07077378E-01   # N_24
  3  1     4.38833684E-02   # N_31
  3  2     1.93560168E-02   # N_32
  3  3    -7.06297475E-01   # N_33
  3  4     7.06288519E-01   # N_34
  4  1     1.72486980E-04   # N_41
  4  2    -9.99812360E-01   # N_42
  4  3    -1.39490031E-02   # N_43
  4  4     1.34402155E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97250033E-02   # U_11
  1  2     9.99805443E-01   # U_12
  2  1     9.99805443E-01   # U_21
  2  2     1.97250033E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.90053615E-02   # V_11
  1  2     9.99819382E-01   # V_12
  2  1     9.99819382E-01   # V_21
  2  2     1.90053615E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00926018E-01   # cos(theta_t)
  1  2     9.94893933E-01   # sin(theta_t)
  2  1    -9.94893933E-01   # -sin(theta_t)
  2  2     1.00926018E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19069364E-01   # cos(theta_b)
  1  2     6.94938306E-01   # sin(theta_b)
  2  1    -6.94938306E-01   # -sin(theta_b)
  2  2     7.19069364E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07172899E-01   # cos(theta_tau)
  1  2     7.07040657E-01   # sin(theta_tau)
  2  1    -7.07040657E-01   # -sin(theta_tau)
  2  2     7.07172899E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20621273E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00183475E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45487246E-01   # tanbeta(Q)          
         3     2.44400633E+02   # vev(Q)              
         4     1.09996213E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00183475E+03  # The gauge couplings
     1     3.65909759E-01   # gprime(Q) DRbar
     2     6.34937259E-01   # g(Q) DRbar
     3     1.04719061E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00183475E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15038679E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.84889940E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00183475E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38910326E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00183475E+03  # The soft SUSY breaking masses at the scale Q
         1     1.80000000E+00   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     5.18500000E+01   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03399385E+05   # M^2_Hd              
        22     6.37126130E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39118736E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.10746381E-10   # gluino decays
#          BR         NDA      ID1       ID2
     5.51018264E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.66888258E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.28137979E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.66888258E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.28137979E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.52445649E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.15669790E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.86292092E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.77135903E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.69902233E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.93649438E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.93017539E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.99372910E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.55324135E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23672604E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.24065980E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.16859664E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.07578379E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.48374911E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.11299737E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.42920887E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.40091230E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.06620624E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.76851606E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.71941498E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.33167102E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.45317383E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.50474628E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     6.45721872E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.36039349E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.20373099E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.53818834E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.23961176E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.28921972E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.37241977E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.58402326E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.18989164E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.75088145E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.91520572E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.47627409E-10    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.75765125E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.30823418E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.51705425E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.61722083E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97882792E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.85889675E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.99968167E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.23314658E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.37898714E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.35671673E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.69949351E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.75084852E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.94243262E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.73413904E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.81032310E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.30201007E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.78844861E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.60339111E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97884801E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.77191651E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.67211833E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.08268789E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.37575303E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.88059908E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.92314113E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.75088145E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.91520572E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.47627409E-10    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.75765125E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.30823418E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.51705425E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.61722083E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97882792E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.85889675E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.99968167E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.23314658E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.37898714E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.35671673E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.69949351E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.75084852E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.94243262E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.73413904E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.81032310E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.30201007E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.78844861E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.60339111E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97884801E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.77191651E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.67211833E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.08268789E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.37575303E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.88059908E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.92314113E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.68172627E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.92358103E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.35872196E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.49544686E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14611453E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66323265E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98229029E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.39263155E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.76957864E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.68172627E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.92358103E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.35872196E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.49544686E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14611453E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66323265E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98229029E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.39263155E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.76957864E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.67057871E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94643044E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54939976E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.89327986E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90873615E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66611590E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.96302806E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.04543759E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.18752592E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.12446516E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67596095E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97905575E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.15860195E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.71552882E-05    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99411096E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.67596095E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97905575E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.15860195E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.71552882E-05    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99411096E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.69359026E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95277332E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.15028295E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.68994045E-05    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.62261879E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.25506539E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99249129E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     7.50870723E-04    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000037     7.52524110E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     8.79129098E-06    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07831453E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07831453E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07152002E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08456416E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08456416E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05703378E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02756103E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78782971E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.32461810E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71846400E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71767625E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85033280E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.30802787E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.30797766E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.33128185E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.30364781E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29739894E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.53177145E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.98387207E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.28129898E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     3.30571268E-04    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     9.22771687E-07    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000025     6.66344022E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.70211507E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.97869645E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.55060289E-07    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.35978858E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
#
#         PDG            Width
DECAY   1000035     7.52523617E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.52290768E-08    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78755970E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.16309587E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71813212E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71813212E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42428085E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.26399375E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.32332225E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.01186492E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.27852123E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.35019786E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84883497E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.12926377E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.28001465E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31187719E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31187719E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04210395E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04210395E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.12113696E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.12113696E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04210395E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04210395E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.12113696E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.12113696E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.26745751E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.26745751E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.11977194E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.11977194E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03936398E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03936398E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03936398E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03936398E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03936398E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03936398E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.56093882E-03   # h decays
#          BR         NDA      ID1       ID2
     7.20540617E-01    2           5        -5   # BR(h -> b       bb     )
     6.87767960E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.43774196E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.59629801E-04    2           3        -3   # BR(h -> s       sb     )
     2.36966962E-02    2           4        -4   # BR(h -> c       cb     )
     3.38671383E-02    2          21        21   # BR(h -> g       g      )
     8.74002575E-04    2          22        22   # BR(h -> gam     gam    )
     7.44778191E-04    2          24       -24   # BR(h -> W+      W-     )
     1.94408083E-04    2          23        23   # BR(h -> Z       Z      )
     1.50502160E-01    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.82966133E+01   # H decays
#          BR         NDA      ID1       ID2
     2.50515507E-04    2           5        -5   # BR(H -> b       bb     )
     3.81273348E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34781019E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.87202587E-07    2           3        -3   # BR(H -> s       sb     )
     1.11080082E-05    2           4        -4   # BR(H -> c       cb     )
     9.96342003E-01    2           6        -6   # BR(H -> t       tb     )
     1.42519180E-03    2          21        21   # BR(H -> g       g      )
     4.73761497E-06    2          22        22   # BR(H -> gam     gam    )
     1.68903044E-06    2          23        22   # BR(H -> Z       gam    )
     3.49461253E-04    2          24       -24   # BR(H -> W+      W-     )
     1.72867511E-04    2          23        23   # BR(H -> Z       Z      )
     1.40358922E-03    2          25        25   # BR(H -> h       h      )
     9.13142348E-19    2          36        36   # BR(H -> A       A      )
     1.46879206E-09    2          23        36   # BR(H -> Z       A      )
     3.70499709E-10    2          24       -37   # BR(H -> W+      H-     )
     3.70499709E-10    2         -24        37   # BR(H -> W-      H+     )
     3.85134742E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.17915833E+01   # A decays
#          BR         NDA      ID1       ID2
     2.42235751E-04    2           5        -5   # BR(A -> b       bb     )
     3.57760244E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26467533E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89260157E-07    2           3        -3   # BR(A -> s       sb     )
     1.02316884E-05    2           4        -4   # BR(A -> c       cb     )
     9.97593186E-01    2           6        -6   # BR(A -> t       tb     )
     1.70735464E-03    2          21        21   # BR(A -> g       g      )
     5.53927787E-06    2          22        22   # BR(A -> gam     gam    )
     2.07448035E-06    2          23        22   # BR(A -> Z       gam    )
     3.14325680E-04    2          23        25   # BR(A -> Z       h      )
     8.89606607E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.02906192E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.01850034E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.69674793E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30679297E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.46836679E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.92506130E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02857515E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99623289E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.28913539E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11161781E-11    2          24        36   # BR(H+ -> W+      A      )
