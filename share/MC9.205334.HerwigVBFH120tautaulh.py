###############################################################
#
# Job options file
#
# Higgs via VBF with H->tautau with 1lep GEF for lh channel
#
# Responsible person(s)
#   25 Aug, 2008-xx xxx, 20xx: Soshi Tsuno (Soshi.Tsuno@cern.ch)
#
#==============================================================
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

# ... Herwig
include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )
Herwig.HerwigCommand += ["iproc 11909",
                         "rmass 201 120.0",
                         "taudec TAUOLA"]
 
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
try:
     StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
except Exception, e:
     pass

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = 0.424
#5000/10600=.47169811320754716981
#5000/10600*0.9=.42452830188679245282
#==============================================================
#
# End of job options file
#
###############################################################
