###############################################################
#
# Job options file
# Wouter Verkerke
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
try:
  if runArgs.ecmEnergy == 7000.0:
    print runArgs 
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_AlpgenJimmy_Common.py" )
except NameError:
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig 

Herwig.HerwigCommand += [ "iproc alpgen",
                          "taudec TAUOLA",
                        ]
# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC09JobOptions.AlpgenEvgenConfig import evgenConfig
 
# input file names need updating for MC9
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107690.WmunuNp0_pt20_7tev.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107690.WmunuNp0.pt20.v2'    
except NameError:
  pass

# 7 TeV - Information on sample 107690
# 7 TeV - Filter efficiency  = 1.0000
# 7 TeV - MLM matching efficiency = 0.84
# 7 TeV - Number of Matrix Elements in input file  = 800
# 7 TeV - Alpgen cross section = 8274.8 pb
# 7 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 6935.4 pb
# 7 TeV - Cross section after filtering = 6935.4 pb
# 7 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.07 pb-1
#
# 7 TeV - Filter efficiency estimate below reduced by 10% to produce 555 events on average,
# 7 TeV - of which only 500 will be used in further processing
evgenConfig.efficiency = 0.90000
# 10 TeV - Information on sample 107690
# 10 TeV - Filter efficiency  = 1.0000
# 10 TeV - MLM matching efficiency = 0.81
# 10 TeV - Number of Matrix Elements in input file  = 600
# 10 TeV - Alpgen cross section = 12476.7 pb
# 10 TeV - Herwig cross section = Alpgen cross section * eff(MLM) = 10125.7 pb
# 10 TeV - Cross section after filtering = 10125.7 pb
# 10 TeV - Lumi/500 events post-filter = 500/eff(Filter)/XS(jimmy) = 0.05 pb-1
#==============================================================
#
# End of job options file
#
###############################################################
