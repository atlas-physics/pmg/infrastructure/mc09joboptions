#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50019002E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00190025E+03   # EWSB                
         1     3.06000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.91400000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05889085E+01   # W+
        25     8.63104254E+01   # h
        35     1.00950608E+03   # H
        36     1.00000000E+03   # A
        37     1.00337377E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04413576E+03   # ~d_L
   2000001     5.04415151E+03   # ~d_R
   1000002     5.04417118E+03   # ~u_L
   2000002     5.04416327E+03   # ~u_R
   1000003     5.04413576E+03   # ~s_L
   2000003     5.04415151E+03   # ~s_R
   1000004     5.04417118E+03   # ~c_L
   2000004     5.04416327E+03   # ~c_R
   1000005     5.04392026E+03   # ~b_1
   2000005     5.04436810E+03   # ~b_2
   1000006     4.90927312E+03   # ~t_1
   2000006     5.04301876E+03   # ~t_2
   1000011     4.99998827E+03   # ~e_L
   2000011     4.99998833E+03   # ~e_R
   1000012     5.00002340E+03   # ~nu_eL
   1000013     4.99998827E+03   # ~mu_L
   2000013     4.99998833E+03   # ~mu_R
   1000014     5.00002340E+03   # ~nu_muL
   1000015     4.99982412E+03   # ~tau_1
   2000015     5.00015309E+03   # ~tau_2
   1000016     5.00002340E+03   # ~nu_tauL
   1000021     5.00063366E+02   # ~g
   1000022     3.00047763E+02   # ~chi_10
   1000023    -1.01760742E+03   # ~chi_20
   1000025     1.01888906E+03   # ~chi_30
   1000035     5.02219790E+03   # ~chi_40
   1000024     1.01610799E+03   # ~chi_1+
   1000037     5.02219775E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98062613E-01   # N_11
  1  2    -1.02011341E-03   # N_12
  1  3     4.32817952E-02   # N_13
  1  4    -4.46840652E-02   # N_14
  2  1     9.90848513E-04   # N_21
  2  2    -3.75954084E-04   # N_22
  2  3     7.07131869E-01   # N_23
  2  4     7.07080899E-01   # N_24
  3  1     6.22093627E-02   # N_31
  3  2     1.93140956E-02   # N_32
  3  3    -7.05618066E-01   # N_33
  3  4     7.05592025E-01   # N_34
  4  1     1.83042261E-04   # N_41
  4  2    -9.99812874E-01   # N_42
  4  3    -1.39409849E-02   # N_43
  4  4     1.34101340E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97135490E-02   # U_11
  1  2     9.99805669E-01   # U_12
  2  1     9.99805669E-01   # U_21
  2  2     1.97135490E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89627054E-02   # V_11
  1  2     9.99820192E-01   # V_12
  2  1     9.99820192E-01   # V_21
  2  2     1.89627054E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.03741200E-01   # cos(theta_t)
  1  2     9.94604325E-01   # sin(theta_t)
  2  1    -9.94604325E-01   # -sin(theta_t)
  2  2     1.03741200E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19434825E-01   # cos(theta_b)
  1  2     6.94559956E-01   # sin(theta_b)
  2  1    -6.94559956E-01   # -sin(theta_b)
  2  2     7.19434825E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173130E-01   # cos(theta_tau)
  1  2     7.07040426E-01   # sin(theta_tau)
  2  1    -7.07040426E-01   # -sin(theta_tau)
  2  2     7.07173130E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.22211749E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00190025E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43119754E-01   # tanbeta(Q)          
         3     2.44113592E+02   # vev(Q)              
         4     1.10428492E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00190025E+03  # The gauge couplings
     1     3.65891730E-01   # gprime(Q) DRbar
     2     6.34859941E-01   # g(Q) DRbar
     3     1.02144540E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00190025E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00190025E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00190025E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00190025E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.17371688E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00190025E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.87340230E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00190025E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38882465E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00190025E+03  # The soft SUSY breaking masses at the scale Q
         1     3.06000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.91400000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03129882E+05   # M^2_Hd              
        22     6.83482151E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39084764E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.92159082E-08   # gluino decays
#          BR         NDA      ID1       ID2
     2.64946074E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     9.95940668E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.37683739E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.95940668E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.37683739E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.89497815E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.90626170E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.95838942E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.05779465E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.04652078E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.09345718E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.60638844E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.69476307E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.75616176E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.36652125E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.37017645E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.70825192E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.50615398E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.21672666E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.30747088E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.58962486E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.08076866E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.37050635E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.14890480E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.14882264E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.10916455E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.62795293E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.32670317E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.87972146E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.03909830E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.48578399E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.93458579E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.70708079E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.08351322E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.54161633E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.41149162E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.49210398E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.40556699E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.08757500E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.84751028E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.24162747E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.13275628E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.85795887E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.26627777E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97697437E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.51310728E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.27259979E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.01416040E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.18789598E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.92659474E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.67155182E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.40553636E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.13248176E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72001727E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.90702092E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.12596985E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.16952916E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.25130308E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97699126E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.42655845E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.38812995E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.72571115E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.04474215E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.03049263E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91581415E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.40556699E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.08757500E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.84751028E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.24162747E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.13275628E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.85795887E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.26627777E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97697437E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.51310728E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.27259979E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.01416040E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.18789598E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.92659474E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.67155182E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.40553636E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.13248176E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72001727E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.90702092E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.12596985E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.16952916E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.25130308E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97699126E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.42655845E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.38812995E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.72571115E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.04474215E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.03049263E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91581415E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.63204070E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.89389692E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.05724451E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.45094898E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.15925333E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.64347477E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.96417041E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.08927665E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.58205043E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.63204070E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.89389692E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.05724451E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.45094898E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.15925333E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.64347477E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.96417041E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.08927665E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.58205043E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.65829692E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.92849845E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.55423763E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.67345791E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.92127354E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65366939E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.93993736E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.10705291E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.49034944E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.20886989E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.62673663E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97237793E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.49285838E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.60194462E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.99951935E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.62673663E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97237793E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.49285838E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.60194462E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.99951935E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.64435969E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94592787E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.48624649E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.58178173E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.64654908E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     9.41132687E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51322100E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.12697615E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07980191E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07980191E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07299828E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08632180E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08632180E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05788882E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02846318E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78549050E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.60967765E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71615360E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71307211E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85255627E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31976433E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31983143E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.83439626E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31547091E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.29414924E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.79999898E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99581896E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.18103792E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.01467279E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.53206479E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99746794E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51320065E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.32312306E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78519077E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.71268562E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71580395E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71580395E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.89380088E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.32034690E-06    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.82220399E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.10671559E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28838770E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.47574768E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84851463E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.32362527E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.27571531E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.32360285E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.32360285E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04297266E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04297266E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.64554701E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.64554701E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04297266E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04297266E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.64554701E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.64554701E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27166918E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27166918E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12420002E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12420002E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.04012202E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.04012202E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.04012202E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.04012202E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04012202E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04012202E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.22993909E-03   # h decays
#          BR         NDA      ID1       ID2
     8.45806702E-01    2           5        -5   # BR(h -> b       bb     )
     8.11605580E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87627868E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.56640941E-04    2           3        -3   # BR(h -> s       sb     )
     2.77657108E-02    2           4        -4   # BR(h -> c       cb     )
     4.18113449E-02    2          21        21   # BR(h -> g       g      )
     1.09589959E-03    2          22        22   # BR(h -> gam     gam    )
     1.13444752E-03    2          24       -24   # BR(h -> W+      W-     )
     2.81068244E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.85863133E+01   # H decays
#          BR         NDA      ID1       ID2
     2.47468619E-04    2           5        -5   # BR(H -> b       bb     )
     3.76927380E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33244708E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85028434E-07    2           3        -3   # BR(H -> s       sb     )
     1.11067141E-05    2           4        -4   # BR(H -> c       cb     )
     9.96308317E-01    2           6        -6   # BR(H -> t       tb     )
     1.42483623E-03    2          21        21   # BR(H -> g       g      )
     4.73398926E-06    2          22        22   # BR(H -> gam     gam    )
     1.68541003E-06    2          23        22   # BR(H -> Z       gam    )
     3.81156063E-04    2          24       -24   # BR(H -> W+      W-     )
     1.88547149E-04    2          23        23   # BR(H -> Z       Z      )
     1.39385751E-03    2          25        25   # BR(H -> h       h      )
     1.08254120E-18    2          36        36   # BR(H -> A       A      )
     1.71530286E-09    2          23        36   # BR(H -> Z       A      )
     4.67139052E-10    2          24       -37   # BR(H -> W+      H-     )
     4.67139052E-10    2         -24        37   # BR(H -> W-      H+     )
     2.77791568E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.20560076E+01   # A decays
#          BR         NDA      ID1       ID2
     2.39666405E-04    2           5        -5   # BR(A -> b       bb     )
     3.54162636E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25195786E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.87397018E-07    2           3        -3   # BR(A -> s       sb     )
     1.02308873E-05    2           4        -4   # BR(A -> c       cb     )
     9.97515082E-01    2           6        -6   # BR(A -> t       tb     )
     1.70717975E-03    2          21        21   # BR(A -> g       g      )
     5.53877162E-06    2          22        22   # BR(A -> gam     gam    )
     2.07430759E-06    2          23        22   # BR(A -> Z       gam    )
     3.42415778E-04    2          23        25   # BR(A -> Z       h      )
     1.42082764E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.05455523E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.97661519E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.65976682E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29372022E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.44156343E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.83578099E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02836325E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99594237E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.58343875E-04    2          24        25   # BR(H+ -> W+      h      )
     1.13369456E-11    2          24        36   # BR(H+ -> W+      A      )
