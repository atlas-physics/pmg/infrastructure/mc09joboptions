#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.97630557E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.76305572E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     1.30000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07286167E+01   # W+
        25     1.30275986E+02   # h
        35     1.00093932E+03   # H
        36     1.00000000E+03   # A
        37     1.00383933E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03842127E+03   # ~d_L
   2000001     5.03816813E+03   # ~d_R
   1000002     5.03785517E+03   # ~u_L
   2000002     5.03798874E+03   # ~u_R
   1000003     5.03842127E+03   # ~s_L
   2000003     5.03816813E+03   # ~s_R
   1000004     5.03785517E+03   # ~c_L
   2000004     5.03798874E+03   # ~c_R
   1000005     2.40080951E+02   # ~b_1
   2000005     5.03816935E+03   # ~b_2
   1000006     9.10624382E+02   # ~t_1
   2000006     5.07190523E+03   # ~t_2
   1000011     5.00019158E+03   # ~e_L
   2000011     5.00017774E+03   # ~e_R
   1000012     4.99963065E+03   # ~nu_eL
   1000013     5.00019158E+03   # ~mu_L
   2000013     5.00017774E+03   # ~mu_R
   1000014     4.99963065E+03   # ~nu_muL
   1000015     4.99933905E+03   # ~tau_1
   2000015     5.00103075E+03   # ~tau_2
   1000016     4.99963065E+03   # ~nu_tauL
   1000021     1.22181648E+03   # ~g
   1000022     5.91070881E+01   # ~chi_10
   1000023     6.87492150E+02   # ~chi_20
   1000025    -1.00163380E+03   # ~chi_30
   1000035     1.01503456E+03   # ~chi_40
   1000024     6.87452824E+02   # ~chi_1+
   1000037     1.01471074E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98964574E-01   # N_11
  1  2    -2.47242603E-03   # N_12
  1  3     4.39281275E-02   # N_13
  1  4    -1.15752582E-02   # N_14
  2  1     1.09819136E-02   # N_21
  2  2     9.78709346E-01   # N_22
  2  3    -1.61349812E-01   # N_23
  2  4     1.26386910E-01   # N_24
  3  1     2.27760664E-02   # N_31
  3  2    -2.52427942E-02   # N_32
  3  3    -7.05818295E-01   # N_33
  3  4    -7.07576559E-01   # N_34
  4  1     3.78210024E-02   # N_41
  4  2    -2.03677941E-01   # N_42
  4  3    -6.88372786E-01   # N_43
  4  4     6.95145866E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73499832E-01   # U_11
  1  2     2.28687727E-01   # U_12
  2  1     2.28687727E-01   # U_21
  2  2     9.73499832E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83789122E-01   # V_11
  1  2     1.79329205E-01   # V_12
  2  1     1.79329205E-01   # V_21
  2  2     9.83789122E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998688E-01   # cos(theta_t)
  1  2     1.61987601E-03   # sin(theta_t)
  2  1    -1.61987601E-03   # -sin(theta_t)
  2  2     9.99998688E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999884E-01   # cos(theta_b)
  1  2     4.81663769E-04   # sin(theta_b)
  2  1    -4.81663769E-04   # -sin(theta_b)
  2  2     9.99999884E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04208395E-01   # cos(theta_tau)
  1  2     7.09993335E-01   # sin(theta_tau)
  2  1    -7.09993335E-01   # -sin(theta_tau)
  2  2     7.04208395E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10051838E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.76305572E+02  # DRbar Higgs Parameters
         1     9.97467412E+02   # mu(Q)               
         2     4.81623086E+00   # tanbeta(Q)          
         3     2.44376492E+02   # vev(Q)              
         4     9.89383161E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.76305572E+02  # The gauge couplings
     1     3.60251439E-01   # gprime(Q) DRbar
     2     6.39964371E-01   # g(Q) DRbar
     3     1.04725777E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.76305572E+02  # The trilinear couplings
  1  1     2.12280903E+03   # A_u(Q) DRbar
  2  2     2.12280903E+03   # A_c(Q) DRbar
  3  3     2.77926484E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.76305572E+02  # The trilinear couplings
  1  1     1.45490882E+03   # A_d(Q) DRbar
  2  2     1.45490882E+03   # A_s(Q) DRbar
  3  3     1.67404056E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  9.76305572E+02  # The trilinear couplings
  1  1     4.44489218E+02   # A_e(Q) DRbar
  2  2     4.44489218E+02   # A_mu(Q) DRbar
  3  3     4.44858324E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.76305572E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75815429E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.76305572E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.11860066E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.76305572E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.99938015E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.76305572E+02  # The soft SUSY breaking masses at the scale Q
         1     1.57351166E+02   # M_1                 
         2     8.92817462E+02   # M_2                 
         3     4.56981713E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.46498523E+06   # M^2_Hd              
        22     2.41508279E+07   # M^2_Hu              
        31     5.14550299E+03   # M_eL                
        32     5.14550299E+03   # M_muL               
        33     5.14895954E+03   # M_tauL              
        34     4.68187521E+03   # M_eR                
        35     4.68187521E+03   # M_muR               
        36     4.68950887E+03   # M_tauR              
        41     4.95091535E+03   # M_q1L               
        42     4.95091535E+03   # M_q2L               
        43     2.90456780E+03   # M_q3L               
        44     5.22766642E+03   # M_uR                
        45     5.22766642E+03   # M_cR                
        46     6.73286043E+03   # M_tR                
        47     4.91023833E+03   # M_dR                
        48     4.91023833E+03   # M_sR                
        49     4.91509171E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40717009E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.14221198E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.12466364E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.12466364E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.87533636E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.87533636E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.26257903E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.02930282E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98970697E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.42338338E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.54137820E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.39309346E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.24377677E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.98338056E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.77309619E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.38847838E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.38065652E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.90211540E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     7.00190129E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.88279028E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.39816539E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     3.42316447E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.00514944E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.31420485E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.84447360E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.82631643E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.96898396E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.35145797E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.07330288E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.79336592E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.39767533E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86663225E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.22392323E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.05549931E-04    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     1.05165900E-04    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     2.06686292E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.56558371E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.73868872E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.39073913E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00628579E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.46766451E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.40512506E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.10956561E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.10027992E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.51125607E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.27644204E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.91841184E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.83005538E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.48705090E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.85727201E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.11109288E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61625275E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.39086044E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.11514902E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.42226701E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.76596492E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.42542127E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.07747592E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.71066924E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.27730839E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.83454281E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.85880222E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.15499765E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.78075932E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.31563435E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90122106E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.39073913E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00628579E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.46766451E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.40512506E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.10956561E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.10027992E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.51125607E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.27644204E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.91841184E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.83005538E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.48705090E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.85727201E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.11109288E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61625275E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.39086044E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.11514902E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.42226701E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.76596492E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.42542127E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.07747592E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.71066924E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.27730839E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.83454281E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.85880222E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.15499765E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.78075932E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.31563435E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90122106E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.51516053E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79771045E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.91925866E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.44482717E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.56119151E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70426404E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.00649861E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.58086953E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98090158E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.16136736E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.78162051E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.31554304E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.51516053E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79771045E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.91925866E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.44482717E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.56119151E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70426404E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.00649861E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.58086953E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98090158E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.16136736E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.78162051E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.31554304E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.57060750E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54533113E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13926766E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.44550684E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.49312892E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18212763E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.38872255E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.59416435E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.46948543E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01493625E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.86170893E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.65374862E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93142250E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.80163867E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.51670623E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96806050E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84540509E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.17223686E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45413259E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82339688E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84806491E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.51670623E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96806050E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84540509E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.17223686E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45413259E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82339688E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84806491E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.53961714E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93313838E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83543649E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.15761984E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.44903818E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80488627E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.17301962E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.00718076E+01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.94052611E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.48522765E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     2.07197393E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.61969415E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.57575016E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.54640485E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.33562658E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.31301004E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.36786947E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.56187036E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.44436714E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.39233416E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.43165283E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.98564557E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.98564557E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.59465061E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.08099282E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.09423161E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.11983911E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.11983911E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.03605010E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.43586461E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.07135554E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.07135554E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.86580717E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.86580717E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.57845672E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.04470431E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01685506E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.21301502E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.21301502E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.00313870E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.06271106E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.81975616E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.81975616E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.23396864E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.23396864E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.05497842E-03   # h decays
#          BR         NDA      ID1       ID2
     5.39615645E-01    2           5        -5   # BR(h -> b       bb     )
     5.61311504E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.98642202E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.17344834E-04    2           3        -3   # BR(h -> s       sb     )
     1.72500623E-02    2           4        -4   # BR(h -> c       cb     )
     6.54534349E-02    2          21        21   # BR(h -> g       g      )
     2.19115001E-03    2          22        22   # BR(h -> gam     gam    )
     1.94945165E-03    2          22        23   # BR(h -> Z       gam    )
     2.76095572E-01    2          24       -24   # BR(h -> W+      W-     )
     3.87902416E-02    2          23        23   # BR(h -> Z       Z      )
     1.90730419E-03    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     2.32409686E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32325816E-01    2           5        -5   # BR(H -> b       bb     )
     2.06599157E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.30333070E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.04513403E-04    2           3        -3   # BR(H -> s       sb     )
     9.16825956E-06    2           4        -4   # BR(H -> c       cb     )
     8.20464977E-01    2           6        -6   # BR(H -> t       tb     )
     1.13835799E-03    2          21        21   # BR(H -> g       g      )
     3.76523963E-06    2          22        22   # BR(H -> gam     gam    )
     1.23409788E-06    2          23        22   # BR(H -> Z       gam    )
     3.86782498E-03    2          24       -24   # BR(H -> W+      W-     )
     1.91320996E-03    2          23        23   # BR(H -> Z       Z      )
     1.02556219E-02    2          25        25   # BR(H -> h       h      )
     4.29657754E-22    2          36        36   # BR(H -> A       A      )
     3.41088767E-13    2          23        36   # BR(H -> Z       A      )
     1.64845127E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.01081433E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.52329611E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.39201170E+00   # A decays
#          BR         NDA      ID1       ID2
     1.28910880E-01    2           5        -5   # BR(A -> b       bb     )
     2.00997756E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.10523060E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.01983682E-04    2           3        -3   # BR(A -> s       sb     )
     8.53768149E-06    2           4        -4   # BR(A -> c       cb     )
     8.32426925E-01    2           6        -6   # BR(A -> t       tb     )
     1.60964670E-03    2          21        21   # BR(A -> g       g      )
     5.34042587E-06    2          22        22   # BR(A -> gam     gam    )
     1.88817720E-06    2          23        22   # BR(A -> Z       gam    )
     3.60517208E-03    2          23        25   # BR(A -> Z       h      )
     2.03102613E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.11277728E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.34623563E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.06179183E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.05706071E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.27166827E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31944900E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96462916E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.10458703E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.51697976E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.74145212E-03    2          24        25   # BR(H+ -> W+      h      )
     4.65942138E-10    2          24        36   # BR(H+ -> W+      A      )
     1.64730259E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.12129982E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
