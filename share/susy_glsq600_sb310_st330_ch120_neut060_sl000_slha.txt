#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.40483848E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.04838479E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.70000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.90000000E+02   # M_q1L               
        42     5.90000000E+02   # M_q2L               
        43     2.85000000E+02   # M_q3L               
        44     5.90000000E+02   # M_uR                
        45     5.90000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.90000000E+02   # M_dR                
        48     5.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04909710E+01   # W+
        25     9.90231579E+01   # h
        35     1.00105660E+03   # H
        36     1.00000000E+03   # A
        37     1.00417231E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     6.07138369E+02   # ~d_L
   2000001     6.05015121E+02   # ~d_R
   1000002     6.02389471E+02   # ~u_L
   2000002     6.03537491E+02   # ~u_R
   1000003     6.07138369E+02   # ~s_L
   2000003     6.05015121E+02   # ~s_R
   1000004     6.02389471E+02   # ~c_L
   2000004     6.03537491E+02   # ~c_R
   1000005     3.10080206E+02   # ~b_1
   2000005     1.00789409E+03   # ~b_2
   1000006     3.31233231E+02   # ~t_1
   2000006     7.02179849E+02   # ~t_2
   1000011     5.00019931E+03   # ~e_L
   2000011     5.00017982E+03   # ~e_R
   1000012     4.99962084E+03   # ~nu_eL
   1000013     5.00019931E+03   # ~mu_L
   2000013     5.00017982E+03   # ~mu_R
   1000014     4.99962084E+03   # ~nu_muL
   1000015     4.99932982E+03   # ~tau_1
   2000015     5.00104979E+03   # ~tau_2
   1000016     4.99962084E+03   # ~nu_tauL
   1000021     5.99284012E+02   # ~g
   1000022     5.98692650E+01   # ~chi_10
   1000023     1.20059754E+02   # ~chi_20
   1000025    -9.95983775E+02   # ~chi_30
   1000035     1.00017139E+03   # ~chi_40
   1000024     1.20005925E+02   # ~chi_1+
   1000037     1.00042551E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98500010E-01   # N_11
  1  2    -2.65098524E-02   # N_12
  1  3     4.63415106E-02   # N_13
  1  4    -1.21417362E-02   # N_14
  2  1     3.04477230E-02   # N_21
  2  2     9.96038274E-01   # N_22
  2  3    -7.96230651E-02   # N_23
  2  4     2.53152347E-02   # N_24
  3  1     2.30847371E-02   # N_31
  3  2    -3.90912180E-02   # N_32
  3  3    -7.05153922E-01   # N_33
  3  4    -7.07599404E-01   # N_34
  4  1     3.92142923E-02   # N_41
  4  2    -7.53449522E-02   # N_42
  4  3    -7.03043795E-01   # N_43
  4  4     7.06055805E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93449458E-01   # U_11
  1  2     1.14272368E-01   # U_12
  2  1     1.14272368E-01   # U_21
  2  2     9.93449458E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99335254E-01   # V_11
  1  2     3.64561477E-02   # V_12
  2  1     3.64561477E-02   # V_21
  2  2     9.99335254E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96591971E-01   # cos(theta_t)
  1  2     8.24890498E-02   # sin(theta_t)
  2  1    -8.24890498E-02   # -sin(theta_t)
  2  2     9.96591971E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99914228E-01   # cos(theta_b)
  1  2     1.30971998E-02   # sin(theta_b)
  2  1    -1.30971998E-02   # -sin(theta_b)
  2  2     9.99914228E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03089617E-01   # cos(theta_tau)
  1  2     7.11101252E-01   # sin(theta_tau)
  2  1    -7.11101252E-01   # -sin(theta_tau)
  2  2     7.03089617E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.04970064E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.04838479E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.87820648E+00   # tanbeta(Q)          
         3     2.46621766E+02   # vev(Q)              
         4     1.01425003E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.04838479E+02  # The gauge couplings
     1     3.58679494E-01   # gprime(Q) DRbar
     2     6.43301650E-01   # g(Q) DRbar
     3     1.09934522E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.04838479E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  4.04838479E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  4.04838479E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.04838479E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.99560537E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.04838479E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.02609849E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.04838479E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03411443E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.04838479E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     5.70000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.98806032E+04   # M^2_Hd              
        22    -9.54462923E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.90000000E+02   # M_q1L               
        42     5.90000000E+02   # M_q2L               
        43     2.85000000E+02   # M_q3L               
        44     5.90000000E+02   # M_uR                
        45     5.90000000E+02   # M_cR                
        46     5.00000000E+02   # M_tR                
        47     5.90000000E+02   # M_dR                
        48     5.90000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43244329E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.14798632E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.01218694E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.01218694E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.98781306E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.98781306E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.49102394E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04161786E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.50971489E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.38612333E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.38201700E+00   # stop2 decays
#          BR         NDA      ID1       ID2
     5.51142620E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.11944053E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.21146396E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46634548E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.06957394E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.89934533E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.36713240E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.93400666E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.65643748E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.95016185E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     3.18040599E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.70118248E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.59534011E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.92493535E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.78162833E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.94944767E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.77108794E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.29208196E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.83089095E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.73479541E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.38896163E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     6.69461031E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     8.78948139E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31117340E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59157917E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.35261212E-04    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.29609237E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     9.90096910E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.74747856E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.02834253E-03    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.71862027E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.56651252E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.24797113E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.53669052E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.86871065E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.42845559E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.37509894E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.28524808E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.16615810E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.69461031E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.78948139E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31117340E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59157917E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.35261212E-04    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.29609237E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.90096910E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.74747856E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.02834253E-03    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.71862027E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.56651252E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.24797113E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.53669052E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.86871065E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.42845559E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.37509894E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.28524808E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.16615810E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.80282906E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.50364960E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10140039E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.91836300E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.97540986E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96552028E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.28205969E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55837285E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97164107E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.26411097E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91677130E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.41780482E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.80282906E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.50364960E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10140039E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.91836300E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.97540986E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96552028E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.28205969E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55837285E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97164107E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.26411097E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91677130E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.41780482E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.69544609E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.36906531E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.26149443E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09556597E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.51970923E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34989138E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.07352257E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.73552885E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.26845281E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.21863157E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.33742290E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.41457620E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25736998E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.48025653E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.80451214E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02856198E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89593473E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.53137467E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.63396280E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03422350E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.40878969E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.80451214E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02856198E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89593473E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.53137467E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.63396280E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03422350E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.40878969E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.82776689E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02505879E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88607144E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.50572349E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.62499176E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01414965E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.09644785E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.20317604E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.37274179E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.37274179E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08644255E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08644255E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08163131E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.60511093E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.92341254E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.85872055E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.92341254E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.85872055E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     5.56610599E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.05073767E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.85654986E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10972414E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.62096471E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.21684651E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.01276644E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.08237055E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.38033823E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     6.70315089E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.94730964E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     6.70315089E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.94730964E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.15321525E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     5.44868755E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     5.44868755E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     5.70364273E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.08959694E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.08959694E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.08959694E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.93421332E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.93421332E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.93421332E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.93421332E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.27009088E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.27009088E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.27009088E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.27009088E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.46639926E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.55312513E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.82359801E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.24423054E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.24423054E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.26560682E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.28369430E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     9.33560408E-05    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     9.33560408E-05    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     2.26187589E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.26187589E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.42853730E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.42853730E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     5.62772783E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.62772783E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     9.33560408E-05    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     9.33560408E-05    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     2.26187589E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.26187589E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.42853730E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.42853730E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     5.62772783E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.62772783E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.89382059E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.89382059E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.04561224E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.04561224E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     2.17066809E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.17066809E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.54181792E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.24090455E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.28169702E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.14345621E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.14345621E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49917236E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.66365612E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.53779721E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.53779721E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     6.46777025E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     6.46777025E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.13689075E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.13689075E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.60933144E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.60933144E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.53779721E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.53779721E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     6.46777025E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     6.46777025E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.13689075E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.13689075E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.60933144E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.60933144E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.09123716E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.09123716E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     8.48767607E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     8.48767607E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.89603137E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.89603137E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.74518888E-03   # h decays
#          BR         NDA      ID1       ID2
     8.26839792E-01    2           5        -5   # BR(h -> b       bb     )
     7.66575884E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.71503981E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.02514062E-04    2           3        -3   # BR(h -> s       sb     )
     2.55272733E-02    2           4        -4   # BR(h -> c       cb     )
     5.99289784E-02    2          21        21   # BR(h -> g       g      )
     1.38113680E-03    2          22        22   # BR(h -> gam     gam    )
     3.14354556E-05    2          22        23   # BR(h -> Z       gam    )
     7.88356380E-03    2          24       -24   # BR(h -> W+      W-     )
     8.76213858E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.43856336E+00   # H decays
#          BR         NDA      ID1       ID2
     8.74012887E-02    2           5        -5   # BR(H -> b       bb     )
     1.43429442E-02    2         -15        15   # BR(H -> tau+    tau-   )
     5.07026580E-05    2         -13        13   # BR(H -> mu+     mu-    )
     7.25592113E-05    2           3        -3   # BR(H -> s       sb     )
     5.89912991E-06    2           4        -4   # BR(H -> c       cb     )
     5.27928960E-01    2           6        -6   # BR(H -> t       tb     )
     1.49991559E-03    2          21        21   # BR(H -> g       g      )
     7.81573865E-06    2          22        22   # BR(H -> gam     gam    )
     8.10166018E-07    2          23        22   # BR(H -> Z       gam    )
     7.10644405E-04    2          24       -24   # BR(H -> W+      W-     )
     3.51438765E-04    2          23        23   # BR(H -> Z       Z      )
     3.33971526E-03    2          25        25   # BR(H -> h       h      )
     2.09775488E-22    2          36        36   # BR(H -> A       A      )
     4.12965152E-13    2          23        36   # BR(H -> Z       A      )
     2.47083941E-02    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.38887991E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.15273484E-02    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.03836825E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.17261973E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.36234312E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.51193264E+00   # A decays
#          BR         NDA      ID1       ID2
     1.19828741E-01    2           5        -5   # BR(A -> b       bb     )
     1.96359666E-02    2         -15        15   # BR(A -> tau+    tau-   )
     6.94127506E-05    2         -13        13   # BR(A -> mu+     mu-    )
     9.96260775E-05    2           3        -3   # BR(A -> s       sb     )
     7.92482162E-06    2           4        -4   # BR(A -> c       cb     )
     7.72672873E-01    2           6        -6   # BR(A -> t       tb     )
     1.49907135E-03    2          21        21   # BR(A -> g       g      )
     2.21594611E-06    2          22        22   # BR(A -> gam     gam    )
     1.65681070E-06    2          23        22   # BR(A -> Z       gam    )
     9.53771113E-04    2          23        25   # BR(A -> Z       h      )
     4.66540910E-02    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.39560039E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.17812488E-02    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.43978003E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.76072565E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.60231994E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.79409439E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.34208756E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.02540438E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.32975862E-06    2           2        -3   # BR(H+ -> u       sb     )
     9.59703738E-05    2           4        -3   # BR(H+ -> c       sb     )
     7.82121888E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.84135830E-04    2          24        25   # BR(H+ -> W+      h      )
     6.00034134E-10    2          24        36   # BR(H+ -> W+      A      )
     2.17108456E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.04232352E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.76996784E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
