###############################################################
#
# Job options file
# Christoph Wasicki
#
#for mcatnlo input card see job options 106201 to 106208
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

#-- Dll's and Algorithms
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

# ... Main generator : Herwig
try:
    if runArgs.ecmEnergy == 7000.0:
        include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_7TeV.py" ) 
    if runArgs.ecmEnergy == 10000.0:
        include ( "MC09JobOptions/MC9_McAtNloJimmy_Common.py" )
except NameError:
    # needed (dummy) default for first scan of jo 
    from Herwig_i.Herwig_iConf import Herwig
    topAlg += Herwig()
    Herwig = topAlg.Herwig

# ... Main generator : Herwig
Herwig.HerwigCommand += [ "taudec TAUOLA" ]
#... New mass
Herwig.HerwigCommand.append( 'rmass 6 167.5' )

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
topAlg += TTbarWToLeptonFilter()

TTbarWToLeptonFilter = topAlg.TTbarWToLeptonFilter
TTbarWToLeptonFilter.Ptcut = 1.

try:
    StreamEVGEN.VetoAlgs +=  [ "TTbarWToLeptonFilter" ]
except Exception, e:
    pass


from MC09JobOptions.McAtNloEvgenConfig import evgenConfig

evgenConfig.inputfilebase = 'mcatnlo'
try:
    if runArgs.ecmEnergy == 7000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106225.ttbar_fullhad_7TeV.TXT.v1'
    if runArgs.ecmEnergy == 10000.0:
        evgenConfig.inputfilebase = 'group09.phys-gener.mcatnlo341.106225.ttbar_fullhad_10TeV.TXT.v1'
except NameError:
    pass

evgenConfig.efficiency = 0.4
