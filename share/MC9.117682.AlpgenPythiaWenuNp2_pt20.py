###############################################################
#
# Job options file
# James Ferrando
#
#==============================================================
#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

#--------------------------------------------------------------
# Generator
#--------------------------------------------------------------

# ... Main generator : Pythia
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 
 
include ( "MC09JobOptions/MC9_PythiaMC09p_Common.py" )

Pythia.PythiaCommand += ["pyinit user alpgen",
"pydat1 parj 90 20000.", # Turn off FSR.
"pydat3 mdcy 15 1 0", # Turn off tau decays.
"pypars mstp 143 1" # matching
]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from MC09JobOptions.AlpgenPythiaEvgenConfig import evgenConfig
# input file names need updating for MC9
evgenConfig.inputfilebase = 'alpgen'
try:
  if runArgs.ecmEnergy == 7000.0:
    evgenConfig.inputfilebase = 'group09.phys-gener.alpgen.107682.WenuNp2_pt20_7tev.TXT.v1'    
  if runArgs.ecmEnergy == 10000.0:
    evgenConfig.inputfilebase = 'alpgen.107682.WenuNp2.pt20.v2'    
except NameError:
  pass


evgenConfig.efficiency = 0.98000
evgenConfig.minevents = 250

#==============================================================
#
# End of job options file
#
###############################################################
