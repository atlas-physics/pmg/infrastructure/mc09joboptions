###############################################################
#
# Job options file
# WW -> any production with MC@NLO 3.10 and Herwig/Herwig
#
# Author : Martin Schmitz mschmitz@cern.ch
#


# ... Main generator : Herwig
from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 


include ( "MC09JobOptions/MC9_McAtNloJimmy_Common_14TeV.py" )
Herwig.HerwigCommand += [ "taudec TAUOLA"]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )



from MC09JobOptions.McAtNloEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'mcatnlo0310.206860.WpWm_any'
evgenConfig.efficiency = 0.9
#==============================================================
#
# End of job options file
#
###############################################################
