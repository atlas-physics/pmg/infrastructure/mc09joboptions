#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.34170401E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.41704012E+02   # EWSB                
         1     1.01000000E+02   # M_1                 
         2     2.02000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     8.00000000E+02   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+01   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     7.50000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     1.60000000E+02   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05356142E+01   # W+
        25     1.20503008E+02   # h
        35     1.00060865E+03   # H
        36     1.00000000E+03   # A
        37     1.00396753E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03210719E+03   # ~d_L
   2000001     5.03182876E+03   # ~d_R
   1000002     5.03148530E+03   # ~u_L
   2000002     5.03163372E+03   # ~u_R
   1000003     5.03210719E+03   # ~s_L
   2000003     5.03182876E+03   # ~s_R
   1000004     5.03148530E+03   # ~c_L
   2000004     5.03163372E+03   # ~c_R
   1000005     5.00028490E+03   # ~b_1
   2000005     5.00387403E+03   # ~b_2
   1000006     2.50208095E+02   # ~t_1
   2000006     7.75959825E+02   # ~t_2
   1000011     5.00021105E+03   # ~e_L
   2000011     5.00019286E+03   # ~e_R
   1000012     4.99959607E+03   # ~nu_eL
   1000013     5.00021105E+03   # ~mu_L
   2000013     5.00019286E+03   # ~mu_R
   1000014     4.99959607E+03   # ~nu_muL
   1000015     4.99144478E+03   # ~tau_1
   2000015     5.00894444E+03   # ~tau_2
   1000016     4.99959607E+03   # ~nu_tauL
   1000021     1.12848467E+03   # ~g
   1000022     1.00722055E+02   # ~chi_10
   1000023     2.00461859E+02   # ~chi_20
   1000025    -1.00329397E+03   # ~chi_30
   1000035     1.00511006E+03   # ~chi_40
   1000024     2.00458475E+02   # ~chi_1+
   1000037     1.00644416E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98968916E-01   # N_11
  1  2    -4.89548949E-03   # N_12
  1  3     4.48090742E-02   # N_13
  1  4    -5.41167436E-03   # N_14
  2  1     8.62999949E-03   # N_21
  2  2     9.96486236E-01   # N_22
  2  3    -8.13674412E-02   # N_23
  2  4     1.78897693E-02   # N_24
  3  1     2.75661016E-02   # N_31
  3  2    -4.50990268E-02   # N_32
  3  3    -7.04908133E-01   # N_33
  3  4    -7.07326453E-01   # N_34
  4  1     3.50248244E-02   # N_41
  4  2    -7.04080478E-02   # N_42
  4  3    -7.03189882E-01   # N_43
  4  4     7.06639907E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93334334E-01   # U_11
  1  2     1.15268817E-01   # U_12
  2  1     1.15268817E-01   # U_21
  2  2     9.93334334E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99678452E-01   # V_11
  1  2     2.53572993E-02   # V_12
  2  1     2.53572993E-02   # V_21
  2  2     9.99678452E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -2.60128808E-01   # cos(theta_t)
  1  2     9.65573924E-01   # sin(theta_t)
  2  1    -9.65573924E-01   # -sin(theta_t)
  2  2    -2.60128808E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.85945590E-01   # cos(theta_b)
  1  2     1.67066734E-01   # sin(theta_b)
  2  1    -1.67066734E-01   # -sin(theta_b)
  2  2     9.85945590E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06739257E-01   # cos(theta_tau)
  1  2     7.07474114E-01   # sin(theta_tau)
  2  1    -7.07474114E-01   # -sin(theta_tau)
  2  2     7.06739257E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.23269711E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.41704012E+02  # DRbar Higgs Parameters
         1     1.14788746E+03   # mu(Q)               
         2     4.92691361E+01   # tanbeta(Q)          
         3     2.45748459E+02   # vev(Q)              
         4     7.03999447E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.41704012E+02  # The gauge couplings
     1     3.57555484E-01   # gprime(Q) DRbar
     2     6.38474481E-01   # g(Q) DRbar
     3     1.07084875E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.41704012E+02  # The trilinear couplings
  1  1     3.14656078E+03   # A_u(Q) DRbar
  2  2     3.14656078E+03   # A_c(Q) DRbar
  3  3     5.86830707E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.41704012E+02  # The trilinear couplings
  1  1     1.30449178E+03   # A_d(Q) DRbar
  2  2     1.30449178E+03   # A_s(Q) DRbar
  3  3     2.01744351E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  3.41704012E+02  # The trilinear couplings
  1  1     2.67334257E+02   # A_e(Q) DRbar
  2  2     2.67334257E+02   # A_mu(Q) DRbar
  3  3     2.99621128E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.41704012E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.06837874E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.41704012E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.36555198E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.41704012E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03651275E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.41704012E+02  # The soft SUSY breaking masses at the scale Q
         1     2.62636733E+02   # M_1                 
         2     2.81807743E+02   # M_2                 
         3     4.42529151E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     5.99741644E+06   # M^2_Hd              
        22     8.56864371E+06   # M^2_Hu              
        31     5.01334130E+03   # M_eL                
        32     5.01334130E+03   # M_muL               
        33     5.54747536E+03   # M_tauL              
        34     5.01644392E+03   # M_eR                
        35     5.01644392E+03   # M_muR               
        36     6.04453805E+03   # M_tauR              
        41     5.01741868E+03   # M_q1L               
        42     5.01741868E+03   # M_q2L               
        43     1.98208333E+03   # M_q3L               
        44     4.99710692E+03   # M_uR                
        45     4.99710692E+03   # M_cR                
        46     2.47390436E+03   # M_tR                
        47     5.00296203E+03   # M_dR                
        48     5.00296203E+03   # M_sR                
        49     1.17745439E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40906637E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.84524353E+01   # gluino decays
#          BR         NDA      ID1       ID2
     1.02363710E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     1.02363710E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.39400032E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     1.39400032E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.27484046E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.27484046E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     5.62122413E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     5.62122413E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     2.00576271E-03   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     2.66352329E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.28283579E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.65314808E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.94847973E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.47480483E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.35532883E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     1.66254590E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.35666155E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.52613945E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.66617821E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.72411572E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.16808447E+00   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60520844E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.67361189E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56922001E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.55384521E-01    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42990744E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.10259840E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.06478239E-01    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     3.16681149E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.05487322E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.19782202E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.28230404E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.37152437E-04    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.24349518E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.47454550E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.11212667E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.67907287E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.09885524E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.05381573E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.91227752E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.70020431E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58932269E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.16690866E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28265793E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.15845065E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.46819497E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.44237246E-04    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.22787775E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.54472428E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.11309280E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.59667704E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.05727044E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.87710071E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.51205450E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.21239104E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89406872E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.16681149E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.05487322E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.19782202E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.28230404E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.37152437E-04    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.24349518E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.47454550E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.11212667E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.67907287E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.09885524E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.05381573E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.91227752E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.70020431E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58932269E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.16690866E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28265793E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.15845065E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.46819497E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.44237246E-04    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.22787775E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.54472428E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.11309280E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.59667704E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.05727044E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.87710071E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.51205450E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.21239104E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89406872E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.69386713E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.30712362E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02723664E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.45467483E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.19600232E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95829837E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.41019509E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.54105232E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.98094381E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.43095331E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.00611574E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.13069766E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.69386713E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.30712362E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02723664E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.45467483E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.19600232E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95829837E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.41019509E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.54105232E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.98094381E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.43095331E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.00611574E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.13069766E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     8.13592125E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.01569381E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.48559982E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.30986142E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.25209398E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.91271326E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.02403771E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     8.07583879E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89352634E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.05321430E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54521197E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59419825E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.04408859E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.86976056E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.69571885E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.63482471E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96788558E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.02201388E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.25939726E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03223333E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.58450673E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.69571885E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.63482471E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96788558E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.02201388E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.25939726E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03223333E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.58450673E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     9.02097410E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.15134272E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.20288044E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.58578570E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.67701278E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.51437676E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.54325261E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.92622997E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.64796654E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.30437243E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.77408849E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.53120794E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.42581454E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.64104732E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.38086388E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.42238206E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.27233005E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     3.75185613E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04344359E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.69906839E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.18942633E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.18942633E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.41118051E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.51186185E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65151096E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65151096E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.97645606E-02    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     2.97645606E-02    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     1.87126207E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.87126207E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.38015102E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.18932402E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.71015604E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.78285277E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.78285277E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16477381E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.13937425E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05325421E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.05325421E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.73607567E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     7.73607567E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.08191118E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.08191118E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     5.97661502E-03   # h decays
#          BR         NDA      ID1       ID2
     8.36545090E-01    2           5        -5   # BR(h -> b       bb     )
     5.05166482E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.78806672E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.80769911E-04    2           3        -3   # BR(h -> s       sb     )
     1.37319753E-02    2           4        -4   # BR(h -> c       cb     )
     3.03055688E-03    2          21        21   # BR(h -> g       g      )
     2.46545396E-03    2          22        22   # BR(h -> gam     gam    )
     6.85464674E-04    2          22        23   # BR(h -> Z       gam    )
     8.27379778E-02    2          24       -24   # BR(h -> W+      W-     )
     9.72725615E-03    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.52742180E+01   # H decays
#          BR         NDA      ID1       ID2
     3.27830093E-01    2           5        -5   # BR(H -> b       bb     )
     1.42709720E-01    2         -15        15   # BR(H -> tau+    tau-   )
     5.04482355E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.22787501E-04    2           3        -3   # BR(H -> s       sb     )
     6.63959523E-09    2           4        -4   # BR(H -> c       cb     )
     5.94120011E-04    2           6        -6   # BR(H -> t       tb     )
     3.96033711E-04    2          21        21   # BR(H -> g       g      )
     6.83607098E-07    2          22        22   # BR(H -> gam     gam    )
     3.62979982E-08    2          23        22   # BR(H -> Z       gam    )
     3.70554515E-05    2          24       -24   # BR(H -> W+      W-     )
     1.83258389E-05    2          23        23   # BR(H -> Z       Z      )
     4.66405965E-06    2          25        25   # BR(H -> h       h      )
    -1.62828484E-23    2          36        36   # BR(H -> A       A      )
     2.55749466E-15    2          23        36   # BR(H -> Z       A      )
     2.44123872E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.29863965E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19750370E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     8.00586006E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.03507653E-01    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.09552573E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.09552573E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     2.66328354E+01   # A decays
#          BR         NDA      ID1       ID2
     4.34206666E-01    2           5        -5   # BR(A -> b       bb     )
     1.88917594E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.67819928E-04    2         -13        13   # BR(A -> mu+     mu-    )
     9.56921600E-04    2           3        -3   # BR(A -> s       sb     )
     7.32741082E-09    2           4        -4   # BR(A -> c       cb     )
     7.14425111E-04    2           6        -6   # BR(A -> t       tb     )
     1.53518677E-04    2          21        21   # BR(A -> g       g      )
     3.62013202E-08    2          22        22   # BR(A -> gam     gam    )
     6.57614603E-08    2          23        22   # BR(A -> Z       gam    )
     4.74843581E-05    2          23        25   # BR(A -> Z       h      )
     3.92163984E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.81022204E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.92390840E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.17053644E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.83569177E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.83569177E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     2.83018705E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.63177878E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.78481974E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     6.30930187E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.04432860E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.30750394E-05    2           2        -3   # BR(H+ -> u       sb     )
     8.85954728E-04    2           4        -3   # BR(H+ -> c       sb     )
     1.00548630E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.55036702E-05    2          24        25   # BR(H+ -> W+      h      )
     4.55166558E-11    2          24        36   # BR(H+ -> W+      A      )
     2.07795799E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.50838068E-07    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     7.17121602E-01    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
