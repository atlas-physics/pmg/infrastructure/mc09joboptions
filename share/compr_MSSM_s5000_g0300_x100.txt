#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018765E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00187648E+03   # EWSB                
         1     1.04000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.48000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05888974E+01   # W+
        25     8.55063161E+01   # h
        35     1.00939492E+03   # H
        36     1.00000000E+03   # A
        37     1.00336191E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.04473786E+03   # ~d_L
   2000001     5.04475339E+03   # ~d_R
   1000002     5.04477279E+03   # ~u_L
   2000002     5.04476499E+03   # ~u_R
   1000003     5.04473786E+03   # ~s_L
   2000003     5.04475339E+03   # ~s_R
   1000004     5.04477279E+03   # ~c_L
   2000004     5.04476499E+03   # ~c_R
   1000005     5.04452306E+03   # ~b_1
   2000005     5.04496929E+03   # ~b_2
   1000006     4.91182463E+03   # ~t_1
   2000006     5.04453912E+03   # ~t_2
   1000011     4.99998844E+03   # ~e_L
   2000011     4.99998850E+03   # ~e_R
   1000012     5.00002306E+03   # ~nu_eL
   1000013     4.99998844E+03   # ~mu_L
   2000013     4.99998850E+03   # ~mu_R
   1000014     5.00002306E+03   # ~nu_muL
   1000015     4.99982413E+03   # ~tau_1
   2000015     5.00015341E+03   # ~tau_2
   1000016     5.00002306E+03   # ~nu_tauL
   1000021     3.00343154E+02   # ~g
   1000022     1.00219533E+02   # ~chi_10
   1000023    -1.01770545E+03   # ~chi_20
   1000025     1.01838021E+03   # ~chi_30
   1000035     5.02220024E+03   # ~chi_40
   1000024     1.01620443E+03   # ~chi_1+
   1000037     5.02220009E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98813312E-01   # N_11
  1  2    -7.66175453E-04   # N_12
  1  3     3.36097794E-02   # N_13
  1  4    -3.52386626E-02   # N_14
  2  1     1.15095972E-03   # N_21
  2  2    -3.70452249E-04   # N_22
  2  3     7.07134094E-01   # N_23
  2  4     7.07078434E-01   # N_24
  3  1     4.86889312E-02   # N_31
  3  2     1.93350144E-02   # N_32
  3  3    -7.06142572E-01   # N_33
  3  4     7.06129034E-01   # N_34
  4  1     1.75741490E-04   # N_41
  4  2    -9.99812699E-01   # N_42
  4  3    -1.39435989E-02   # N_43
  4  4     1.34205889E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.97173268E-02   # U_11
  1  2     9.99805595E-01   # U_12
  2  1     9.99805595E-01   # U_21
  2  2     1.97173268E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.89775703E-02   # V_11
  1  2     9.99819910E-01   # V_12
  2  1     9.99819910E-01   # V_21
  2  2     1.89775703E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.02273316E-01   # cos(theta_t)
  1  2     9.94756336E-01   # sin(theta_t)
  2  1    -9.94756336E-01   # -sin(theta_t)
  2  2     1.02273316E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19306949E-01   # cos(theta_b)
  1  2     6.94692387E-01   # sin(theta_b)
  2  1    -6.94692387E-01   # -sin(theta_b)
  2  2     7.19306949E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07173153E-01   # cos(theta_tau)
  1  2     7.07040403E-01   # sin(theta_tau)
  2  1    -7.07040403E-01   # -sin(theta_tau)
  2  2     7.07173153E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.21644564E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00187648E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.43960179E-01   # tanbeta(Q)          
         3     2.44210894E+02   # vev(Q)              
         4     1.10237862E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00187648E+03  # The gauge couplings
     1     3.65898634E-01   # gprime(Q) DRbar
     2     6.34889655E-01   # g(Q) DRbar
     3     1.03103895E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00187648E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00187648E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00187648E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00187648E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.16532083E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00187648E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.86455071E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00187648E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38895640E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00187648E+03  # The soft SUSY breaking masses at the scale Q
         1     1.04000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     1.48000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.03071692E+05   # M^2_Hd              
        22     6.66867730E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39097832E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.70416708E-09   # gluino decays
#          BR         NDA      ID1       ID2
     1.92400654E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.00289358E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.40275266E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.00289358E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.40275266E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.96306864E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     6.01551629E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.91260242E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.02468887E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.01619638E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.02947669E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.73837782E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.82138988E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.63079171E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.31279086E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.31696952E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.43773182E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.30681538E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.32775546E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.21837912E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.07474539E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.21494440E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.24802105E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.99294152E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.67176936E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.16137553E-06    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.55557255E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.40037696E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     7.82192946E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.17328893E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.38539822E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.76257415E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.00921823E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.12907672E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.47048259E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.48391629E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.48670371E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.54856378E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.02156876E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.14782630E-09    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.23506297E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.15542632E-05    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.70766599E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.31159529E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.97774333E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.65661667E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.16414410E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.90647762E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.99005996E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.35470101E-13    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.68288619E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.54853204E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.05412105E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.80088606E-08    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.12676811E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.14887308E-05    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.00235741E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.29712932E-05    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.97776251E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.56964107E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.10308039E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.00041243E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.79008907E-05    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.88239844E-13    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.91879009E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.54856378E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.02156876E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.14782630E-09    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.23506297E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.15542632E-05    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.70766599E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.31159529E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.97774333E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.65661667E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.16414410E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.90647762E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.99005996E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.35470101E-13    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.68288619E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.54853204E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.05412105E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.80088606E-08    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.12676811E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.14887308E-05    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.00235741E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.29712932E-05    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.97776251E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.56964107E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.10308039E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.00041243E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.79008907E-05    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.88239844E-13    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.91879009E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.67563423E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.91655743E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.36680619E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.19787928E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.14614100E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.66083983E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97818608E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21843022E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.18017370E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     6.67563423E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.91655743E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.36680619E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.19787928E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.14614100E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.66083983E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97818608E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21843022E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.18017370E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.66910366E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.94242528E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.54298033E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.29374949E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.90942481E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.66458797E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.95764940E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.05625825E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.72427245E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.16206008E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.67001001E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97796998E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95148310E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.10234329E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.98981624E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     6.67001001E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97796998E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95148310E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.10234329E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.98981624E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     6.68763612E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95167179E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.94370409E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.09680230E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.62019712E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.43704414E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.51741994E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.00640901E-05    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.07933408E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     2.07933408E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07253358E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.08575700E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.08575700E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.05761586E-05    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.02817244E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.78629447E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.81216688E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71695023E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.71567164E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.85179828E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.31570227E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.31572487E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.67830941E-04    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.31138211E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     7.30164486E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.76068896E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99085110E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.14890407E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.95891304E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.49718705E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.99450281E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     7.51740702E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.91338965E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.78599762E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.51751906E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.71661362E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.71661362E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.95246115E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.53055446E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.66927125E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.07244886E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.28496894E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.43042547E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.84971904E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.25543048E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     7.28352999E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.31957958E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.31957958E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.04269785E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.04269785E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.28095347E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.28095347E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.04269785E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.04269785E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.28095347E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.28095347E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.27034345E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.27034345E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.12279842E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     5.12279842E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.03987848E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.03987848E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.03987848E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.03987848E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.03987848E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.03987848E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     2.21036458E-03   # h decays
#          BR         NDA      ID1       ID2
     8.46669293E-01    2           5        -5   # BR(h -> b       bb     )
     8.10950987E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.87409674E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.57434756E-04    2           3        -3   # BR(h -> s       sb     )
     2.78132912E-02    2           4        -4   # BR(h -> c       cb     )
     4.11129158E-02    2          21        21   # BR(h -> g       g      )
     1.07164811E-03    2          22        22   # BR(h -> gam     gam    )
     1.03168983E-03    2          24       -24   # BR(h -> W+      W-     )
     2.61219208E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84827828E+01   # H decays
#          BR         NDA      ID1       ID2
     2.48586136E-04    2           5        -5   # BR(H -> b       bb     )
     3.78469311E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.33789784E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.85799884E-07    2           3        -3   # BR(H -> s       sb     )
     1.11071902E-05    2           4        -4   # BR(H -> c       cb     )
     9.96320664E-01    2           6        -6   # BR(H -> t       tb     )
     1.42496759E-03    2          21        21   # BR(H -> g       g      )
     4.73530785E-06    2          22        22   # BR(H -> gam     gam    )
     1.68671836E-06    2          23        22   # BR(H -> Z       gam    )
     3.69541551E-04    2          24       -24   # BR(H -> W+      W-     )
     1.82801345E-04    2          23        23   # BR(H -> Z       Z      )
     1.39734291E-03    2          25        25   # BR(H -> h       h      )
     1.01748369E-18    2          36        36   # BR(H -> A       A      )
     1.62081815E-09    2          23        36   # BR(H -> Z       A      )
     4.31460096E-10    2          24       -37   # BR(H -> W+      H-     )
     4.31460096E-10    2         -24        37   # BR(H -> W-      H+     )
     3.98273021E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        36     5.19610486E+01   # A decays
#          BR         NDA      ID1       ID2
     2.40614623E-04    2           5        -5   # BR(A -> b       bb     )
     3.55442502E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.25648216E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.88059927E-07    2           3        -3   # BR(A -> s       sb     )
     1.02313416E-05    2           4        -4   # BR(A -> c       cb     )
     9.97559379E-01    2           6        -6   # BR(A -> t       tb     )
     1.70727018E-03    2          21        21   # BR(A -> g       g      )
     5.53904316E-06    2          22        22   # BR(A -> gam     gam    )
     2.07440157E-06    2          23        22   # BR(A -> Z       gam    )
     3.32134426E-04    2          23        25   # BR(A -> Z       h      )
     1.06898859E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04545401E+01   # H+ decays
#          BR         NDA      ID1       ID2
     3.99215554E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.67286225E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.29834943E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45150805E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.86740388E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02843912E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99604885E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.47561540E-04    2          24        25   # BR(H+ -> W+      h      )
     1.11592868E-11    2          24        36   # BR(H+ -> W+      A      )
