# ttbar with a pT min = 150 GeV
# Author: Giacinto Piacquadio (18/5/2008)
#

from AthenaCommon.AlgSequence import AlgSequence 
topAlg = AlgSequence("TopAlg") 

include ( "MC09JobOptions/MC9_Herwig_Common_14TeV.py" )

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------
Herwig = topAlg.Herwig
Herwig.HerwigCommand = ["iproc 11706"]
Herwig.HerwigCommand += ["ptmin 150."]

# ... Tauola
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )

# ... Photos
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
MissingEtFilter= MissingEtFilter(name = "MissingEtFilter",
                                 MEtcut = 100.*GeV)

topAlg += MissingEtFilter

try:
     StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]
except Exception, e:
     pass

#---------------------------------------------------------------
#==============================================================
#
# End of job options file
#
###############################################################
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
#21.5+-0.2%
evgenConfig.efficiency = 0.205
