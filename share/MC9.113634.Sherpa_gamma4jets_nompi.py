from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence('TopAlg')
from Sherpa_i.Sherpa_iConf import Sherpa_i
sherpa = Sherpa_i()

# example for how to overwrite/specify arbitrary Sherpa parameters
# every Sherpa setting of the type parameter=value can be entered on 
# these lines and will be treated in the same way as on the Sherpa
# command line (=> spaces are not allowed)
params="""
MI_HANDLER=None
MASSIVE[15]=1
"""
sherpa.Parameters = params.strip().splitlines()

topAlg += sherpa
from MC09JobOptions.SherpaFEvgenConfig import evgenConfig
evgenConfig.inputfilebase = 'group10.phys-gener.sherpa010202.113634.Sherpa_gamma4jets_nompi.TXT.v1'
evgenConfig.efficiency = 0.9
evgenConfig.weighting = 0
