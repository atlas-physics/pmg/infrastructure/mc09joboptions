#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.36495433E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     3.64954332E+02   # EWSB                
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.80000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.00000000E+02   # M_q1L               
        42     4.00000000E+02   # M_q2L               
        43     2.85000000E+02   # M_q3L               
        44     4.00000000E+02   # M_uR                
        45     4.00000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.00000000E+02   # M_dR                
        48     4.00000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04944398E+01   # W+
        25     9.79196860E+01   # h
        35     1.00070456E+03   # H
        36     1.00000000E+03   # A
        37     1.00414437E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00973845E+02   # ~d_L
   2000001     4.00877131E+02   # ~d_R
   1000002     4.00032084E+02   # ~u_L
   2000002     4.00721386E+02   # ~u_R
   1000003     4.00973845E+02   # ~s_L
   2000003     4.00877131E+02   # ~s_R
   1000004     4.00032084E+02   # ~c_L
   2000004     4.00721386E+02   # ~c_R
   1000005     3.10083100E+02   # ~b_1
   2000005     1.00849713E+03   # ~b_2
   1000006     3.30075938E+02   # ~t_1
   2000006     6.38921784E+02   # ~t_2
   1000011     5.00020030E+03   # ~e_L
   2000011     5.00017997E+03   # ~e_R
   1000012     4.99961971E+03   # ~nu_eL
   1000013     5.00020030E+03   # ~mu_L
   2000013     5.00017997E+03   # ~mu_R
   1000014     4.99961971E+03   # ~nu_muL
   1000015     4.99932887E+03   # ~tau_1
   2000015     5.00105188E+03   # ~tau_2
   1000016     4.99961971E+03   # ~nu_tauL
   1000021     4.00020369E+02   # ~g
   1000022     5.97713718E+01   # ~chi_10
   1000023     1.20051895E+02   # ~chi_20
   1000025    -9.96795818E+02   # ~chi_30
   1000035     1.00097921E+03   # ~chi_40
   1000024     1.20007452E+02   # ~chi_1+
   1000037     1.00124206E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98489089E-01   # N_11
  1  2    -2.69091794E-02   # N_12
  1  3     4.63511308E-02   # N_13
  1  4    -1.21246807E-02   # N_14
  2  1     3.08460933E-02   # N_21
  2  2     9.96027036E-01   # N_22
  2  3    -7.96410650E-02   # N_23
  2  4     2.52183117E-02   # N_24
  3  1     2.30855481E-02   # N_31
  3  2    -3.91821047E-02   # N_32
  3  3    -7.05148789E-01   # N_33
  3  4    -7.07599467E-01   # N_34
  4  1     3.91805455E-02   # N_41
  4  2    -7.53047321E-02   # N_42
  4  3    -7.03046271E-01   # N_43
  4  4     7.06059504E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.93443775E-01   # U_11
  1  2     1.14321762E-01   # U_12
  2  1     1.14321762E-01   # U_21
  2  2     9.93443775E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99340042E-01   # V_11
  1  2     3.63246464E-02   # V_12
  2  1     3.63246464E-02   # V_21
  2  2     9.99340042E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94273909E-01   # cos(theta_t)
  1  2     1.06861564E-01   # sin(theta_t)
  2  1    -1.06861564E-01   # -sin(theta_t)
  2  2     9.94273909E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99911806E-01   # cos(theta_b)
  1  2     1.32808216E-02   # sin(theta_b)
  2  1    -1.32808216E-02   # -sin(theta_b)
  2  2     9.99911806E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.02921814E-01   # cos(theta_tau)
  1  2     7.11267125E-01   # sin(theta_tau)
  2  1    -7.11267125E-01   # -sin(theta_tau)
  2  2     7.02921814E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.04411562E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.64954332E+02  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     4.88518694E+00   # tanbeta(Q)          
         3     2.46813380E+02   # vev(Q)              
         4     1.00709253E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  3.64954332E+02  # The gauge couplings
     1     3.58502642E-01   # gprime(Q) DRbar
     2     6.43907530E-01   # g(Q) DRbar
     3     1.11403909E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.64954332E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  3.64954332E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  3.64954332E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.64954332E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.05060936E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.64954332E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.11865362E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.64954332E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.03875245E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.64954332E+02  # The soft SUSY breaking masses at the scale Q
         1     6.00000000E+01   # M_1                 
         2     1.16000000E+02   # M_2                 
         3     3.80000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.34513151E+04   # M^2_Hd              
        22    -9.53309447E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.00000000E+02   # M_q1L               
        42     4.00000000E+02   # M_q2L               
        43     2.85000000E+02   # M_q3L               
        44     4.00000000E+02   # M_uR                
        45     4.00000000E+02   # M_cR                
        46     4.00000000E+02   # M_tR                
        47     4.00000000E+02   # M_dR                
        48     4.00000000E+02   # M_sR                
        49     1.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43498777E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.82104713E+00   # gluino decays
#          BR         NDA      ID1       ID2
     2.60072749E-04    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.60072749E-04    2    -1000002         2   # BR(~g -> ~u_L* u )
     2.89981776E-06    2     2000002        -2   # BR(~g -> ~u_R  ub)
     2.89981776E-06    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.60072749E-04    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.60072749E-04    2    -1000004         4   # BR(~g -> ~c_L* c )
     2.89981776E-06    2     2000004        -4   # BR(~g -> ~c_R  cb)
     2.89981776E-06    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.99474055E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.99474055E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     2.40221040E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.17612368E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.46383157E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.41855606E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20971960E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.43472557E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.29123367E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.88772164E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.02069673E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     3.06991249E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.33692123E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.66978891E-02    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.35359683E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.88788790E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.60824570E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.00296551E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.92231729E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.10672026E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.06458992E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.02273046E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.33156065E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.35853178E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.85174752E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.37486527E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.09671701E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.80669548E-03    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.71195076E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     4.22251168E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     9.29883681E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.31262800E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.59438363E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
#
#         PDG            Width
DECAY   2000002     8.62943788E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     9.99149768E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.50232024E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     4.28457890E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.66503228E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.24358254E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.52932821E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.05860250E-03    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.20413977E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.81242016E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.36141315E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.79218423E-02    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     4.22251168E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.29883681E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.31262800E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.59438363E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
#
#         PDG            Width
DECAY   2000004     8.62943788E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.99149768E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.50232024E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     4.28457890E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.66503228E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.24358254E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.52932821E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.05860250E-03    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.20413977E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.81242016E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.36141315E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.79218423E-02    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.81392116E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.46724627E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.10338809E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.93455943E-04    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.97931684E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96708127E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.28921362E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.55585291E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97142352E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.50829046E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.91644969E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.41517373E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.81392116E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.46724627E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.10338809E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.93455943E-04    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.97931684E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96708127E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.28921362E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.55585291E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97142352E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.50829046E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.91644969E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.41517373E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.69873194E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.36333584E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.26403174E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09509367E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.53497301E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.35306601E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.08049754E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.74093451E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.25941000E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.22226099E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.34294674E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.41407727E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26255701E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.48201760E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.81561772E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02739417E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.89548505E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.55340086E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.62937637E-03    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.03591732E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.35628715E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.81561772E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02739417E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.89548505E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.55340086E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.62937637E-03    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.03591732E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.35628715E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.83891223E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02389469E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.88562253E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.52767268E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.62042026E-03    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01583665E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.09142500E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.15282735E-06   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.42736808E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.42736808E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.05002538E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.05002538E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.04521309E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.88015799E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.91225486E-04    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.84806679E-03    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.91225486E-04    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.84806679E-03    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     7.45915098E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.53512791E-01    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.46566503E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.70285589E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.50810968E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.80846474E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.59886673E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.50601058E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.00634901E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.09200265E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.66527518E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.09200265E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.66527518E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.12961897E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60049363E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60049363E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.77750211E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19923291E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19923291E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19923291E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.41237395E-12    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.41237395E-12    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.41237395E-12    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.41237395E-12    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.73286256E-12    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.73286256E-12    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.73286256E-12    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.73286256E-12    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     3.78579839E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.42700157E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.42995165E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.74741650E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.74741650E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.83994996E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.18404943E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.36702253E-04    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.36702253E-04    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     3.28734948E-05    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     3.28734948E-05    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.09596265E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.09596265E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     8.18849303E-06    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     8.18849303E-06    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.36702253E-04    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.36702253E-04    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     3.28734948E-05    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     3.28734948E-05    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.09596265E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.09596265E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     8.18849303E-06    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     8.18849303E-06    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     2.64270397E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.64270397E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     1.37843960E-01    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     1.37843960E-01    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     2.01242316E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.01242316E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     3.83664183E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.86358089E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.19344594E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.70108820E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.70108820E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38505323E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.30657415E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.16319833E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     5.16319833E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     9.41050527E-05    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     9.41050527E-05    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     7.51847490E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     7.51847490E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.34415978E-05    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.34415978E-05    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     5.16319833E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     5.16319833E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     9.41050527E-05    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     9.41050527E-05    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     7.51847490E-04    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     7.51847490E-04    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.34415978E-05    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.34415978E-05    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     2.93375814E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.93375814E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.07311575E-01    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.07311575E-01    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     2.67477009E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.67477009E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     2.68763045E-03   # h decays
#          BR         NDA      ID1       ID2
     8.27652896E-01    2           5        -5   # BR(h -> b       bb     )
     7.72195479E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.73506268E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.08367398E-04    2           3        -3   # BR(h -> s       sb     )
     2.58457459E-02    2           4        -4   # BR(h -> c       cb     )
     5.96440999E-02    2          21        21   # BR(h -> g       g      )
     1.35776554E-03    2          22        22   # BR(h -> gam     gam    )
     2.05582592E-05    2          22        23   # BR(h -> Z       gam    )
     6.59238095E-03    2          24       -24   # BR(h -> W+      W-     )
     7.85131624E-04    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.77907332E+01   # H decays
#          BR         NDA      ID1       ID2
     1.71211491E-02    2           5        -5   # BR(H -> b       bb     )
     2.77946647E-03    2         -15        15   # BR(H -> tau+    tau-   )
     9.82548197E-06    2         -13        13   # BR(H -> mu+     mu-    )
     1.40618002E-05    2           3        -3   # BR(H -> s       sb     )
     1.13358668E-06    2           4        -4   # BR(H -> c       cb     )
     1.01437628E-01    2           6        -6   # BR(H -> t       tb     )
     3.27175217E-04    2          21        21   # BR(H -> g       g      )
     1.58132603E-06    2          22        22   # BR(H -> gam     gam    )
     1.57286556E-07    2          23        22   # BR(H -> Z       gam    )
     1.11170140E-04    2          24       -24   # BR(H -> W+      W-     )
     5.49773211E-05    2          23        23   # BR(H -> Z       Z      )
     5.30854903E-04    2          25        25   # BR(H -> h       h      )
     1.35542402E-25    2          36        36   # BR(H -> A       A      )
     1.05288320E-14    2          23        36   # BR(H -> Z       A      )
     4.79125162E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.68973390E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.23335062E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.55682752E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.69577337E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     2.92558340E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     1.69577337E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     2.92558340E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     9.13319120E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.88138462E-01    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.88138462E-01    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     2.38778588E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     7.23195772E-06    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     2.38778588E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     7.23195772E-06    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
     2.61893716E-04    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.68303061E+01   # A decays
#          BR         NDA      ID1       ID2
     1.81289107E-02    2           5        -5   # BR(A -> b       bb     )
     2.93907234E-03    2         -15        15   # BR(A -> tau+    tau-   )
     1.03895621E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.49117617E-05    2           3        -3   # BR(A -> s       sb     )
     1.17940632E-06    2           4        -4   # BR(A -> c       cb     )
     1.14992528E-01    2           6        -6   # BR(A -> t       tb     )
     2.23182368E-04    2          21        21   # BR(A -> g       g      )
     3.30336046E-07    2          22        22   # BR(A -> gam     gam    )
     2.46825889E-07    2          23        22   # BR(A -> Z       gam    )
     1.15419091E-04    2          23        25   # BR(A -> Z       h      )
     6.96954735E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.58034396E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.25100975E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.15061003E-03    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.25422314E-01    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.25422314E-01    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     2.11859690E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.13809406E-05    2           4        -5   # BR(H+ -> c       bb     )
     2.34449947E-03    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.28775844E-06    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.36827542E-07    2           2        -5   # BR(H+ -> u       bb     )
     5.65809613E-07    2           2        -3   # BR(H+ -> u       sb     )
     1.25361806E-05    2           4        -3   # BR(H+ -> c       sb     )
     1.01967598E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.34047766E-05    2          24        25   # BR(H+ -> W+      h      )
     7.56083500E-11    2          24        36   # BR(H+ -> W+      A      )
     2.82997588E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.73171364E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.38521379E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     3.38521379E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
     3.52455625E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.56796277E-01    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
