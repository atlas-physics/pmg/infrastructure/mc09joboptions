#--------------------------------------------------------------
# 
# Job options file
#
#
# Herwig & Jimmy SUSY production (mH+=400GeV, tan(beta)=15, mu = 200, M_2 = 310) 
#
# Responsible person(s)
#   17 Jul, 2009 :  Caleb LAMPEN (lampen@physics.arizona.edu)
# 
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg=AlgSequence("TopAlg")
 

try:
  if runArgs.ecmEnergy == 7000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common_7TeV.py" ) 
  if runArgs.ecmEnergy == 10000.0:
    include ( "MC09JobOptions/MC9_Herwig_Common.py" )
except NameError:
  # needed (dummy) default
  from Herwig_i.Herwig_iConf import Herwig
  topAlg += Herwig()
  Herwig = topAlg.Herwig

topAlg.Herwig.HerwigCommand += [ "iproc 13000",              #iproc SUSY proccees 
                                  "susyfile susy_mHp400tanb15_2.txt",#isawig input file
                                  "taudec TAUOLA"]           #taudec tau dcay package
#Setup tauola and photos. 
include ( "MC09JobOptions/MC9_Tauola_Fragment.py" )
                                 
include ( "MC09JobOptions/MC9_Photos_Fragment.py" )

#######################################################
# FILTER EFFICIENCY
#######################################################
from MC09JobOptions.HerwigEvgenConfig import evgenConfig
evgenConfig.efficiency = .9

from MC09JobOptions.SUSYEvgenConfig import evgenConfig
#==============================================================
#
# End of job options file
#
#####################################
