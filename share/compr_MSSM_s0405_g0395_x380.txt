#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50018495E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00184950E+03   # EWSB                
         1     3.98000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     1.05000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05882812E+01   # W+
        25     6.85012056E+01   # h
        35     1.00913868E+03   # H
        36     1.00000000E+03   # A
        37     1.00328221E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.04743162E+02   # ~d_L
   2000001     4.04930398E+02   # ~d_R
   1000002     4.05164012E+02   # ~u_L
   2000002     4.05069828E+02   # ~u_R
   1000003     4.04743162E+02   # ~s_L
   2000003     4.04930398E+02   # ~s_R
   1000004     4.05164012E+02   # ~c_L
   2000004     4.05069828E+02   # ~c_R
   1000005     5.04577285E+03   # ~b_1
   2000005     5.04621744E+03   # ~b_2
   1000006     4.91242511E+03   # ~t_1
   2000006     5.04748452E+03   # ~t_2
   1000011     4.99998854E+03   # ~e_L
   2000011     4.99998865E+03   # ~e_R
   1000012     5.00002281E+03   # ~nu_eL
   1000013     4.99998854E+03   # ~mu_L
   2000013     4.99998865E+03   # ~mu_R
   1000014     5.00002281E+03   # ~nu_muL
   1000015     4.99982377E+03   # ~tau_1
   2000015     5.00015402E+03   # ~tau_2
   1000016     5.00002281E+03   # ~nu_tauL
   1000021     3.95262289E+02   # ~g
   1000022     3.80093622E+02   # ~chi_10
   1000023    -1.01803797E+03   # ~chi_20
   1000025     1.01965985E+03   # ~chi_30
   1000035     4.96110078E+03   # ~chi_40
   1000024     1.01649349E+03   # ~chi_1+
   1000037     4.96110061E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.97520476E-01   # N_11
  1  2    -1.19813674E-03   # N_12
  1  3     4.91135037E-02   # N_13
  1  4    -5.03917495E-02   # N_14
  2  1     9.03261482E-04   # N_21
  2  2    -3.67857065E-04   # N_22
  2  3     7.07130496E-01   # N_23
  2  4     7.07082392E-01   # N_24
  3  1     7.03707795E-02   # N_31
  3  2     1.97469193E-02   # N_32
  3  3    -7.05231396E-01   # N_33
  3  4     7.05199752E-01   # N_34
  4  1     1.94145911E-04   # N_41
  4  2    -9.99804225E-01   # N_42
  4  3    -1.42479045E-02   # N_43
  4  4     1.37284812E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.01474287E-02   # U_11
  1  2     9.99797020E-01   # U_12
  2  1     9.99797020E-01   # U_21
  2  2     2.01474287E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94127457E-02   # V_11
  1  2     9.99811555E-01   # V_12
  2  1     9.99811555E-01   # V_21
  2  2     1.94127457E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00348300E-01   # cos(theta_t)
  1  2     9.94952370E-01   # sin(theta_t)
  2  1    -9.94952370E-01   # -sin(theta_t)
  2  2     1.00348300E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     7.19232425E-01   # cos(theta_b)
  1  2     6.94769544E-01   # sin(theta_b)
  2  1    -6.94769544E-01   # -sin(theta_b)
  2  2     7.19232425E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.07216343E-01   # cos(theta_tau)
  1  2     7.06997202E-01   # sin(theta_tau)
  2  1    -7.06997202E-01   # -sin(theta_tau)
  2  2     7.07216343E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -8.20777324E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00184950E+03  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     9.45234591E-01   # tanbeta(Q)          
         3     2.44833383E+02   # vev(Q)              
         4     1.10498891E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  5.00184950E+03  # The gauge couplings
     1     3.66958371E-01   # gprime(Q) DRbar
     2     6.37497405E-01   # g(Q) DRbar
     3     1.04300372E+00   # g3(Q) DRbar
#
BLOCK AU Q=  5.00184950E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00184950E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00184950E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00184950E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.15307021E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00184950E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.85140493E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00184950E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.38845333E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00184950E+03  # The soft SUSY breaking masses at the scale Q
         1     3.98000000E+02   # M_1                 
         2     5.00000000E+03   # M_2                 
         3     2.80000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.05161247E+05   # M^2_Hd              
        22     6.40698464E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     3.77000000E+02   # M_q1L               
        42     3.77000000E+02   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     3.77000000E+02   # M_uR                
        45     3.77000000E+02   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     3.77000000E+02   # M_dR                
        48     3.77000000E+02   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40245532E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.07601124E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.77930687E-06    2     1000022        21   # BR(~g -> ~chi_10 g)
#           BR         NDA      ID1       ID2       ID3
     1.15975177E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.84022928E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.15975177E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.84022928E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.00080591E-08    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
#
#         PDG            Width
DECAY   1000006     5.61674602E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     2.07347927E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07482034E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.06225028E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.12932437E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.52625708E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     4.43911164E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.93325140E-03    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.39726431E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.39965563E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.51864545E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.13411819E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.15533252E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.50411572E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.90335739E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.84280019E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.64783977E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.30065583E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.00683865E-05    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.26143761E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.67009812E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.28151337E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.53223823E-05    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     3.80285606E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.75784962E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.09168464E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.02978822E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.12673834E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.58197723E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.36800220E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.17254727E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     9.82838147E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.39053154E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.86094685E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.17144452E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.87958679E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.12041321E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     9.04036822E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.51120274E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.84887973E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     9.79624449E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.57207643E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.44279236E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     9.82838147E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.39053154E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.86094685E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.17144452E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.87958679E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.12041321E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     9.04036822E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.51120274E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.84887973E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     9.79624449E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.57207643E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.44279236E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.65695201E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.85422839E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.45247196E-08    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01257256E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.28529102E-04    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.26546627E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.45737568E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.64713769E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.95395880E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.58642372E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.60336104E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.15816868E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.65695201E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.85422839E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.45247196E-08    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01257256E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.28529102E-04    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.26546627E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.45737568E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.64713769E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.95395880E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.58642372E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.60336104E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.15816868E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     1.66132869E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     9.91328819E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.52604139E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.71959708E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.44503295E-04    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.96510543E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.89371173E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.65678797E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     9.92297404E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.10649330E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.74598183E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.47538155E-04    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.99521447E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.95431116E-04    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.65153801E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94497915E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.20068112E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.20301808E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.30724382E-04    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.10498553E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.46115624E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.65153801E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94497915E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.20068112E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.20301808E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.30724382E-04    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.10498553E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.46115624E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.66914023E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.91873084E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.19487274E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.19984289E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.28795742E-04    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.74025619E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.45582649E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02508660E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.20203466E-03    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
     3.45150009E-03    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
     3.20203466E-03    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
     3.45150009E-03    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
     9.86692930E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     3.06340362E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88542806E-01    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.88541803E-01    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88542806E-01    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.88541803E-01    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.91844600E-07    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.43549835E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.22948795E-04    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     4.26593981E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.25501627E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     4.54081877E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.76849557E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.76842888E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     8.84554546E-05    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.76737904E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.76031193E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.05158116E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99713476E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.71048544E-04    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.56919321E-07    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
     1.56919321E-07    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
     1.99290210E-06    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
     1.99290210E-06    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
     1.21506094E-06    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
     1.21506094E-06    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
     4.98336291E-07    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
     4.98336291E-07    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
     1.56919321E-07    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
     1.56919321E-07    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
     1.99290210E-06    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
     1.99290210E-06    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
     1.21506094E-06    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
     1.21506094E-06    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
     4.98336291E-07    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
     4.98336291E-07    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000025     1.16822675E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.55381820E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     9.28016244E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.13679164E-03    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.13679164E-03    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.09169992E-02    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.09169992E-02    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     1.46013838E-04    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     1.46013838E-04    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.72985425E-03    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.72985425E-03    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.13679164E-03    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.13679164E-03    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.09169992E-02    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.09169992E-02    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.46013838E-04    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     1.46013838E-04    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.72985425E-03    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.72985425E-03    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
#
#         PDG            Width
DECAY   1000035     3.06340766E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.03633068E-09    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.43491402E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.52503140E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.26501868E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.26501868E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30791506E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.56848987E-07    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     8.81189566E-05    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.47882806E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76118184E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.33935987E-05    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.52769240E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.24333986E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.75598770E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.76942796E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.76942796E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     9.16742271E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     9.16742271E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.03639629E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.03639629E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     9.16896694E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     9.16896694E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.09102344E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.09102344E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     9.16742271E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     9.16742271E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.03639629E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.03639629E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     9.16896694E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     9.16896694E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.09102344E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.09102344E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
#
#         PDG            Width
DECAY        25     1.81676679E-03   # h decays
#          BR         NDA      ID1       ID2
     8.63180022E-01    2           5        -5   # BR(h -> b       bb     )
     7.89007596E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.80036935E-04    2         -13        13   # BR(h -> mu+     mu-    )
     6.71778839E-04    2           3        -3   # BR(h -> s       sb     )
     2.84576498E-02    2           4        -4   # BR(h -> c       cb     )
     2.76507443E-02    2          21        21   # BR(h -> g       g      )
     6.37162965E-04    2          22        22   # BR(h -> gam     gam    )
     1.70677324E-04    2          24       -24   # BR(h -> W+      W-     )
     5.11682005E-05    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.84283443E+01   # H decays
#          BR         NDA      ID1       ID2
     2.49536670E-04    2           5        -5   # BR(H -> b       bb     )
     3.79988236E-05    2         -15        15   # BR(H -> tau+    tau-   )
     1.34326729E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.86570182E-07    2           3        -3   # BR(H -> s       sb     )
     1.10835296E-05    2           4        -4   # BR(H -> c       cb     )
     9.94128414E-01    2           6        -6   # BR(H -> t       tb     )
     1.42211401E-03    2          21        21   # BR(H -> g       g      )
     4.40152478E-06    2          22        22   # BR(H -> gam     gam    )
     1.68486229E-06    2          23        22   # BR(H -> Z       gam    )
     3.50631807E-04    2          24       -24   # BR(H -> W+      W-     )
     1.73446206E-04    2          23        23   # BR(H -> Z       Z      )
     1.40665456E-03    2          25        25   # BR(H -> h       h      )
     8.80157279E-19    2          36        36   # BR(H -> A       A      )
     1.41316058E-09    2          23        36   # BR(H -> Z       A      )
     3.72367753E-10    2          24       -37   # BR(H -> W+      H-     )
     3.72367753E-10    2         -24        37   # BR(H -> W-      H+     )
     1.62925502E-07    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.14499801E-04    2     1000002  -1000002   # BR(H -> ~u_L    ~u_L*  )
     7.04913279E-05    2     2000002  -2000002   # BR(H -> ~u_R    ~u_R*  )
     4.14499801E-04    2     1000004  -1000004   # BR(H -> ~c_L    ~c_L*  )
     7.04913279E-05    2     2000004  -2000004   # BR(H -> ~c_R    ~c_R*  )
     6.04149116E-04    2     1000001  -1000001   # BR(H -> ~d_L    ~d_L*  )
     1.76338236E-05    2     2000001  -2000001   # BR(H -> ~d_R    ~d_R*  )
     6.04149116E-04    2     1000003  -1000003   # BR(H -> ~s_L    ~s_L*  )
     1.76338236E-05    2     2000003  -2000003   # BR(H -> ~s_R    ~s_R*  )
#
#         PDG            Width
DECAY        36     5.18225393E+01   # A decays
#          BR         NDA      ID1       ID2
     2.41836453E-04    2           5        -5   # BR(A -> b       bb     )
     3.57355474E-05    2         -15        15   # BR(A -> tau+    tau-   )
     1.26324448E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.89050325E-07    2           3        -3   # BR(A -> s       sb     )
     1.02310437E-05    2           4        -4   # BR(A -> c       cb     )
     9.97530329E-01    2           6        -6   # BR(A -> t       tb     )
     1.70724265E-03    2          21        21   # BR(A -> g       g      )
     5.53909683E-06    2          22        22   # BR(A -> gam     gam    )
     2.07403538E-06    2          23        22   # BR(A -> Z       gam    )
     3.18443965E-04    2          23        25   # BR(A -> Z       h      )
     1.48253071E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        37     5.04115883E+01   # H+ decays
#          BR         NDA      ID1       ID2
     4.00398928E-07    2           4        -5   # BR(H+ -> c       bb     )
     3.68563121E-05    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.30286323E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     2.45927943E-09    2           2        -5   # BR(H+ -> u       bb     )
     8.89832270E-09    2           2        -3   # BR(H+ -> u       sb     )
     1.02657068E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.97687736E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32515769E-04    2          24        25   # BR(H+ -> W+      h      )
     9.90693143E-12    2          24        36   # BR(H+ -> W+      A      )
     9.66041902E-04    2     1000002  -1000001   # BR(H+ -> ~u_L    ~d_L*  )
     9.66041902E-04    2     1000004  -1000003   # BR(H+ -> ~c_L    ~s_L*  )
