#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12494771E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41        # version number                      
#
BLOCK MODSEL  # Model selection
     1    -1   # #bottom-up MSSM                                   
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     1.24947707E+03   # EWSB                
         1     1.01000000E+02   # M_1                 
         2     7.00000000E+02   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     1.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     2.80000000E+02   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.07313796E+01   # W+
        25     1.17085226E+02   # h
        35     1.00085832E+03   # H
        36     1.00000000E+03   # A
        37     1.00374466E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.03982551E+03   # ~d_L
   2000001     5.03957359E+03   # ~d_R
   1000002     5.03926202E+03   # ~u_L
   2000002     5.03939467E+03   # ~u_R
   1000003     5.03982551E+03   # ~s_L
   2000003     5.03957359E+03   # ~s_R
   1000004     5.03926202E+03   # ~c_L
   2000004     5.03939467E+03   # ~c_R
   1000005     3.80721826E+02   # ~b_1
   2000005     5.03957476E+03   # ~b_2
   1000006     9.33079576E+02   # ~t_1
   2000006     5.06084579E+03   # ~t_2
   1000011     5.00019061E+03   # ~e_L
   2000011     5.00017736E+03   # ~e_R
   1000012     4.99963201E+03   # ~nu_eL
   1000013     5.00019061E+03   # ~mu_L
   2000013     5.00017736E+03   # ~mu_R
   1000014     4.99963201E+03   # ~nu_muL
   1000015     4.99934539E+03   # ~tau_1
   2000015     5.00102305E+03   # ~tau_2
   1000016     4.99963201E+03   # ~nu_tauL
   1000021     1.23164865E+03   # ~g
   1000022     1.00018955E+02   # ~chi_10
   1000023     6.87525354E+02   # ~chi_20
   1000025    -1.00160517E+03   # ~chi_30
   1000035     1.01506086E+03   # ~chi_40
   1000024     6.87480640E+02   # ~chi_1+
   1000037     1.01467056E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.98910735E-01   # N_11
  1  2    -2.89858947E-03   # N_12
  1  3     4.45819151E-02   # N_13
  1  4    -1.34682990E-02   # N_14
  2  1     1.17304073E-02   # N_21
  2  2     9.78769224E-01   # N_22
  2  3    -1.61066331E-01   # N_23
  2  4     1.26217435E-01   # N_24
  3  1     2.18890521E-02   # N_31
  3  2    -2.51707900E-02   # N_32
  3  3    -7.05847633E-01   # N_33
  3  4    -7.07577854E-01   # N_34
  4  1     3.95045690E-02   # N_41
  4  2    -2.03393304E-01   # N_42
  4  3    -6.88367059E-01   # N_43
  4  4     6.95141241E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.73570818E-01   # U_11
  1  2     2.28385339E-01   # U_12
  2  1     2.28385339E-01   # U_21
  2  2     9.73570818E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.83818958E-01   # V_11
  1  2     1.79165446E-01   # V_12
  2  1     1.79165446E-01   # V_21
  2  2     9.83818958E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99998169E-01   # cos(theta_t)
  1  2     1.91363441E-03   # sin(theta_t)
  2  1    -1.91363441E-03   # -sin(theta_t)
  2  2     9.99998169E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999887E-01   # cos(theta_b)
  1  2     4.75394560E-04   # sin(theta_b)
  2  1    -4.75394560E-04   # -sin(theta_b)
  2  2     9.99999887E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04310597E-01   # cos(theta_tau)
  1  2     7.09891952E-01   # sin(theta_tau)
  2  1    -7.09891952E-01   # -sin(theta_tau)
  2  2     7.04310597E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.10053758E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.24947707E+03  # DRbar Higgs Parameters
         1     9.93019749E+02   # mu(Q)               
         2     4.79893107E+00   # tanbeta(Q)          
         3     2.43660261E+02   # vev(Q)              
         4     1.00309823E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  1.24947707E+03  # The gauge couplings
     1     3.61037433E-01   # gprime(Q) DRbar
     2     6.40709105E-01   # g(Q) DRbar
     3     1.04187262E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.24947707E+03  # The trilinear couplings
  1  1     2.09501995E+03   # A_u(Q) DRbar
  2  2     2.09501995E+03   # A_c(Q) DRbar
  3  3     2.72791132E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.24947707E+03  # The trilinear couplings
  1  1     1.44810328E+03   # A_d(Q) DRbar
  2  2     1.44810328E+03   # A_s(Q) DRbar
  3  3     1.65938636E+03   # A_b(Q) DRbar
#
BLOCK AE Q=  1.24947707E+03  # The trilinear couplings
  1  1     4.56054884E+02   # A_e(Q) DRbar
  2  2     4.56054884E+02   # A_mu(Q) DRbar
  3  3     4.56425638E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.24947707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.70296673E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.24947707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.02805758E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.24947707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.97322970E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.24947707E+03  # The soft SUSY breaking masses at the scale Q
         1     2.52248594E+02   # M_1                 
         2     8.91685332E+02   # M_2                 
         3     4.61012788E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.45013024E+06   # M^2_Hd              
        22     2.36545491E+07   # M^2_Hu              
        31     5.14343968E+03   # M_eL                
        32     5.14343968E+03   # M_muL               
        33     5.14684365E+03   # M_tauL              
        34     4.68581096E+03   # M_eR                
        35     4.68581096E+03   # M_muR               
        36     4.69331929E+03   # M_tauR              
        41     4.95043560E+03   # M_q1L               
        42     4.95043560E+03   # M_q2L               
        43     2.87624005E+03   # M_q3L               
        44     5.22351117E+03   # M_uR                
        45     5.22351117E+03   # M_cR                
        46     6.69636046E+03   # M_tR                
        47     4.91030950E+03   # M_dR                
        48     4.91030950E+03   # M_sR                
        49     4.91504680E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41032579E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.87140271E+01   # gluino decays
#          BR         NDA      ID1       ID2
     3.03660516E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.03660516E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     1.96339484E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     1.96339484E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.38201468E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.27024774E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.97729752E-01    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     4.44984997E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.53161411E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.38057412E-03    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.24924486E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.99051083E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.76257407E-03    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.39341528E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.38804047E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.80017922E-02    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.84590984E-03    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     6.74684587E-03    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     1.36674072E-02    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     4.54218897E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.28140487E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.02712371E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     2.87209600E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.77448015E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.92798867E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.27062560E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.01072897E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.66595301E-05    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.37954377E-03    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.86777981E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.21746123E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.92738225E-05    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     9.89513857E-05    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     1.95095703E-04    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     4.37365116E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.46877955E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.42000825E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.98896790E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.43864229E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.41196931E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.08449813E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.09405465E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.48475909E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.28625767E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.94651124E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81067172E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.09558882E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.70737660E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.55003557E-05    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61815613E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.42013280E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.11592332E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.39033779E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.66371817E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.41238142E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.07146963E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.66292718E-03    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.28711790E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86225539E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80746497E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.31144656E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.39427065E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.42841141E-05    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90172545E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.42000825E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.98896790E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.43864229E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.41196931E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.08449813E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.09405465E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.48475909E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.28625767E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.94651124E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81067172E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.09558882E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.70737660E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.55003557E-05    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61815613E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.42013280E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.11592332E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.39033779E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.66371817E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.41238142E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.07146963E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.66292718E-03    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.28711790E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86225539E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80746497E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.31144656E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.39427065E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.42841141E-05    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90172545E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.53132139E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79515872E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.92169816E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.74593399E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.42859565E-03    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70421352E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.99811892E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.59077550E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.97989523E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.32576756E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.41878528E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.43602155E-03    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.53132139E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79515872E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.92169816E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.74593399E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.42859565E-03    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70421352E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.99811892E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.59077550E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.97989523E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.32576756E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.41878528E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.43602155E-03    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.58380807E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54679526E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14010580E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.43060930E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.42917129E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.18038653E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.41146124E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     4.60631332E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.47269525E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.01641603E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.79597632E-03    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64173046E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.93070773E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.78048172E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.53286215E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.99553840E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.84281480E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.05004858E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.46278490E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.82286006E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.84442766E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.53286215E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.99553840E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.84281480E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.05004858E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.46278490E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.82286006E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.84442766E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.55553400E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.96096953E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83298312E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.03604177E-04    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.45772596E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.80458380E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.16527487E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.51610695E+00   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.05028852E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     9.55981116E-01    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
     3.51599895E-03    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     2.30174576E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     9.93266011E-03    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.19301901E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.34901599E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.70047715E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.38800284E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.63904795E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.90411375E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.12836058E-04    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.26801742E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.97559573E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.97559573E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.50613527E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.50185161E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.15756936E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     1.18499038E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.18499038E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.00870455E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.61410087E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.95954575E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     2.95954575E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.30808713E-03    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     4.30808713E-03    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000035     1.49335619E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.00536406E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.12344774E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.28099526E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.28099526E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.43489270E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.14690150E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.71224861E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.71224861E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.00675304E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.00675304E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.42255247E-03   # h decays
#          BR         NDA      ID1       ID2
     7.26202853E-01    2           5        -5   # BR(h -> b       bb     )
     7.39787926E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.61872510E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.61826845E-04    2           3        -3   # BR(h -> s       sb     )
     2.33883642E-02    2           4        -4   # BR(h -> c       cb     )
     6.92630913E-02    2          21        21   # BR(h -> g       g      )
     2.08630587E-03    2          22        22   # BR(h -> gam     gam    )
     8.36153294E-04    2          22        23   # BR(h -> Z       gam    )
     9.29403676E-02    2          24       -24   # BR(h -> W+      W-     )
     1.04803732E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     2.31589573E+00   # H decays
#          BR         NDA      ID1       ID2
     1.32084282E-01    2           5        -5   # BR(H -> b       bb     )
     2.05888609E-02    2         -15        15   # BR(H -> tau+    tau-   )
     7.27821265E-05    2         -13        13   # BR(H -> mu+     mu-    )
     1.04154804E-04    2           3        -3   # BR(H -> s       sb     )
     9.20300679E-06    2           4        -4   # BR(H -> c       cb     )
     8.23555659E-01    2           6        -6   # BR(H -> t       tb     )
     1.17296297E-03    2          21        21   # BR(H -> g       g      )
     3.83485121E-06    2          22        22   # BR(H -> gam     gam    )
     1.26991357E-06    2          23        22   # BR(H -> Z       gam    )
     2.90849170E-03    2          24       -24   # BR(H -> W+      W-     )
     1.43867949E-03    2          23        23   # BR(H -> Z       Z      )
     1.03694580E-02    2          25        25   # BR(H -> h       h      )
     6.29256506E-22    2          36        36   # BR(H -> A       A      )
     2.18101945E-13    2          23        36   # BR(H -> Z       A      )
     1.61043451E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.17044337E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.90948397E-03    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     2.40391961E+00   # A decays
#          BR         NDA      ID1       ID2
     1.27557237E-01    2           5        -5   # BR(A -> b       bb     )
     1.98567880E-02    2         -15        15   # BR(A -> tau+    tau-   )
     7.01933498E-05    2         -13        13   # BR(A -> mu+     mu-    )
     1.00752038E-04    2           3        -3   # BR(A -> s       sb     )
     8.55675066E-06    2           4        -4   # BR(A -> c       cb     )
     8.34286175E-01    2           6        -6   # BR(A -> t       tb     )
     1.61175970E-03    2          21        21   # BR(A -> g       g      )
     5.34721779E-06    2          22        22   # BR(A -> gam     gam    )
     1.89288483E-06    2          23        22   # BR(A -> Z       gam    )
     2.71628409E-03    2          23        25   # BR(A -> Z       h      )
     2.08812971E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.16968841E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     2.33863499E+00   # H+ decays
#          BR         NDA      ID1       ID2
     2.05680106E-04    2           4        -5   # BR(H+ -> c       bb     )
     2.04875377E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     7.24230339E-05    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.31625404E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94464968E-06    2           2        -3   # BR(H+ -> u       sb     )
     1.10134725E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.59985822E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.84097199E-03    2          24        25   # BR(H+ -> W+      h      )
     4.12629098E-10    2          24        36   # BR(H+ -> W+      A      )
     1.62911694E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
