###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#                                                         #
#  Revised by P. Jackson for StoppedGluino decays         #
#                                                         #
#  May 17th, 2010                                         #
#                                                         #
###########################################################

import os
import sys

os.system('get_files -data PDGTABLE_'+CASE+'_'+str(MASS)+'GeV.MeV')
os.system('cp PDGTABLE_'+CASE+'_'+str(MASS)+'GeV.MeV PDGTABLE.MeV')

from MC09JobOptions.PythiaRhadEvgenConfig import evgenConfig

from AthenaCommon.AlgSequence import AlgSequence
job=AlgSequence()
from Pythia_i.PythiaRhad_iConf import PythiaRhad
job +=PythiaRhad()

# use MC09 tune
#job.PythiaRhad.useAtlasPythiaTune09=True
#job.PythiaRhad.useAtlasPythiaTune08=False

# First add the ATLAS common parameters as defined in
# http://atlas-sw.cern.ch/cgi-bin/viewcvs-atlas.cgi/offline/Generators/MC09JobOptions/share/MC8_Pythia_Common.py?view=markup
# dedicated UE tune (from Holger Schulz)
# [MC09c basic parameters (LO* PDF) but with MSTP(81)=1 (R-hadron requirement).
job.PythiaRhad.Tune_Name="ATLAS_20090001"  # sets MC09 tune
# reset colour reconnection parameter for MC09c
job.PythiaRhad.PythiaCommand += [  "pypars parp 78 0.224"]   # colour reconnection in FS
# reset parameters for explicit R-hadron tune
job.PythiaRhad.PythiaCommand += [  "pypars mstp 81 1",    # Old shower/multiple-interaction model (new model is not compatible with R-hadron fragmentation)
                                   "pydat1 mstj 11 4",    # Set longitudinal fragmentation function to Pythia default
                                   "pypars parp 82 1.925399",
                                   "pypars parp 90 0.265917"]

#-------------------------#
# R-hadron commands below #
#-------------------------#
if (CASE=='gluino'):    
    job.PythiaRhad.RunGluinoHadrons=True
    job.PythiaRhad.RunStopHadrons=False
    #job.PythiaRhad.xshift=0 #mm
    #job.PythiaRhad.yshift=2500 #mm
    #job.PythiaRhad.zshift=1000 #mm
    #job.PythiaRhad.tshift=TIMESHIFT #ns
    job.PythiaRhad.randomtshift=50 # +-X ns, overrides tshift if non-zero
    job.PythiaRhad.rh_decay=True
    job.PythiaRhad.strip_out_rh=True
    job.PythiaRhad.boost_rh_to_rest_frame=True
    job.PythiaRhad.rotate_rh=True
    job.PythiaRhad.translate_rh_to_stopping_position=True
    job.PythiaRhad.StoppingInput = [ [ 0,0,0,0,0,0 ] ]
    include("StoppingInput.txt")
    ##theApp.EvtMax = len(job.PythiaRhad.StoppingInput)-1
    job.PythiaRhad.PythiaCommand += ["pymssm imss 1 1", "pymssm imss 3 1", "pymssm imss 5 1",
                                     "pymssm rmss 3 "+str(MASS)+".0", "pymssm rmss 1 "+str(MASSX)+".0", #gluino and neutralino masses
                                     "pymssm rmss 2 3000.0", "pymssm rmss 4 10000.0",
                                     "pymssm rmss 7 4000.0", "pymssm rmss 8 4000.0",
                                     "pymssm rmss 9 4000.0", "pymssm rmss 10 4000.0",
                                     "pymssm rmss 11 4000.0", "pymssm rmss 12 5000.0",
                                     #decays into gluons
                                     "pymssm rmss 21 "+str(MASSX)+".0e9", "pymssm imss 11 1", #ok to use the Gravitino
                                     "pymssm rmss 29 2.0e6", #planck mass, controls BR(gluino->g+Gravitino)
                                     "pydat3 mdcy 1000022 1 0", #kill neutralino decays
                                     #------------------
                                     #"pydat1 mstj 45 6", #allow CMshower->ttbar in gluino decays
                                     #"pydat1 mstj 43 1", #z definition in CM shower
                                     "pysubs msel 0", "pysubs msub 243 1", "pysubs msub 244 1", "pypars mstp 111 0",
                                     "pyinit pylisti 12", #dumps the full decay table, etc.
                                     #"pyinit pylistf 1", #dumps pythia event
                                     "pyinit dumpr 0 100" #,"pystat 2"
                                     ]
    
    
