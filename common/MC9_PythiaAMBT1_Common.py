m###############################################################
# job option fragments for year 2009 production -- ABMT1 tune
# U.E. tuning parameters for PYTHIA 6.421
# use Pythia_i/src/atlasTune.cxx
###############################################################

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# use MC09 tune
Pythia.useAtlasPythiaTune09=True
Pythia.useAtlasPythiaTune08=False

Pythia.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # the settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]

# reset parameters for ABMT1 tune [ATLAS-CONF-2010-031]
Pythia.PythiaCommand += [ "pypars parp 62 1.025232",
                          "pypars parp 77 1.015919",
                          "pypars parp 78 0.5378330",
                          "pypars parp 82 2.292052",
                          "pypars parp 83 0.356290",
                          "pypars parp 84 0.6508366",
                          "pypars parp 90 0.250241",
                          "pypars parp 93 10.0"
                          ]
