#
# CTEQ6L with UE tuning by Arthur Moraes
#
# author: B-Physics group, 2006-11-09
#
#--------------------------------------------------------------------------
PythiaB = Algorithm( "PythiaB" )
#PythiaB.PythiaCommand += [
# eta ranges 
#                          "pysubs ckin 9 -4.5",
#                          "pysubs ckin 10 4.5",
#                          "pysubs ckin 11 -4.5",
#                          "pysubs ckin 12 4.5",
# no B-B mixing
#                          "pydat1 mstj 26 0",
# parton decay length scheme:
# particle decayed if average proper lifetime
# c\tau < PARJ(71): default PARJ(71):= 10 mm
#                          "pydat1 mstj 22 2",
# Peterson / SLAC fragmentaton function for b-quark (def: -0.005)
#                          "pydat1 parj 55 -0.006",
# Excited B-meson parameters
#                          "pydat1 parj 13 0.65",
#                          "pydat1 parj 14 0.12",
#                          "pydat1 parj 15 0.04",
#                          "pydat1 parj 16 0.12",
#                          "pydat1 parj 17 0.2"
#                         ]

PythiaB.PythiaCommand += [
    "pysubs ckin 9 -4.5",
    "pysubs ckin 10 4.5",
    "pysubs ckin 11 -4.5",
    "pysubs ckin 12 4.5",
    "pydat1 mstj 26 0",
    "pydat1 mstj 22 2",
    "pydat1 parj 13 0.65",
    "pydat1 parj 14 0.12",
    "pydat1 parj 15 0.04",
    "pydat1 parj 16 0.12",
    "pydat1 parj 17 0.2",
    "pydat1 parj 55 -0.006",
    ]

#########################################################################################

