###########################################################
#                                                         #
#  R-hadron evgen job options fragment                    #
#  for running with Evgen_trf.py                          #
#                                                         #
#  Revised by C. Ohm for MC08 production 2008-09-22       #
#  Revised by R. Mackeprang for MC09 2010-02-26           # 
#                                                         #
###########################################################

import os
import sys

os.system('get_files -data PDGTABLE_'+CASE+'_'+str(MASS)+'GeV.MeV')
os.system('cp PDGTABLE_'+CASE+'_'+str(MASS)+'GeV.MeV PDGTABLE.MeV')

from MC09JobOptions.PythiaRhadEvgenConfig import evgenConfig

from AthenaCommon.AlgSequence import AlgSequence
job=AlgSequence()
from Pythia_i.PythiaRhad_iConf import PythiaRhad
job +=PythiaRhad()

# R. Field's D6 tune (hep-ph/0610012)? (satisfies R-hadron requirements of mstp(81)=1, mstj(11)=4)
# job.PythiaRhad.Tune_Name="PYTUNE_108"

# dedicated UE tune (from Holger Schulz)
# [MC09c basic parameters (LO* PDF) but with MSTP(81)=1 (R-hadron requirement).
# two-free parameter tune: final params PARP(82) = 1.925339, PARP(90) = 0.265917]
job.PythiaRhad.useAtlasPythiaTune09=True     # sets MC09 tune
job.PythiaRhad.useAtlasPythiaTune08=False
# reset colour reconnection parameter for MC09c 
job.PythiaRhad.PythiaCommand += [  "pypars parp 78 0.224"]   # colour reconnection in FS
# reset parameters for explicit R-hadron tune
job.PythiaRhad.PythiaCommand += [
	"pypars mstp 81 1",		# Old shower/multiple-interaction model (new model is not compatible with R-hadron fragmentation)
	"pydat1 mstj 11 4",		# Set longitudinal fragmentation function to Pythia default
        "pypars parp 82 1.925399",
        "pypars parp 90 0.265917"]


# add the ATLAS common parameters as defined in
# http://atlas-sw.cern.ch/cgi-bin/viewcvs-atlas.cgi/offline/Generators/MC09JobOptions/share/MC9_Pythia_Common.py?view=markup
job.PythiaRhad.PythiaCommand += [
	# initializations
	# "pyinit win 10000.",		# CM energy (comment out: defined in JobTransform)
	"pyinit pylisti 12",
	"pyinit pylistf 1",
	"pystat 1 3 4 5",
	"pyinit dumpr 1 5",
	# mass
	"pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
	"pydat2 pmas 24 1 80.403",  # PDG2007 W mass
	"pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
        # settings below have no effect (widths calculated perturbatively in Pythia)
	# "pydat2 pmas 24 2 2.141",   # PDG2007 W width
	# "pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
]

#-------------------------#
# R-hadron commands below #
#-------------------------#

# Add some commands valid for both gluino and stop cases
job.PythiaRhad.PythiaCommand += [
	"pysubs ckin 3 18.",		# pT cut at 18 GeV	
#	"pypars mstp 81 1",		# Old shower/multiple-interaction model (new model is not compatible with R-hadron fragmentation)
#	"pydat1 mstj 11 4",		# Set longitudinal fragmentation function to Pythia default
	"pymssm imss 1 1",		# General MSSM simulation
	"pymssm imss 3 1",		# Tell Pythia that rmss 3 below should be interpreted as the gluino pole mass
	"pymssm imss 5 1",		# Set stop, sbottom and stau masses and mixing by hand (26-28 for mixing not set!)
	"pymssm rmss 1 4000.0",		# Photino mass
	"pymssm rmss 2 4000.0",		# Wino/Zino mass
	"pymssm rmss 7 4000.0",		# Right slepton mass
	"pymssm rmss 8 4000.0",		# Left squark mass
	"pymssm rmss 9 4000.0",		# Right squark mass
	"pymssm rmss 10 4000.0",	# stop2 mass
	"pymssm rmss 11 4000.0",	# sbottom1 mass
	"pymssm rmss 4 4000.0",		# Higgsino mass parameter
	"pysubs msel 0",		# Turn off all processes
	"pypars mstp 111 0",		# Turn off master switch for fragmentation and decay
	"pyinit pylistf 3",					
	"pystat 2"]

if (CASE=='gluino'):
    job.PythiaRhad.RunGluinoHadrons=True
    job.PythiaRhad.RunStopHadrons=False
elif (CASE=='stop'):
    job.PythiaRhad.RunGluinoHadrons=False
    job.PythiaRhad.RunStopHadrons=True

# Gluino case
if job.PythiaRhad.RunGluinoHadrons:
    job.PythiaRhad.PythiaCommand += [
	"pymssm rmss 12 4000.0",		# stop1 mass
	"pymssm rmss 3 "+str(MASS)+".0",	# gluino pole mass
	#"pysubs msub 243 1",			# Turn on ffbar -> ~g~g, not used => conservative ~g production xsec!
	"pysubs msub 244 1"]			# Turn on gg -> ~g~g

# Stop case									
elif job.PythiaRhad.RunStopHadrons:
    job.PythiaRhad.PythiaCommand += [
	"pymssm rmss 12 "+str(MASS)+".0",	# stop1 mass
	"pymssm rmss 3 4000.0",			# gluino pole mass
	"pysubs msub 261 1",			# Turn on ffbar -> stop1stop1bar
	"pysubs msub 264 1"]			# Turn on gg -> stop1stop1bar
