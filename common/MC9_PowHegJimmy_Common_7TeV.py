###############################################################
# job option fragments for year 2009 7TeV production
# U.E. tuning parameters are for POWHEG
#      with HERWIG 6.510 + Jimmy 4.1
# contact: U. Husemann, C. Wasicki
# C. Gwenlan (modified to use CTEQ6L1, MC08 tune for PS/UE;
# alternative could be CTEQ6.6 with Holger's tune - decide before production!)
###############################################################

from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()

Herwig = topAlg.Herwig
Herwig.HerwigCommand = [
    # initializations
    "iproc lhef",
    # mass  
    "rmass 6 172.5",    # PDG2007 TOP mass
    "rmass 198 80.403", # PDG2007 W mass
    "rmass 199 80.403", # PDG2007 W mass 
    "rmass 200 91.1876",# PDG2007 Z0 mass
    "gamw 2.141",       # PDG2007 W width
    "gamz 2.4952",      # PDG2007 Z0 width
    # main switch 
    "jmueo 1",          # Underlying event option (2->2 QCD)
    "msflag 1",         # turn on multiple interactions
    "jmbug 0",
    # U.E. tuning (for CTEQ6.6) 
    # "ptjim 4.73",      # for 7TeV (5.72 for 14TeV; 5.22 for 10TeV) minimum pT of secondary scatters
    # "jmrad 73 2.88",   # Inverse proton radius squared  
    # U.E. tuning (for CTEQ6L1 i.e. MC08 tune) 
    "ptjim 4.06",       # for 7TeV (4.91 for 14TeV; 4.48 for 10TeV) minimum pT of secondary scatters
    "jmrad 73 1.8",     #  Inverse proton radius squared  
    "prsof 0",          #  soft underlying event off (Herwig parameter)
    # PDF 
    # "modpdf 10550",   #  CTEQ66
    "modpdf 10042",     #  CTEQ6L1 (LO fit with LO alpha_s)
    "autpdf HWLHAPDF",  #  external PDF library
    # Fragmentation
    "clpow 1.20",       # to fix the ratio of mesons to baryons in B decays
    "pltcut 0.0000000000333",  # to make Ks and Lambda stable
    "ptmin 10.",        # (D=10.) min. pT in hadronic jet production
    # Others
    "mixing 1"          # (D=1) include neutral B meson mixing 
    "maxpr 5"           # print out event record
    ]


