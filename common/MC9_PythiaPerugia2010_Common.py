#######################################################################################
#
# Job options file for using adaption of Perugia2010 for pythia 6.421 
# [omitting the new color reconnection which is only available from pythia 6.423 onwards]
# author: J.Katzy 
#
# reference Perugia2010 parameters as in arXiv:1005.3457v1 [hep-ph]
#=====================================================================================

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()
Pythia = topAlg.Pythia

# MC9 runs with Pythia 6.4.21, Perugia2010 only in PYTUNE since 6.4.24;
#Pythia.Tune_Name="PYTUNE_327"

# => need to pass parameter explicitly
Pythia.Tune_Name="ATLAS_-1"              # turn off ATLAS defaults (tune etc.)
Pythia.PygiveCommand += [ "MSTJ(22)=2" ] # ATLAS stable particles convention

# Perugia 10 NOCR [334] parameter list:
Pythia.PygiveCommand += [ "PARJ(81)=0.26",
			  "PARJ(82)=1.0",  # FSR
			  "MSTJ(11)=5",    # HAD
			  "PARJ(1)=0.08",
			  "PARJ(2)=0.21", 
			  "PARJ(3)=0.94",
			  "PARJ(4)=0.04", 
			  "PARJ(11)=0.35", 
			  "PARJ(12)=0.35",
			  "PARJ(13)=0.54", 
			  "PARJ(21)=0.36",
			  "PARJ(25)=0.63", 
			  "PARJ(26)=0.12",  
			  "PARJ(41)=0.35",  
			  "PARJ(42)=0.9", 
			  "PARJ(46)=1.0",  
			  "PARJ(47)=1.0",  
			  "MSTP(51)=7",  # CTEQ5L pdf
			  "MSTP(52)=1",  # CTEQ5L pdf
			  "MSTP(3)=1",   # lambda QCD for FSR handling
			  "MSTU(112)=4", #
			  "PARU(112)=0.192", #
			  "PARP(1)=0.192",  # ME
			  "PARP(61)=0.192",  # ISR
			  "PARP(72)=0.26",   # IFSR
                          "MSTP(64)=3",
			  "PARP(64)=1.0",    # ISR
			  "MSTP(67)=2",      # ISR
			  "PARP(67)=1.0",    #
			  "PARP(71)=2.0",    #
                          "MSTP(70)=2",      #
			  "MSTP(72)=2",      #
			  "MSTP(91)=1",      #
			  "PARP(91)=2.0",    #
			  "PARP(93)=10.0",   #
                          
			  "MSTP(81)=21",     # UE
			  "PARP(82)=2.05",   #
			  "PARP(89)=1800",   # 
			  "PARP(90)=0.25",   # 
			  "MSTP(82)=5",      # 
			  "PARP(83)=1.5",    #
                          "MSTP(33)=0",      # 
			  "MSTP(88)=0",      # BR
			  "PARP(79)=2.0",    #
			  "MSTP(89)=0",      # 
			  "PARP(80)=0.1",    #                         
                          #
			  "MSTP(95)=6",      # CR taken from Perugia0
			  "PARP(78)=0.33",   #
			  "PARP(77)=0.9",    #

                          ]    #


Pythia.PythiaCommand += [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",    # PDG2007 TOP mass 
    "pydat2 pmas 24 1 80.403",  # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876", # PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",   # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2007 Z0 width
    ]


