###############################################################
# job option fragments for year 2009 10TeV production
# U.E. tuning parameters are for PYTHIA 6.417
# contact: Osamu Jinnouchi
# Adapted for PythiaB by James.Catmore@cern.ch
###############################################################

from PythiaB.PythiaBConf import PythiaB
topAlg += PythiaB()
PythiaB = topAlg.PythiaB

# use MC09 tune
PythiaB.useAtlasPythiaTune09=True
PythiaB.useAtlasPythiaTune08=False

PythiaB.PythiaCommand = [
    # initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # mass
    "pydat2 pmas 6 1 172.5",   # PDG2007 TOP mass
    "pydat2 pmas 24 1 80.403", # PDG2007 W mass
    "pydat2 pmas 23 1 91.1876",# PDG2007 Z0 mass
    # settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.141",  # PDG2007 W width
    #"pydat2 pmas 23 2 2.4952", # PDG2007 Z0 width
    ]
