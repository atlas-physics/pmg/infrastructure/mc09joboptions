###############################################################
# job option fragments for year 2009 10TeV production
# U.E. tuning parameters for HERWIG 6.510 + Jimmy 4.1
# (for Alpgen production; these are identical to MC8 settings)
# contact: Osamu Jinnouchi, Claire Gwenlan
###############################################################

from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()

Herwig = topAlg.Herwig
Herwig.HerwigCommand = [
    # mass  
    "rmass 6 172.5",    # PDG2007 TOP mass
    "rmass 198 80.403", # PDG2007 W mass
    "rmass 199 80.403", # PDG2007 W mass 
    "rmass 200 91.1876",# PDG2007 Z0 mass
    "gamw 2.141",       # PDG2007 W width
    "gamz 2.4952",      # PDG2007 Z0 width
    # main switch 
    "jmueo 1",          # Underlying event option (2->2 QCD)
    "msflag 1",         # turn on multiple interactions
    "jmbug 0",
    # U.E. tuning 
    "ptjim 4.48",       # for 10TeV (4.91 for 14TeV; 4.06 for 7TeV) minimum pT of secondary scatters
    "jmrad 73 1.8",     #  Inverse proton radius squared  
    "prsof 0",          #  soft underlying event off (Herwig parameter)
     # PDF 
    "modpdf 10042",     #  CTEQ6L1 (LO fit with LO alpha_s)
    "autpdf HWLHAPDF",  #  external PDF library
    # Fragmentation
    "clpow 1.20",       # to fix the ratio of mesons to baryons in B decays
    "pltcut 0.0000000000333",  # to make Ks and Lambda stable
    "ptmin 10.",        # (D=10.) min. pT in hadronic jet production
    # Others
    "mixing 1",         # (D=1) include neutral B meson mixing 
    "maxpr 5"           # print out event record
    ]


